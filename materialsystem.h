#pragma once

#ifndef MATERIALSYSTEM_H
#define MATERIALSYSTEM_H 1

#include "util.h"
using namespace std;

static map<string, ID3D11ShaderResourceView*> TEXTURE_BUFFER;

#define Material(x) &TEXTURE_BUFFER[x]

inline HRESULT CreateMaterial(ID3D11Device* pd3dDevice, const string textureName, const wstring fileName)
{
	return D3DX11CreateShaderResourceViewFromFile(pd3dDevice, fileName.c_str(), NULL, NULL, &TEXTURE_BUFFER[textureName], NULL);
}

inline HRESULT LoadMaterials(ID3D11Device* pd3dDevice)
{
	vector<string> fileNames = GetFileNamesInDirectory("materials/*");

	for (int i = 0; i < fileNames.size(); i++)
	{
		if (fileNames[i] == "..") continue;

		string name = fileNames[i];
		size_t lastindex = name.find_last_of(".");

		if (lastindex != string::npos)
			name = name.substr(0, lastindex);

		wstring path = s2ws("materials/" + fileNames[i]);

		HRESULT result = CreateMaterial(pd3dDevice, name, path);

		if (FAILED(result))
			return result;
	}

	return S_OK;
}

#endif // MATERIALSYSTEM_H