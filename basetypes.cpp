#include "basetypes.h"
#include <time.h>

clock_t START_TIME = clock();

// Returns program uptiem in seconds
clock_t RunTime()
{
	clock_t curTime = clock();
	clock_t diff = curTime - START_TIME;

	return diff / CLOCKS_PER_SEC;
}