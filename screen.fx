#include "shaderheader.fxh"
Texture2D txDiffuse : register(t0);
Texture2D titleScreen : register(t1);

SamplerState samLinear : register(s0);

sampler s0 : register(s0);

cbuffer ConstantBuffer : register(b0)
	{
	matrix World;
	matrix View;
	matrix Projection;
	matrix LightViewProjection;
	float4 info;
	float4 campos;
	float4 sun_position; //rotation of the sun
	float4 daytimer; //day+night cycle
	};

struct VS_INPUT
	{
	float4 Pos : POSITION;
	float2 Tex : TEXCOORD0;
	float3 Norm : NORMAL0;
	float3 Tan : TANGENT0;
	float3 Bi : BiNORMAL0;
	};

struct PS_INPUT
	{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float3 WorldPos : TEXCOORD1;
	float4 Norm : Normal0;
	float4 OPos : TEXCOORD2;

	};


PS_INPUT VS(VS_INPUT input)
	{
	PS_INPUT output = (PS_INPUT)0;
	float4 pos = input.Pos;
	output.Pos = pos;
	output.Tex = input.Tex;
	output.Norm.xyz = input.Norm;
	output.Norm.w = 1;

	return output;
	}


float4 PS(PS_INPUT input) : SV_Target
{

	float4 texture_color = txDiffuse.Sample(samLinear, input.Tex);
	//float4 vl = txVL.Sample(samLinear, input.Tex);
	//float4 bloom = txBloom.Sample(samLinear, input.Tex);

	float4 titleColor = titleScreen.Sample(samLinear, input.Tex);

	bool bShowGlitch = info.y < 4;
	bool bShowTitle = info.w == 1;
	float glitchMod = info.z;
	float md = fmod(input.Tex.y, glitchMod);

	if (bShowGlitch && (input.Tex.y == md))
	{
		float modifier = glitchMod / 50.0f + info.y / 70.0f;

		if (info.y > 2)
			modifier *= -1;
		
		titleColor = titleScreen.Sample(samLinear, float2(input.Tex.x + modifier, input.Tex.y));

		titleColor.rgb *= glitchMod * md;

	    half4 actColor = titleColor;


	    // Sample R G and B of adjacent pixels
	    half4 rightRed = titleScreen.Sample(samLinear, float2(input.Tex.x + (0.01 * 0.4) + modifier, input.Tex.y));
	    half4 leftGreen = titleScreen.Sample(samLinear, float2(input.Tex.x - (0.01 * 0.3) + modifier, input.Tex.y));
	    half4 diagBlue = titleScreen.Sample(samLinear, float2(input.Tex.x + (0.02 * 0.05) + modifier, input.Tex.y + (0.02 * 0.015) + modifier * 0.3f));

	    actColor.r = rightRed.r;
	    actColor.g = leftGreen.g;
	    actColor.b = diagBlue.b;
		
		titleColor = actColor;
		titleColor.rgb *= 2;
	}

	if (bShowGlitch)
	{
		if (fmod(input.Tex.y, 0.005f) <= 0.001f)
		{
			titleColor.rgb = 0.0f;
		}
	}

	texture_color.a = 1;

	if (!bShowTitle)
		return float4(0, 0, 0, 1);

	if (info.x < 1)
	{
		if (info.x >= 0.5)
		{
			float mapFrac = remap(info.x, 0.5, 1, 0, 1);

			return lerp(titleColor, texture_color, mapFrac);
		}
		else
			return titleColor;
	}
	else
	{
		if (info.x == 1.0f)
		{
			if (input.Tex.y == md)
			{
				float modifier = glitchMod / 30.0f + info.y / 50.0f;

				if (info.y > 2)
					modifier *= -1;

			    half4 actColor = texture_color;


			    // Sample R G and B of adjacent pixels
			    half4 rightRed = txDiffuse.Sample(samLinear, float2(input.Tex.x + (0.01 * 0.4) + modifier, input.Tex.y));
			    half4 leftGreen = txDiffuse.Sample(samLinear, float2(input.Tex.x - (0.01 * 0.3) + modifier, input.Tex.y));
			    half4 diagBlue = txDiffuse.Sample(samLinear, float2(input.Tex.x + (0.02 * 0.05) + modifier, input.Tex.y + (0.02 * 0.015) + modifier * 0.3f));

			    actColor.r = rightRed.r;
			    actColor.g = leftGreen.g;
			    actColor.b = diagBlue.b;

			    texture_color.rgb = actColor.rgb;
			}

			texture_color.rgb *= 3;
		}
	}

	//return float4(0, 0, 1, 1);
	return texture_color;
}