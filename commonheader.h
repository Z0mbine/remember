#define NUM_THREADS 1024 //max 1024
#define NUM_THREADSX 8
#define NUM_THREADSY 128
#define NUM_THREADSXY 1024 //max 1024

#define SHADOWMAPXYSIZE 4096

#define WINRESX 1920
#define WINRESY 1080

#define MAXTEX1DSIZE 16384
#define BUFFERSIZE 8000000
#define BUFFERSIZE_NB 40000000
#define BRICKPOOLSIZE 510
#define COUNTSIZE 50			//see shaderheader.fxh for more info

#define OCT_ELEMENTSIZE 10		//8 kids index, brickbuff, binary index       ... 0-7,8,9     //see shaderheader.fxh for more info
#define OCT_ELEMENTSIZE_LL 6	//brickbuffer, binary index, r,g,b,pixelcount ... 0,1,2,3,4,5 //see shaderheader.fxh for more info
#define OCT_BRICKINDEX 8
#define OCT_BRICKINDEX_LL 0
#define OCT_BINARYINDEX 9
#define OCT_BINARYINDEX_LL 1

static const unsigned int maxlevel = 8;
static const float vxarea = 256;
static const int brickpoolsector = (BRICKPOOLSIZE*BRICKPOOLSIZE*BRICKPOOLSIZE) / (3 * 3 * 3 * 3); // 1/3 of the total number of 3x3x3 volumes in the brick pool
static const float PI = 3.14159265f;

// Front/Middle/Back, Left/Middle/Right, Up/Middle/Down
#define NB_COUNT 26

#define NB_F_L_U 0
#define NB_F_M_U 1
#define NB_F_R_U 2
#define NB_F_L_M 3
#define NB_F_M_M 4
#define NB_F_R_M 5
#define NB_F_L_D 6
#define NB_F_M_D 7
#define NB_F_R_D 8

#define NB_M_L_U 9
#define NB_M_M_U 10
#define NB_M_R_U 11
#define NB_M_L_M 12
#define NB_M_R_M 13
#define NB_M_L_D 14
#define NB_M_M_D 15
#define NB_M_R_D 16

#define NB_B_L_U 17
#define NB_B_M_U 18
#define NB_B_R_U 19
#define NB_B_L_M 20
#define NB_B_M_M 21
#define NB_B_R_M 22
#define NB_B_L_D 23
#define NB_B_M_D 24
#define NB_B_R_D 25