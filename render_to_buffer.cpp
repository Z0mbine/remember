#include "render_to_buffer.h"

///////////////////////////////////////////////////////////
bool BufferClass::Initialize_buffer(ID3D11Device* device, UINT elementSize, int count, bool rtv_, bool srv_, bool uav_, bool regenerate)
	{
	if (m_buffer)
		{
		if (!regenerate)
			return FALSE;
		else 
			ResetBuffersAndViews();
		}

	UINT bufferCount;
	if (count >= 0)	bufferCount = count;
	else bufferCount = 10000;
	w = bufferCount;

	rtv = rtv_;
	srv = srv_;
	uav = uav_;

	HRESULT result;
	D3D11_BUFFER_DESC bufferDesc;

	// Set up and create the buffer.
	ZeroMemory(&bufferDesc, sizeof(bufferDesc));
	bufferDesc.BindFlags = (rtv ? D3D11_BIND_RENDER_TARGET : 0) | (srv ? D3D11_BIND_SHADER_RESOURCE : 0) | (uav ? D3D11_BIND_UNORDERED_ACCESS : 0);
	bufferDesc.ByteWidth = elementSize * w;
	bufferDesc.StructureByteStride = elementSize;
	bufferDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	result = device->CreateBuffer(&bufferDesc, NULL, &m_buffer);
	if (FAILED(result))
		return FALSE;

	// Set up and create the render target view.
	if (rtv)
		{
		D3D11_BUFFER_RTV bufferRTV;
		ZeroMemory(&bufferRTV, sizeof(bufferRTV));
		bufferRTV.FirstElement = 0;
		bufferRTV.NumElements = w;

		D3D11_RENDER_TARGET_VIEW_DESC descRTV;
		ZeroMemory(&descRTV, sizeof(descRTV));
		descRTV.Buffer = bufferRTV;
		descRTV.Format = DXGI_FORMAT_UNKNOWN;
		descRTV.ViewDimension = D3D11_RTV_DIMENSION_BUFFER;
		result = device->CreateRenderTargetView(m_buffer, &descRTV, &m_renderTargetView);
		if (FAILED(result))
			return FALSE;
		}

	// Set up and create the shader resource view.
	if (srv)
		{
		D3D11_BUFFER_SRV bufferSRV;
		ZeroMemory(&bufferSRV, sizeof(bufferSRV));
		bufferSRV.FirstElement = 0;
		bufferSRV.NumElements = w;

		D3D11_SHADER_RESOURCE_VIEW_DESC descSRV;
		ZeroMemory(&descSRV, sizeof(descSRV));
		descSRV.Buffer = bufferSRV;
		descSRV.Format = DXGI_FORMAT_UNKNOWN;
		descSRV.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
		result = device->CreateShaderResourceView(m_buffer, &descSRV, &m_shaderResourceView);
		if (FAILED(result))
			return FALSE;
		}

	// Set up and create the unordered access view.
	if (uav)
		{
		D3D11_BUFFER_UAV bufferUAV;
		ZeroMemory(&bufferUAV, sizeof(bufferUAV));
		bufferUAV.FirstElement = 0;
		bufferUAV.NumElements = w;
		bufferUAV.Flags = D3D11_BUFFER_UAV_FLAG_COUNTER; //D3D11_BUFFER_UAV_FLAG_APPEND or D3D11_BUFFER_UAV_FLAG_COUNTER

		D3D11_UNORDERED_ACCESS_VIEW_DESC descUAV;
		ZeroMemory(&descUAV, sizeof(descUAV));
		descUAV.Format = DXGI_FORMAT_UNKNOWN;
		descUAV.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
		descUAV.Buffer = bufferUAV;
		result = device->CreateUnorderedAccessView(m_buffer, &descUAV, &m_unorderedAccessView);
		if (FAILED(result))
			return FALSE;
		}

	return TRUE;
	}

///////////////////////////////////////////////////////////
bool BufferClass::Initialize_staging(ID3D11Device* device)
	{
	HRESULT result;

	if(!m_buffer)
		return FALSE;
	if (!m_staging) 
		{
		D3D11_BUFFER_DESC bufferDesc;
		m_buffer->GetDesc(&bufferDesc);
		bufferDesc.BindFlags = 0;
		bufferDesc.Usage = D3D11_USAGE_STAGING;
		bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
		result = device->CreateBuffer(&bufferDesc, NULL, &m_staging);
		if (FAILED(result))
			return FALSE;
		}

	return TRUE;
	}

///////////////////////////////////////////////////////////
void BufferClass::ResetBuffersAndViews()
	{
	srv = uav = rtv = false;
	w = 0;
	if (m_shaderResourceView) { m_shaderResourceView->Release();  m_shaderResourceView = NULL; }
	if (m_renderTargetView) { m_renderTargetView->Release();  m_renderTargetView = NULL; }
	if (m_unorderedAccessView) { m_unorderedAccessView->Release();  m_unorderedAccessView = NULL; }
	if (m_staging) { m_staging->Release(); m_staging = NULL; }
	if (m_buffer) { m_buffer->Release(); m_buffer = NULL; }
	}