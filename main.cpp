#include "engine.h"
#include "Font.h"
#include <time.h>
#include <thread>
#include "timer.h"
#include "vxgi.h"
#include "filesys.h"


/*


	REMEMBER

	A tech demo by Austin Thao and Dylan Montgomery


*/







//--------------------------------------------------------------------------------------
// Global Variables
// I hate all of these they should be contained in a class
// BUT THEY'RE NOT?????????????
// Bad programming
//--------------------------------------------------------------------------------------

vxgi_ vxgi;
shadow_mapping_	shadow_mapping;
music_ sound;

map<string, double> songs;
map<string, int> musicTracks;

int mode = 0;
bool isStatic = false;
bool voxeldraw = false;
float shadow_factor = 1;

CBasePlayer PLAYER;
CEngine engine;
XMMATRIX View;
XMMATRIX LightView;
XMMATRIX World;
XMMATRIX Projection;
Font ui;

ID3D11Buffer* g_pVertexBuffer = NULL;
ID3D11VertexShader* pVertexShader = NULL;
ID3D11PixelShader* pPixelShader = NULL;
ID3D11VertexShader* pVertexShader_screen = NULL;
ID3D11PixelShader* pPixelShader_screen = NULL;
ID3D11Buffer* g_pVertexBuffer_screen = NULL;
ID3D11BlendState* g_pBlendState;
ID3D11Debug* g_pDebug;
ID3D11InfoQueue* g_pInfoQueue;
RenderTextureClass LightTexture;
ID3D11RenderTargetView* g_pRenderTargetView = NULL;
ID3D11VertexShader*                 g_pScreenVS = NULL;
ID3D11PixelShader*                  g_pScreenPS = NULL;
ID3D11VertexShader*                 g_pScreenVScompare = NULL;
ID3D11PixelShader*                  g_pScreenPScompare = NULL;
ID3D11Buffer*                       g_pDoubleVertexBuffer = NULL;

#define PS(name) engine.GetPixelShader(name)
#define VS(name) engine.GetVertexShader(name)

float MOUSESENSE_X = 0.022f;
float MOUSESENSE_Y = 0.022f;

#define SCRW 1920
#define SCRH 1000
int playerCount[3] = { 0 };
D3D11_VIEWPORT						viewport_normal;
D3D11_VIEWPORT						viewport_octree;
XMMATRIX                            g_View, g_View_front, g_View_top, g_View_left;
XMMATRIX                            g_Projection;
XMMATRIX							g_Projection2;
bool GLITCH = false;

//--------------------------------------------------------------------------------------
// Forward declarations
//--------------------------------------------------------------------------------------
HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
HRESULT InitDevice();
void CleanupDevice();
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
void Render();
bool Load3DS(char *filename, ID3D11Device* pd3dDevice, ID3D11Buffer **ppVertexBuffer, int *vertex_count);
bool loadauh(char *filename, ID3D11Device* g_pd3dDevice, ID3D11Buffer **ppVertexBuffer, int *vertex_count);
bool LoadOBJ(char *filename, ID3D11Device* g_pd3dDevice, ID3D11Buffer **ppVertexBuffer, int *vertex_count);
void debugCS();
int find_in_octreebin(unsigned int binaryindex);
unsigned int vectobin(XMFLOAT3 pos);

inline bool LoadCatmullClark(LPCTSTR filename, ID3D11Device* g_pd3dDevice, ID3D11Buffer **ppVertexBuffer, int *vertex_count)
{
	struct CatmullVertex
	{
		XMFLOAT3 pos;
		XMFLOAT3 normal;
		XMFLOAT2 tex;
	};
	HANDLE file;
	std::vector<SimpleVertex> data;
	DWORD burn;

	file = CreateFile(filename, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	SetFilePointer(file, 80, NULL, FILE_BEGIN);
	ReadFile(file, vertex_count, 4, &burn, NULL);

	for (int i = 0; i < *vertex_count; ++i)
	{
		CatmullVertex vertData;
		ReadFile(file, &vertData, sizeof(CatmullVertex), &burn, NULL);
		SimpleVertex sv;
		sv.Pos = vertData.pos;
		sv.Normal = vertData.normal;
		sv.Tex = vertData.tex;
		data.push_back(sv);
	}

	D3D11_BUFFER_DESC desc = {
		sizeof(SimpleVertex)* *vertex_count,
		D3D11_USAGE_DEFAULT,
		D3D11_BIND_VERTEX_BUFFER,
		0, 0,
		sizeof(SimpleVertex)
	};
	D3D11_SUBRESOURCE_DATA subdata = {
		&(data[0]), 0, 0
	};
	HRESULT hr = g_pd3dDevice->CreateBuffer(&desc, &subdata, ppVertexBuffer);

	if (FAILED(hr))
		return false;

	return true;
}

XMFLOAT3 cone_tracing(XMFLOAT3 pos, XMFLOAT3 direction, int steps)
{
	float voxelsize = 1;
	XMFLOAT3 start = pos + direction;
	float dist_mul = 1.0;
	float angle = 0.785398163;
	XMFLOAT3 cone_color = XMFLOAT3(0, 0, 0);
	float check = 0;
	float lods[10];
	float dist[10];
	for (int ii = 0; ii < 10; ii++)
	{
		XMFLOAT3 t = direction * dist_mul;
		float d = 2 * length(t) * tan(angle / 2.0);
		float miplevel = log2(d / voxelsize) - 0.1;
		miplevel *= 1.6;
		miplevel = max(miplevel, 0);
		float sampleLOD = 8 - miplevel;
		sampleLOD = max(sampleLOD, 2);
		dist[ii] = length(t);
		lods[ii] = sampleLOD;
		cone_color = cone_color + XMFLOAT3(0.1, 0.1, 0.1);// get_interpolated_octree_color(start + t, sampleLOD);

		if (ii == 0)
		{
			check = cone_color.x;
		}
		dist_mul += d / 2.7;
	}
	if (check > 0.1)
		return XMFLOAT3(0, 0, 0);
	return cone_color;
}

void PlayMusic(const char *song, bool bAutoPlay = false)
{
	double duration;

	if (song == "0")
	{
		auto it = songs.begin();
		std::advance(it, rand() % songs.size());
		string random_key = it->first;

		duration = songs[random_key];

		start_music(GetWC(&random_key[0u]), -3000);
	}
	else
	{
		start_music(GetWC(const_cast<char*>(song)), -3000);

		if (song == "music/spook_intro.wav")
			duration = 61.0;
		else
			duration = songs[song];
	}

	if (bAutoPlay)
	{
		auto lambda = []{
			PlayMusic("0", true);
		};

		Timer::Simple(duration, lambda);
	}
}

//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	if (FAILED(InitWindow(hInstance, nCmdShow)))
		return 0;

	if (FAILED(InitDevice()))
	{
		CleanupDevice();
		return 0;
	}

	// Main message loop
	MSG msg = { 0 };
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			Render();
		}
	}

	CleanupDevice();

	return (int)msg.wParam;
}

ID3D11InputLayout *surfaceLayout = NULL;
bool ConstructVShader(ID3D11Device *device, TCHAR* shaderPath, ID3D11VertexShader *buffer)
{
	ID3DBlob* pVSBlob = NULL;
	HRESULT hr = CompileShaderFromFile(shaderPath, "VS", "vs_5_0", &pVSBlob);
	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"Font shader cannot be compiled. Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return false;
	}
	hr = device->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &buffer);
	if (FAILED(hr))
	{
		pVSBlob->Release();
		return false;
	}
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	hr = device->CreateInputLayout(layout, 3, pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(),
		&surfaceLayout);
	pVSBlob->Release();
	if (FAILED(hr))
		return FALSE;
	return true;
}

bool ConstructPShader(ID3D11Device *device, TCHAR* shaderPath, ID3D11PixelShader *buffer)
{
	HRESULT hr = S_OK;
	ID3DBlob* pPSBlob = NULL;
	hr = CompileShaderFromFile(shaderPath, "PS", "ps_5_0", &pPSBlob);
	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return FALSE;
	}
	// Create the pixel shader
	hr = device->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &buffer);
	pPSBlob->Release();
	if (FAILED(hr))
		return FALSE;
	return true;
}

//--------------------------------------------------------------------------------------
// Register class and create window
//--------------------------------------------------------------------------------------
HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow)
{
	// Register class
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_TUTORIAL1);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = L"REMEMBER";
	wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_TUTORIAL1);
	if (!RegisterClassEx(&wcex))
		return E_FAIL;

	// Create window
	engine.hInst = hInstance;
	RECT rc = { 0, 0, SCRW, SCRH };
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
	engine.hWnd = CreateWindow(L"REMEMBER", L"REMEMBER", WS_OVERLAPPEDWINDOW,
		0, 0, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance,
		NULL);

	if (!engine.hWnd)
		return E_FAIL;

	ShowWindow(engine.hWnd, nCmdShow);

	srand(time(NULL));
}

ID3D11VertexShader *hudvs = NULL;
ID3D11PixelShader *hudps = NULL;
HRESULT FuckOff()
{
	// Load all our textures
	HRESULT hr = LoadMaterials(engine.pd3dDevice);

	if (FAILED(hr))
		return hr;

	Load("pryamidhead.3ds", engine.pd3dDevice, &engine.VertexBuffers["pyramidhead"].vertices, &engine.VertexBuffers["pyramidhead"].vertexCount);

	songs["music/pipe_ambience.wav"] = 27.0;
	songs["music/spook1.wav"] = 49.0;
	songs["music/spook2.wav"] = 45.0;
	songs["music/spook3.wav"] = 46.0;


	// Load all of our music
	vector<string> files;
}

//--------------------------------------------------------------------------------------
// Create Direct3D device and swap chain
//--------------------------------------------------------------------------------------
HRESULT InitDevice()
{
	HRESULT hr = S_OK;

	RECT rc;
	GetClientRect(engine.hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	UINT createDeviceFlags = 0;
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	UINT numDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	DXGI_SWAP_CHAIN_DESC sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.BufferCount = 1;
	sd.BufferDesc.Width = width;
	sd.BufferDesc.Height = height;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.BufferDesc.RefreshRate.Numerator = 60;
	sd.BufferDesc.RefreshRate.Denominator = 1;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.OutputWindow = engine.hWnd;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		engine.driverType = driverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(NULL, engine.driverType, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
			D3D11_SDK_VERSION, &sd, &engine.pSwapChain, &engine.pd3dDevice, &engine.featureLevel, &engine.pImmediateContext);
		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr))
		return hr;

	// Create a render target view
	ID3D11Texture2D* pBackBuffer = NULL;
	hr = engine.pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
	if (FAILED(hr))
		return hr;

	hr = engine.pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &engine.pRenderTargetView);
	pBackBuffer->Release();
	if (FAILED(hr))
		return hr;

	// Create depth stencil texture
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = width;
	descDepth.Height = height;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_R32_TYPELESS;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	hr = engine.pd3dDevice->CreateTexture2D(&descDepth, NULL, &engine.pDepthStencil);
	if (FAILED(hr))
		return hr;


	// Create the depth stencil view
	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = DXGI_FORMAT_D32_FLOAT;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;
	hr = engine.pd3dDevice->CreateDepthStencilView(engine.pDepthStencil, &descDSV, &engine.pDepthStencilView);
	if (FAILED(hr))
		return hr;


	// Setup the viewport
	viewport_normal.Width = (FLOAT)width;
	viewport_normal.Height = (FLOAT)height;
	viewport_normal.MinDepth = 0.0f;
	viewport_normal.MaxDepth = 1.0f;
	viewport_normal.TopLeftX = 0;
	viewport_normal.TopLeftY = 0;
	engine.pImmediateContext->RSSetViewports(1, &viewport_normal);

	viewport_octree.Width = (FLOAT)256;
	viewport_octree.Height = (FLOAT)144;
	viewport_octree.MinDepth = 0.0f;
	viewport_octree.MaxDepth = 1.0f;
	viewport_octree.TopLeftX = 0;
	viewport_octree.TopLeftY = 0;


	/////////// Shaders ///////////
	ID3DBlob* pVSBlob = NULL;
	ID3DBlob* pPSBlob = NULL;

	// Screen vertex shader
	pVSBlob = NULL;
	hr = CompileShaderFromFile(L"screen.fx", "VS", "vs_5_0", &pVSBlob);
	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}
	hr = engine.pd3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &g_pScreenVS);
	if (FAILED(hr))
	{
		pVSBlob->Release();
		return hr;
	}

	// Screen pixel shader
	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"screen.fx", "PS", "ps_5_0", &pPSBlob);
	if (FAILED(hr))
	{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}
	hr = engine.pd3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &g_pScreenPS);
	pPSBlob->Release();
	if (FAILED(hr))
		return hr;

	//////////////////////////////////////////g_pScreenPScompare




	// Define the input layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 44, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	// Create the input layout
	hr = engine.pd3dDevice->CreateInputLayout(layout, numElements, pVSBlob->GetBufferPointer(),
		pVSBlob->GetBufferSize(), &engine.pVertexLayout);
	pVSBlob->Release();

	if (FAILED(hr))
		return hr;

	// Set the input layout
	engine.pImmediateContext->IASetInputLayout(engine.pVertexLayout);


	// Create screen/one-sided square vertex buffer
	SimpleVertex vertices_screen[6];
	vertices_screen[0].load(XMFLOAT3(-1.0f, 1.0f, 0.0f), XMFLOAT2(0, 0), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices_screen[1].load(XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(1, 0), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices_screen[2].load(XMFLOAT3(-1.0f, -1.0f, 0.0f), XMFLOAT2(0, 1), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices_screen[3].load(XMFLOAT3(-1.0f, -1.0f, 0.0f), XMFLOAT2(0, 1), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices_screen[4].load(XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(1, 0), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices_screen[5].load(XMFLOAT3(1.0f, -1.0f, 0.0f), XMFLOAT2(1, 1), XMFLOAT3(0.0f, 0.0f, 1.0f));

	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(SimpleVertex) * 6;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = vertices_screen;
	hr = engine.pd3dDevice->CreateBuffer(&bd, &InitData, &g_pVertexBuffer);
	if (FAILED(hr))
		return hr;


	Load("table.3ds", engine.pd3dDevice, &engine.VertexBuffers["table"].vertices, &engine.VertexBuffers["table"].vertexCount);
	Load("bed.3DS", engine.pd3dDevice, &engine.VertexBuffers["bed"].vertices, &engine.VertexBuffers["bed"].vertexCount);
	Load("table2.3DS", engine.pd3dDevice, &engine.VertexBuffers["desk"].vertices, &engine.VertexBuffers["desk"].vertexCount);
	Load("dresser.3ds", engine.pd3dDevice, &engine.VertexBuffers["dresser"].vertices, &engine.VertexBuffers["dresser"].vertexCount);
	LoadOBJ("Wraith.obj", engine.pd3dDevice, &engine.VertexBuffers["wraith"].vertices, &engine.VertexBuffers["wraith"].vertexCount);
	Load("pryamidhead.3ds", engine.pd3dDevice, &engine.VertexBuffers["py"].vertices, &engine.VertexBuffers["py"].vertexCount);
	loadauh("walls.auh", engine.pd3dDevice, &engine.VertexBuffers["walls"].vertices, &engine.VertexBuffers["walls"].vertexCount);
	loadauh("floor.auh", engine.pd3dDevice, &engine.VertexBuffers["floor"].vertices, &engine.VertexBuffers["floor"].vertexCount);
	LoadCatmullClark(L"ccsphere.cmp", engine.pd3dDevice, &engine.VertexBuffers["sky"].vertices, &engine.VertexBuffers["sky"].vertexCount);


	// Create double-sided square vertex buffer
	SimpleVertex vertices[12];
	vertices[0].load(XMFLOAT3(-1.0f, -1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices[1].load(XMFLOAT3(1.0f, -1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices[2].load(XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices[3].load(XMFLOAT3(-1.0f, -1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices[4].load(XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices[5].load(XMFLOAT3(-1.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f));

	vertices[6].load(XMFLOAT3(1.0f, -1.0f, 0.0f), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices[7].load(XMFLOAT3(-1.0f, -1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices[8].load(XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices[9].load(XMFLOAT3(1.0f, 1.0f, 0.0f), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices[10].load(XMFLOAT3(-1.0f, -1.0f, 0.0f), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 1.0f));
	vertices[11].load(XMFLOAT3(-1.0f, 1.0f, 0.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f));

	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(SimpleVertex) * 12;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = vertices;
	hr = engine.pd3dDevice->CreateBuffer(&bd, &InitData, &g_pDoubleVertexBuffer);
	if (FAILED(hr))
		return hr;

	// Set vertex buffer
	UINT stride = sizeof(SimpleVertex);
	UINT offset = 0;
	engine.pImmediateContext->IASetVertexBuffers(0, 1, &g_pDoubleVertexBuffer, &stride, &offset);

	// Create constant buffer
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	hr = engine.pd3dDevice->CreateBuffer(&bd, NULL, &engine.pCBuffer);
	if (FAILED(hr))
		return hr;



	// Set primitive topology
	engine.pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	////////// Textures //////////

	FuckOff();


	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = engine.pd3dDevice->CreateSamplerState(&sampDesc, &engine.pSamplerLinear);
	if (FAILED(hr))
		return hr;

	// Create the screen sample state

	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = engine.pd3dDevice->CreateSamplerState(&sampDesc, &engine.SamplerScreen);
	if (FAILED(hr))
		return hr;

	// Initialize the world matrices
	World = XMMatrixIdentity();

	// Initialize the view matrices
	XMVECTOR Eye = XMVectorSet(0.0f, PLAYER.GetTall(), 0.0f, 0.0f);	//camera position
	XMVECTOR At = XMVectorSet(1.0f, PLAYER.GetTall(), 0.0f, 0.0f);		//look at
	XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);		//normal vector on at vector (always up)
	View = XMMatrixLookAtLH(Eye, At, Up);

	// what the fuck are all these matrices??? what are they doing???

	Eye = XMVectorSet(0.0f, 17.0f, -100.0f, 0.0f);	//camera position
	At = XMVectorSet(0.0f, 17, 0.0f, 0.0f);			//look at
	Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);		//normal vector on at vector (always up)
	g_View_front = XMMatrixLookAtLH(Eye, At, Up);

	Eye = XMVectorSet(0.0f, 100.0f, 0.0f, 0.0f);	//camera position
	At = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);		//look at
	Up = XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f);		//normal vector on at vector (always up)
	g_View_top = XMMatrixLookAtLH(Eye, At, Up);

	Eye = XMVectorSet(100.0f, 20.0f, 0.0f, 0.0f);	//camera position
	At = XMVectorSet(0.0f, 20.0f, 0.0f, 0.0f);		//look at
	Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);		//normal vector on at vector (always up)
	g_View_left = XMMatrixLookAtLH(Eye, At, Up);

	// Initialize the projection matrix
	Projection = XMMatrixPerspectiveFovLH(XM_PIDIV4, width / (FLOAT)height, 0.01f, 10000.0f);
	g_Projection2 = XMMatrixOrthographicLH(256, (FLOAT)144, -100.01f, 10000.0f) * XMMatrixScaling(2.8, 2.8, 2.8);

	ConstantBuffer constantbuffer;
	constantbuffer.View = XMMatrixTranspose(View);
	constantbuffer.Projection = XMMatrixTranspose(Projection);
	constantbuffer.World = XMMatrixTranspose(XMMatrixIdentity());
	constantbuffer.info = XMFLOAT4(1, 1, 1, 1);
	engine.pImmediateContext->UpdateSubresource(engine.pCBuffer, 0, NULL, &constantbuffer, 0, 0);

	//blendstate:
	D3D11_BLEND_DESC blendStateDesc;
	ZeroMemory(&blendStateDesc, sizeof(D3D11_BLEND_DESC));
	blendStateDesc.AlphaToCoverageEnable = FALSE;
	blendStateDesc.IndependentBlendEnable = FALSE;
	blendStateDesc.RenderTarget[0].BlendEnable = TRUE;
	blendStateDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendStateDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendStateDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendStateDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
	blendStateDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendStateDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendStateDesc.RenderTarget[0].RenderTargetWriteMask = 0x0F;
	engine.pd3dDevice->CreateBlendState(&blendStateDesc, &engine.BlendState);

	float blendFactor[] = { 0, 0, 0, 0 };
	UINT sampleMask = 0xffffffff;
	engine.pImmediateContext->OMSetBlendState(engine.BlendState, blendFactor, sampleMask);

	//create the depth stencil states for turning the depth buffer on and of:
	D3D11_DEPTH_STENCIL_DESC DS_ON, DS_OFF;
	DS_ON.DepthEnable = true;
	DS_ON.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	DS_ON.DepthFunc = D3D11_COMPARISON_LESS;
	// Stencil test parameters
	DS_ON.StencilEnable = true;
	DS_ON.StencilReadMask = 0xFF;
	DS_ON.StencilWriteMask = 0xFF;
	// Stencil operations if pixel is front-facing
	DS_ON.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	DS_ON.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	DS_ON.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	DS_ON.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Stencil operations if pixel is back-facing
	DS_ON.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	DS_ON.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	DS_ON.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	DS_ON.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	// Create depth stencil state
	DS_OFF = DS_ON;
	DS_OFF.DepthEnable = false;
	engine.pd3dDevice->CreateDepthStencilState(&DS_ON, &engine.ds_on);
	engine.pd3dDevice->CreateDepthStencilState(&DS_OFF, &engine.ds_off);

	//setting the rasterizer:
	D3D11_RASTERIZER_DESC RS_CW, RS_Wire;

	RS_CW.AntialiasedLineEnable = FALSE;
	RS_CW.CullMode = D3D11_CULL_BACK;
	RS_CW.DepthBias = 0;
	RS_CW.DepthBiasClamp = 0.0f;
	RS_CW.DepthClipEnable = true;
	RS_CW.FillMode = D3D11_FILL_SOLID;
	RS_CW.FrontCounterClockwise = false;
	RS_CW.MultisampleEnable = FALSE;
	RS_CW.ScissorEnable = false;
	RS_CW.SlopeScaledDepthBias = 0.0f;

	RS_Wire = RS_CW;
	RS_Wire.CullMode = D3D11_CULL_NONE;
	RS_Wire.FillMode = D3D11_FILL_WIREFRAME;
	engine.pd3dDevice->CreateRasterizerState(&RS_Wire, &engine.rs_Wire);
	engine.pd3dDevice->CreateRasterizerState(&RS_CW, &engine.rs_CW);

	//rendertarget texture
	engine.RenderToTexture.Initialize(engine.pd3dDevice, engine.hWnd, -1, -1, FALSE, DXGI_FORMAT_R8G8B8A8_UNORM, TRUE);

	SimpleVertex *landscape = new SimpleVertex[6 * 500 * 500];

	for (int zz = 0; zz < 500; zz++)
	{
		for (int xx = 0; xx < 500; xx++)
		{
			landscape[(xx + zz * 500) * 6 + 1].Pos = XMFLOAT3(0.f + xx, -1.f, 0.f + zz);
			landscape[(xx + zz * 500) * 6 + 0].Pos = XMFLOAT3(1.f + xx, -1.f, 0.f + zz);
			landscape[(xx + zz * 500) * 6 + 2].Pos = XMFLOAT3(1.f + xx, -1.f, 1.f + zz);
			landscape[(xx + zz * 500) * 6 + 4].Pos = XMFLOAT3(0.f + xx, -1.f, 0.f + zz);
			landscape[(xx + zz * 500) * 6 + 3].Pos = XMFLOAT3(1.f + xx, -1.f, 1.f + zz);
			landscape[(xx + zz * 500) * 6 + 5].Pos = XMFLOAT3(0.f + xx, -1.f, 1.f + zz);

			landscape[(xx + zz * 500) * 6 + 1].Tex = XMFLOAT2((0.f + xx) / 500, (0.f + zz) / 500);
			landscape[(xx + zz * 500) * 6 + 0].Tex = XMFLOAT2((1.f + xx) / 500, (0.f + zz) / 500);
			landscape[(xx + zz * 500) * 6 + 2].Tex = XMFLOAT2((1.f + xx) / 500, (1.f + zz) / 500);
			landscape[(xx + zz * 500) * 6 + 4].Tex = XMFLOAT2((0.f + xx) / 500, (0.f + zz) / 500);
			landscape[(xx + zz * 500) * 6 + 3].Tex = XMFLOAT2((1.f + xx) / 500, (1.f + zz) / 500);
			landscape[(xx + zz * 500) * 6 + 5].Tex = XMFLOAT2((0.f + xx) / 500, (1.f + zz) / 500);

			landscape[(xx + zz * 500) * 6 + 1].Normal = XMFLOAT3(0, 1, 0);
			landscape[(xx + zz * 500) * 6 + 0].Normal = XMFLOAT3(0, 1, 0);
			landscape[(xx + zz * 500) * 6 + 2].Normal = XMFLOAT3(0, 1, 0);
			landscape[(xx + zz * 500) * 6 + 4].Normal = XMFLOAT3(0, 1, 0);
			landscape[(xx + zz * 500) * 6 + 3].Normal = XMFLOAT3(0, 1, 0);
			landscape[(xx + zz * 500) * 6 + 5].Normal = XMFLOAT3(0, 1, 0);
		}
	}

	engine.CreateVertexBuffer("terrain", *landscape, 6 * 500 * 500);

	ui.init(engine.pd3dDevice, engine.pImmediateContext, ui.defaultFontMapDesc);

	// Initialize render pathways
	shadow_mapping.init(engine.hWnd, engine.pd3dDevice);
	vxgi.init(engine.hWnd, engine.pd3dDevice);
	SimpleVertex audioPlayer[6] = {};

	//bottom
	audioPlayer[0].Pos = XMFLOAT3(1.0f, 0.0f, -0.5f); audioPlayer[0].Tex = XMFLOAT2(1.0f, 0.0f);
	audioPlayer[1].Pos = XMFLOAT3(-1.0f, 0.0f, -0.5f); audioPlayer[1].Tex = XMFLOAT2(0.0f, 0.0f);
	audioPlayer[2].Pos = XMFLOAT3(1.0f, 0.0f, 0.5f); audioPlayer[2].Tex = XMFLOAT2(1.0f, 1.0f);
	audioPlayer[3].Pos = XMFLOAT3(1.0f, 0.0f, 0.5f); audioPlayer[3].Tex = XMFLOAT2(1.0f, 1.0f);
	audioPlayer[4].Pos = XMFLOAT3(-1.0f, 0.0f, -0.5f); audioPlayer[4].Tex = XMFLOAT2(0.0f, 0.0f);
	audioPlayer[5].Pos = XMFLOAT3(-1.0f, 0.0f, 0.5f); audioPlayer[5].Tex = XMFLOAT2(0.0f, 1.0f);


	engine.CreateVertexBuffer("audioPlayer", *audioPlayer, 6);

	ui.init(engine.pd3dDevice, engine.pImmediateContext, ui.defaultFontMapDesc);

	return S_OK;
}


//--------------------------------------------------------------------------------------
// Clean up the objects we've created
//--------------------------------------------------------------------------------------
void CleanupDevice()
{
	if (engine.pImmediateContext)
	{
		engine.pImmediateContext->ClearState();
		engine.pImmediateContext->Flush();
	}
	//shadow_mapping.CleanupDevice();
	//vxgi.CleanupDevice();

	if (engine.pSwapChain) engine.pSwapChain->Release();
	if (engine.pDepthStencil) engine.pDepthStencil->Release();
	if (engine.ds_on) engine.ds_on->Release();
	if (engine.ds_off) engine.ds_off->Release();
	if (g_pBlendState) g_pBlendState->Release();

	if (g_pRenderTargetView) g_pRenderTargetView->Release();
	if (engine.pDepthStencilView) engine.pDepthStencilView->Release();
	if (engine.pSamplerLinear) engine.pSamplerLinear->Release();

	if (g_pScreenVS) g_pScreenVS->Release();
	if (g_pScreenPS) g_pScreenPS->Release();

	if (engine.pVertexLayout) engine.pVertexLayout->Release();
	if (g_pVertexBuffer) g_pVertexBuffer->Release();
	if (g_pDoubleVertexBuffer) g_pDoubleVertexBuffer->Release();

	if (engine.pCBuffer) engine.pCBuffer->Release();
	if (g_pDebug) g_pDebug->Release();
	if (g_pInfoQueue) g_pInfoQueue->Release();
	if (engine.pImmediateContext) engine.pImmediateContext->Release();
	if (engine.pd3dDevice) engine.pd3dDevice->Release();
}

///////////////////////////////////
// Keyboard interaction functions
///////////////////////////////////
void OnLBD(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags) { }
void OnRBD(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags) { }
void OnChar(HWND hwnd, UINT ch, int cRepeat) { }
void OnLBU(HWND hwnd, int x, int y, UINT keyFlags) { }
void OnRBU(HWND hwnd, int x, int y, UINT keyFlags) { }

int plane;

void mHideCursor() 	//hides cursor
{
	while (plane >= 0)
		plane = ShowCursor(FALSE);
}

void mShowCursor() 	//shows it again
{
	while (plane<0)
		plane = ShowCursor(TRUE);
}

void OnMM(HWND hwnd, int x, int y, UINT keyFlags)
{
	static int holdx = x, holdy = y;
	static int reset_cursor = 0;

	RECT rc; 			//rectange structure
	GetWindowRect(hwnd, &rc); 	//retrieves the window size
	int border = 20;
	rc.bottom -= border;
	rc.right -= border;
	rc.left += border;
	rc.top += border;
	ClipCursor(&rc);

	if ((keyFlags & MK_LBUTTON) == MK_LBUTTON)
	{
	}

	if ((keyFlags & MK_RBUTTON) == MK_RBUTTON)
	{
	}
	if (reset_cursor == 1)
	{
		reset_cursor = 0;
		holdx = x;
		holdy = y;
		return;
	}
	int diffx = holdx - x;
	int diffy = holdy - y;
	float angle_y = (float)diffx * MOUSESENSE_Y;
	float angle_x = (float)diffy * MOUSESENSE_X;

	QAngle eyeAng = PLAYER.GetEyeAngles();

	PLAYER.SetEyeComponent(1, NormalizeAngle(eyeAng.y + angle_y));
	PLAYER.SetEyeComponent(0, Clamp(eyeAng.x - angle_x, -89.0f, 89.0f));

	int midx = (rc.left + rc.right) / 2;
	int midy = (rc.top + rc.bottom) / 2;
	SetCursorPos(midx, midy);
	reset_cursor = 1;

	mHideCursor();
}

BOOL OnCreate(HWND hwnd, CREATESTRUCT FAR* lpCreateStruct)
{
	RECT rc; 			//rectange structure
	GetWindowRect(hwnd, &rc); 	//retrieves the window size
	int border = 5;
	rc.bottom -= border;
	rc.right -= border;
	rc.left += border;
	rc.top += border;
	ClipCursor(&rc);
	int midx = (rc.left + rc.right) / 2;
	int midy = (rc.top + rc.bottom) / 2;
	SetCursorPos(midx, midy);

	SetTimer(hwnd, 10, 0.00001, (TIMERPROC)engine.OnTimer);

	return TRUE;
}
//---------------------------------------
void OnTimer(HWND hwnd, UINT id)
{
	//engine.OnTimer(hwnd, id);
}
//*************************************************************************


unsigned int KEYS = 0x0;

int key_i = 0;
int key_j = 0;
int key_k = 0;
int key_l = 0;

void OnKeyUp(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags)
{
	PLAYER.KeyRelease(vk, fDown, cRepeat, flags);

	switch (vk)
	{
	case 81:
		break;
	case 69://e
		break;
	case KEY_A:
		KEYS &= ~(IN_LEFT);
		break;
	case KEY_D:
		KEYS &= ~(IN_RIGHT);
		break;
	case KEY_SPACE:
		KEYS &= ~(IN_JUMP);
		break;
	case KEY_W:
		KEYS &= ~(IN_FORWARD);
		break;
	case KEY_S:
		KEYS &= ~(IN_BACK);
		break;
	case 0x49:
		key_i = 0;
		break;
	case 0x4A: key_j = 0; break;
	case 0x4B: key_k = 0; break;
	case 0x4C: key_l = 0; break;
	default:
		break;

	}
}

void OnKeyDown(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags)
{
	PLAYER.KeyPress(vk, fDown, cRepeat, flags);

	switch (vk)
	{
	case KEY_A:
		KEYS |= IN_LEFT;
		break;
	case KEY_D:
		KEYS |= IN_RIGHT;
		break;
	case KEY_SPACE:
		KEYS |= IN_JUMP;
		break;
	case KEY_W:
		KEYS |= IN_FORWARD;
		break;
	case KEY_S:
		KEYS |= IN_BACK;
		break;
	case KEY_ESC:
		PostQuitMessage(0);//escape
		break;
	case 0x49: key_i = 1; break;
	case 0x4A: key_j = 1; break;
	case 0x4B: key_k = 1; break;
	case 0x4C: key_l = 1; break;

	case 'V':
		voxeldraw = !(voxeldraw);
		break;

	default:
		break;
	}
}

//--------------------------------------------------------------------------------------
// Called every time the application receives a message
//--------------------------------------------------------------------------------------
#include <windowsx.h>
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, OnLBD);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, OnLBU);
		HANDLE_MSG(hWnd, WM_MOUSEMOVE, OnMM);
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_TIMER, OnTimer);
		HANDLE_MSG(hWnd, WM_KEYDOWN, OnKeyDown);
		HANDLE_MSG(hWnd, WM_KEYUP, OnKeyUp);
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}


//************************************************************
void Render_world(ID3D11DeviceContext* pImmediateContext, float elapsed, ID3D11Buffer* pCBuffer, ConstantBuffer &cb)
{

	UINT stride = sizeof(SimpleVertex);
	UINT offset = 0;

	//only change cb.World !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	XMMATRIX worldMatrix = XMMatrixIdentity();


	cb.World = XMMatrixTranspose(worldMatrix);
	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);
	pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
	//***********************************************************************************

	// house start

	XMMATRIX E = XMMatrixIdentity();

	E = E * XMMatrixScaling(2, 2, 2);
	E = E * XMMatrixTranslation(-2000, 0, -2000);

	cb.World = XMMatrixTranspose(E);

	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);


	//Create House walls
	VertexBufferData wallData = engine.GetVertexBuffer("walls");



	pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetShaderResources(0, 1, Material("wall_tex"));
	pImmediateContext->VSSetShaderResources(0, 1, Material("wall_tex"));

	pImmediateContext->IASetVertexBuffers(0, 1, &wallData.vertices, &stride, &offset);
	pImmediateContext->PSSetSamplers(0, 1, &engine.pSamplerLinear);
	pImmediateContext->VSSetSamplers(0, 1, &engine.pSamplerLinear);

	pImmediateContext->OMSetDepthStencilState(engine.ds_on, 1);
	pImmediateContext->Draw(wallData.vertexCount, 0);



	// Create house floor
	VertexBufferData floorData = engine.GetVertexBuffer("floor");



	pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetShaderResources(0, 1, Material("floor_tex"));
	pImmediateContext->VSSetShaderResources(0, 1, Material("floor_tex"));

	pImmediateContext->IASetVertexBuffers(0, 1, &floorData.vertices, &stride, &offset);
	pImmediateContext->PSSetSamplers(0, 1, &engine.pSamplerLinear);
	pImmediateContext->VSSetSamplers(0, 1, &engine.pSamplerLinear);

	pImmediateContext->OMSetDepthStencilState(engine.ds_on, 1);
	pImmediateContext->Draw(floorData.vertexCount, 0);


	E = E * XMMatrixTranslation(0, 300, 0);

	cb.World = XMMatrixTranspose(E);

	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);


	VertexBufferData ceilingData = engine.GetVertexBuffer("floor");



	pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetShaderResources(0, 1, Material("wall_tex"));
	pImmediateContext->VSSetShaderResources(0, 1, Material("wall_tex"));

	pImmediateContext->IASetVertexBuffers(0, 1, &ceilingData.vertices, &stride, &offset);
	pImmediateContext->PSSetSamplers(0, 1, &engine.pSamplerLinear);
	pImmediateContext->VSSetSamplers(0, 1, &engine.pSamplerLinear);

	pImmediateContext->OMSetDepthStencilState(engine.ds_on, 1);
	pImmediateContext->Draw(ceilingData.vertexCount, 0);

	// house end

	E = XMMatrixIdentity();
	//E = E * XMMatrixRotationX(XM_PIDIV2);
	//E = E * XMMatrixRotationZ(XM_PI);
	E = E * XMMatrixTranslation(210, 65, 0);
	E = XMMatrixScaling(10, 10, 10) * E;


	cb.World = XMMatrixTranspose(E);

	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);



	if (PLAYER.playerCount[0] <= 0) {

		VertexBufferData objData = engine.GetVertexBuffer("audioPlayer");



		pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
		pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
		pImmediateContext->PSSetShaderResources(0, 1, Material("phone_texture"));
		pImmediateContext->VSSetShaderResources(0, 1, Material("phone_texture"));

		pImmediateContext->IASetVertexBuffers(0, 1, &objData.vertices, &stride, &offset);
		pImmediateContext->PSSetSamplers(0, 1, &engine.pSamplerLinear);
		pImmediateContext->VSSetSamplers(0, 1, &engine.pSamplerLinear);

		pImmediateContext->OMSetDepthStencilState(engine.ds_on, 1);
		pImmediateContext->Draw(objData.vertexCount, 0);

	}

	E = E * XMMatrixTranslation(-700, 25, -650);


	cb.World = XMMatrixTranspose(E);

	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);

	if (PLAYER.playerCount[1] <= 0) {

		VertexBufferData objData = engine.GetVertexBuffer("audioPlayer");



		pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
		pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
		pImmediateContext->PSSetShaderResources(0, 1, Material("phone_texture"));
		pImmediateContext->VSSetShaderResources(0, 1, Material("phone_texture"));

		pImmediateContext->IASetVertexBuffers(0, 1, &objData.vertices, &stride, &offset);
		pImmediateContext->PSSetSamplers(0, 1, &engine.pSamplerLinear);
		pImmediateContext->VSSetSamplers(0, 1, &engine.pSamplerLinear);

		pImmediateContext->OMSetDepthStencilState(engine.ds_on, 1);
		pImmediateContext->Draw(objData.vertexCount, 0);

	}


	E = E * XMMatrixTranslation(1500, -25, -650);


	cb.World = XMMatrixTranspose(E);

	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);


	if (PLAYER.playerCount[2] <= 0) {

		VertexBufferData objData = engine.GetVertexBuffer("audioPlayer");



		pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
		pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
		pImmediateContext->PSSetShaderResources(0, 1, Material("phone_texture"));
		pImmediateContext->VSSetShaderResources(0, 1, Material("phone_texture"));

		pImmediateContext->IASetVertexBuffers(0, 1, &objData.vertices, &stride, &offset);
		pImmediateContext->PSSetSamplers(0, 1, &engine.pSamplerLinear);
		pImmediateContext->VSSetSamplers(0, 1, &engine.pSamplerLinear);

		pImmediateContext->OMSetDepthStencilState(engine.ds_on, 1);
		pImmediateContext->Draw(objData.vertexCount, 0);

	}





	if (PLAYER.playerCount[2] && PLAYER.playerCount[1] && PLAYER.playerCount[0])
	{
		static bool bSeesWraith = false;
		//E = E * XMMatrixRotationX(XM_PIDIV2);
		//E = E * XMMatrixRotationZ(XM_PI);

		Vector wraithPos = PLAYER.GetPos();
		wraithPos.x -= 300;
		wraithPos.z += 200;
		wraithPos.y -= 200;

		Vector wraithToEyeNorm = wraithPos - PLAYER.GetEyePos();
		VectorNormalize(wraithToEyeNorm);

		QAngle wraithAng;
		VectorAngles(wraithToEyeNorm, wraithAng);

		XMMATRIX wraithRotate = XMMatrixRotationY(DEG2RAD(wraithAng.y) + XM_PIDIV2);
		XMMATRIX wraithTranslate = XMMatrixTranslation(wraithPos.x, wraithPos.y, wraithPos.z);
		XMMATRIX wraithScale = XMMatrixScaling(100, 100, 100);

		XMMATRIX wraithMatrix = wraithScale * wraithRotate * wraithTranslate;

		cb.World = XMMatrixTranspose(wraithMatrix);

		pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);

		VertexBufferData wraithData = engine.GetVertexBuffer("wraith");

		pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
		pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
		pImmediateContext->PSSetShaderResources(0, 1, Material("wraith_tex_test"));
		pImmediateContext->VSSetShaderResources(0, 1, Material("wraith_tex_test"));

		pImmediateContext->IASetVertexBuffers(0, 1, &wraithData.vertices, &stride, &offset);
		pImmediateContext->PSSetSamplers(0, 1, &engine.pSamplerLinear);
		pImmediateContext->VSSetSamplers(0, 1, &engine.pSamplerLinear);

		pImmediateContext->OMSetDepthStencilState(engine.ds_on, 1);
		pImmediateContext->Draw(wraithData.vertexCount, 0);

		Vector eyeForward;
		AngleVectorsXM(PLAYER.GetEyeAngles(), &eyeForward);

		float wraithDot = wraithToEyeNorm.Dot(eyeForward);
		static int bGlitch = 0;

		if (!bSeesWraith && wraithDot >= 0.6)
		{
			int rand = random(0, 1);

			if (rand)
			{
				audio_args_ *info = new audio_args_;
				info->file = L"stingers/scare_light02.wav";
				info->volume = -1000;

				start_music(info);
			}
			else
			{
				audio_args_ *info = new audio_args_;
				info->file = L"stingers/scare_intense.wav";
				info->volume = -1000;

				start_music(info);
			}

			Timer::Simple(2, []{PostQuitMessage(0);});

			bSeesWraith = true;
			GLITCH = true;
		}
	}


	E = XMMatrixIdentity();
	E = E * XMMatrixRotationX(XM_PIDIV2);
	E = E * XMMatrixRotationZ(XM_PI);
	E = E * XMMatrixTranslation(250, 20, 0);
	E = XMMatrixScaling(100, 100, 100) * E;


	cb.World = XMMatrixTranspose(E);

	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);



	VertexBufferData tableData = engine.GetVertexBuffer("table");



	pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetShaderResources(0, 1, Material("wood"));
	pImmediateContext->VSSetShaderResources(0, 1, Material("wood"));

	pImmediateContext->IASetVertexBuffers(0, 1, &tableData.vertices, &stride, &offset);
	pImmediateContext->PSSetSamplers(0, 1, &engine.pSamplerLinear);
	pImmediateContext->VSSetSamplers(0, 1, &engine.pSamplerLinear);

	pImmediateContext->OMSetDepthStencilState(engine.ds_on, 1);
	pImmediateContext->Draw(tableData.vertexCount, 0);




	E = XMMatrixIdentity();
	E = E * XMMatrixRotationX(XM_PIDIV2);
	E = E * XMMatrixRotationZ(XM_PI);
	E = E * XMMatrixTranslation(-475, 50, -650);
	E = XMMatrixScaling(0.1, 0.1, 0.1) * E;


	cb.World = XMMatrixTranspose(E);

	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);

	VertexBufferData dresserData = engine.GetVertexBuffer("dresser");



	pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetShaderResources(0, 1, Material("wood"));
	pImmediateContext->VSSetShaderResources(0, 1, Material("wood"));

	pImmediateContext->IASetVertexBuffers(0, 1, &dresserData.vertices, &stride, &offset);
	pImmediateContext->PSSetSamplers(0, 1, &engine.pSamplerLinear);
	pImmediateContext->VSSetSamplers(0, 1, &engine.pSamplerLinear);

	pImmediateContext->OMSetDepthStencilState(engine.ds_on, 1);
	pImmediateContext->Draw(dresserData.vertexCount, 0);

	E = XMMatrixIdentity();
	E = E * XMMatrixScaling(2, 3, 2);
	E = XMMatrixRotationX(-XM_PIDIV2);
	E = E * XMMatrixTranslation(1000, 0, -1300);
	cb.World = XMMatrixTranspose(E);

	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);

	VertexBufferData deskData = engine.GetVertexBuffer("desk");



	pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetShaderResources(0, 1, Material("wood"));
	pImmediateContext->VSSetShaderResources(0, 1, Material("wood"));

	pImmediateContext->IASetVertexBuffers(0, 1, &deskData.vertices, &stride, &offset);
	pImmediateContext->PSSetSamplers(0, 1, &engine.pSamplerLinear);
	pImmediateContext->VSSetSamplers(0, 1, &engine.pSamplerLinear);

	pImmediateContext->OMSetDepthStencilState(engine.ds_on, 1);
	pImmediateContext->Draw(deskData.vertexCount, 0);


	//Draw the sky

	XMMATRIX worldmatrix = XMMatrixIdentity();

	worldmatrix = worldmatrix * XMMatrixScaling(100, 100, 100);
	worldmatrix = worldmatrix * XMMatrixTranslation(-PLAYER.GetPos().x, -PLAYER.GetPos().y, -PLAYER.GetPos().z);
	cb.World = XMMatrixTranspose(worldmatrix);
	pImmediateContext->UpdateSubresource(pCBuffer, 0, NULL, &cb, 0, 0);


	VertexBufferData sky = engine.GetVertexBuffer("sky");



	pImmediateContext->VSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetConstantBuffers(0, 1, &pCBuffer);
	pImmediateContext->PSSetShaderResources(0, 1, Material("skysphere"));
	pImmediateContext->VSSetShaderResources(0, 1, Material("skysphere"));

	pImmediateContext->IASetVertexBuffers(0, 1, &sky.vertices, &stride, &offset);
	pImmediateContext->PSSetSamplers(0, 1, &engine.pSamplerLinear);
	pImmediateContext->VSSetSamplers(0, 1, &engine.pSamplerLinear);


	pImmediateContext->Draw(sky.vertexCount, 0);
	pImmediateContext->OMSetDepthStencilState(engine.ds_on, 1);
}

unsigned int stageDB[BUFFERSIZE]; // DebugBufer
								  //############################################################################################################
//void Render_to_screen(long elapsed)
//{
//
//	ConstantBuffer constantbuffer;
//	XMMATRIX view = PLAYER.GetViewMatrix(&View);
//	UINT stride = sizeof(SimpleVertex);
//	UINT offset = 0;
//
//	Vector vecPlayerPos = PLAYER.GetEyePos();
//	QAngle eyeAng = PLAYER.GetEyeAngles();
//
//	Vector forward;
//
//	AngleVectorsXM(eyeAng, &forward);
//
//	constantbuffer.View = XMMatrixTranspose(view);
//	constantbuffer.Projection = XMMatrixTranspose(Projection);
//	constantbuffer.CameraPos = XMFLOAT4(vecPlayerPos.x, vecPlayerPos.y, vecPlayerPos.z, 1);
//	constantbuffer.info = XMFLOAT4(forward.x, forward.y, forward.z, 0);
//
//	engine.pImmediateContext->OMSetRenderTargets(1, &engine.pRenderTargetView, engine.pDepthStencilView);
//	// Clear the back buffer
//	float ClearColor2[4] = { 0.0f, 0.0f, 0.0f, 1.0f }; // red, green, blue, alpha
//
//	engine.pImmediateContext->ClearRenderTargetView(engine.pRenderTargetView, ClearColor2);
//	// Clear the depth buffer to 1.0 (max depth)
//	engine.pImmediateContext->ClearDepthStencilView(engine.pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
//
//	constantbuffer.World = XMMatrixIdentity();
//
//	// Render screen
//	//typicall sign of wrong loading
//
//	engine.pImmediateContext->VSSetShader(pVertexShader_screen, NULL, 0);
//	engine.pImmediateContext->PSSetShader(pPixelShader_screen, NULL, 0);
//	engine.pImmediateContext->VSSetConstantBuffers(0, 1, &engine.pCBuffer);
//	engine.pImmediateContext->PSSetConstantBuffers(0, 1, &engine.pCBuffer);
//
//	ID3D11ShaderResourceView* texture = engine.RenderToTexture.GetShaderResourceView();// THE MAGIC
//	ID3D11ShaderResourceView* depthtexture = LightTexture.GetShaderResourceView();// THE MAGIC
//
//																				  //texture = depthtexture;
//
//	engine.pImmediateContext->PSSetShaderResources(0, 1, &texture);
//
//
//	engine.pImmediateContext->IASetVertexBuffers(0, 1, &g_pVertexBuffer_screen, &stride, &offset);
//	engine.pImmediateContext->PSSetSamplers(0, 1, &engine.SamplerScreen);
//	engine.pImmediateContext->VSSetSamplers(0, 1, &engine.SamplerScreen);
//	engine.pImmediateContext->UpdateSubresource(engine.pCBuffer, 0, NULL, &constantbuffer, 0, 0);
//
//
//	engine.pImmediateContext->Draw(6, 0);
//	engine.pImmediateContext->OMSetDepthStencilState(engine.ds_on, 1);
//	engine.pSwapChain->Present(0, 0);
//}

void Render_to_screen(long elapsed)
{
	engine.pImmediateContext->RSSetViewports(1, &viewport_normal);

	float ClearColor[4] = { 0.0f, 1.0f, 0.0f, 1.0f }; // red, green, blue, alpha
	ConstantBuffer constantbuffer;
	XMMATRIX view = PLAYER.GetViewMatrix(&View);
	UINT stride = sizeof(SimpleVertex);
	UINT offset = 0;

	Vector position = PLAYER.GetEyePos();
	QAngle eyeAng = PLAYER.GetEyeAngles();

	Vector forward;

	AngleVectorsXM(eyeAng, &forward);

	constantbuffer.View = XMMatrixTranspose(view);
	constantbuffer.Projection = XMMatrixTranspose(Projection);
	constantbuffer.CameraPos = position;

	// Clear render target & shaders, set render target & primitive topology
	engine.pImmediateContext->ClearRenderTargetView(engine.pRenderTargetView, ClearColor);
	engine.pImmediateContext->ClearDepthStencilView(engine.pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0, 0);
	engine.pImmediateContext->OMSetRenderTargets(1, &engine.pRenderTargetView, engine.pDepthStencilView);
	engine.pImmediateContext->VSSetShader(NULL, NULL, 0);
	engine.pImmediateContext->PSSetShader(NULL, NULL, 0);
	engine.pImmediateContext->GSSetShader(NULL, NULL, 0);
	engine.pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	ID3D11ShaderResourceView* texture = vxgi.get_output()->GetShaderResourceView();
	//ID3D11ShaderResourceView* texture = vxgi.get_positions()->GetShaderResourceView();
	//texture = shadow_mapping.get_output()->GetShaderResourceView();
	//texture = g_pSunTex;

	static double first = RunTime();
	static int bShowTitle = 0;
	static int titleDelay = 2;
	double elapsedTime = RunTime() - first;

	static bool bTitleSound = false;

	if (!bTitleSound && elapsedTime >= titleDelay)
	{
		PlayMusic("music/spook_intro.wav", true);
		bTitleSound = true;
		bShowTitle = 1;
	}

	static float glitchRand = randomf(0, 1);
	float glitchFrac = ((elapsedTime - titleDelay) / 5.0f);

	if (Mod(elapsedTime, 0.2f) <= 0.05f)
		glitchRand = randomf(0.0f, 1.0f);

	constantbuffer.World = XMMatrixIdentity();
	constantbuffer.info.x = (GLITCH ? 1 : glitchFrac);
	constantbuffer.info.y = random(1, 4);
	constantbuffer.info.z = glitchRand;
	constantbuffer.info.w = bShowTitle;
	engine.pImmediateContext->UpdateSubresource(engine.pCBuffer, 0, NULL, &constantbuffer, 0, 0);

	engine.pImmediateContext->VSSetShader(g_pScreenVS, NULL, 0);
	engine.pImmediateContext->VSSetConstantBuffers(0, 1, &engine.pCBuffer);
	engine.pImmediateContext->VSSetSamplers(0, 1, &engine.pSamplerLinear);

	engine.pImmediateContext->PSSetShader(g_pScreenPS, NULL, 0);
	engine.pImmediateContext->PSSetShaderResources(0, 1, &texture);
	engine.pImmediateContext->PSSetShaderResources(1, 1, Material("title"));

	engine.pImmediateContext->PSSetConstantBuffers(0, 1, &engine.pCBuffer);
	engine.pImmediateContext->PSSetSamplers(0, 1, &engine.pSamplerLinear);

	engine.pImmediateContext->IASetVertexBuffers(0, 1, &g_pVertexBuffer, &stride, &offset);
	engine.pImmediateContext->Draw(6, 0);

	// Let's count our glorious FPS
	string text;
	static int fps = 1.0f / FrameTime();
	static double lastFPSpoll = RunTime();

	if (RunTime() > (lastFPSpoll + 0.5))
	{
		fps = 1.0f / FrameTime();
		lastFPSpoll = RunTime();
	}

	text = to_string(fps);

	text = "FPS: " + text;

	// Upwards of 60 FPS is green, while anything under slowly interpolates to red, 0 being complete red
	float frac = Clamp(fps / 60.0f, 0.0f, 1.0f);
	float red = 1.0f - frac;
	float green = frac;

	ui.setScaling(XMFLOAT3(0.7, 0.7, 0.7));
	ui.setColor(XMFLOAT3(red, green, 0));
	ui.setPosition(XMFLOAT3(-1, 1, 0));
	ui << text;

	tostring(PLAYER.GetEyeAngles(), text);

	text = "ANG: " + text;

	ui.setColor(XMFLOAT3(1, 1, 1));
	ui.setPosition(XMFLOAT3(-1, 1 - 0.05, 0));
	ui << text;

	tostring(PLAYER.GetPos(), text);

	text = "POS: " + text;


	ui.setPosition(XMFLOAT3(-1, 1 - 0.05 * 2, 0));
	ui << text;

	tostring(PLAYER.GetVelocity(), text);

	text = "VEL: " + text;

	ui.setPosition(XMFLOAT3(-1, 1 - 0.05 * 3, 0));
	ui << text;

	tostring(PLAYER.GetEyePos(), text);

	text = "EYEPOS: " + text;

	ui.setPosition(XMFLOAT3(-1, 1 - 0.05 * 4, 0));
	ui << text;

	engine.pSwapChain->Present(0, 0);
	engine.pImmediateContext->IASetInputLayout(engine.pVertexLayout);
	ID3D11ShaderResourceView* srvs[D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT] = { 0 };
	engine.pImmediateContext->VSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);
	engine.pImmediateContext->PSSetShaderResources(0, D3D10_COMMONSHADER_INPUT_RESOURCE_SLOT_COUNT, srvs);
}
//############################################################################################################

bool bInitialized = false;

void Render()
{
	static float rxw = 0, rzw = 0;
	if (key_i)
	{
		rxw += 0.04;
	}
	if (key_k)
	{
		rxw -= 0.04;
	}
	if (key_j)
	{
		rzw += 0.04;
	}
	if (key_l)
	{
		rzw -= 0.04;
	}

	shadow_mapping.set_x_rot(rxw);
	shadow_mapping.set_z_rot(rzw);

	double curTime = RunTime();

	frametime = curTime - framecheck;
	framecheck = curTime;

	PLAYER.SetButtons(KEYS);

	PLAYER.Think(frametime);

	Vector eyePos = PLAYER.GetEyePos();
	QAngle eyeAng = PLAYER.GetEyeAngles();
	Vector forward, right, up;

	AngleVectorsXM(eyeAng, &forward, &right, &up);

	Vector at = eyePos + forward;

	XMVECTOR Eye = XMVectorSet(eyePos.x, eyePos.y, eyePos.z, 0.0f);	//camera position
	XMVECTOR At = XMVectorSet(at.x, at.y, at.z, 0.0f);		//look at
	XMVECTOR Up = XMVectorSet(up.x, up.y, up.z, 0.0f);		//normal vector on at vector (always up)

	shadow_mapping.View_light = XMMatrixLookAtLH(Eye, At, Up);

	Vector pos = PLAYER.GetEyePos();

	XMMATRIX worldMatrix = PLAYER.GetViewMatrix(&View);

	engine.pImmediateContext->IASetInputLayout(engine.pVertexLayout);

	static int staticScene = 0;
	if (staticScene == 0)
	{
		shadow_mapping.render_into_shadow_map(vxgi.get_output()->GetRenderTargetView(), engine.pImmediateContext, engine.pSwapChain, worldMatrix, 0.0, Render_world);

		vxgi.reset_all(engine.pImmediateContext);

		vxgi.render_1_of_8_render_into_voxel_fragment_list(engine.pImmediateContext, engine.pSwapChain, worldMatrix, 0.0, &shadow_mapping, Render_world);
		vxgi.render_2_of_8_build_octree(engine.pImmediateContext);
		vxgi.render_3_of_8_collect_kids_color(engine.pImmediateContext);
		vxgi.render_4_of_8_build_neighbor_info(engine.pImmediateContext);

		vxgi.render_5_of_8_exchange_color(engine.pImmediateContext);

		if (isStatic) staticScene++;
	}
	//if (breakpoint) debugCS();
	//press 'b' to use break point


	vxgi.render_6_of_8_render_scene_into_RT(engine.pImmediateContext, &PLAYER, engine.pSwapChain, worldMatrix, PLAYER.GetViewMatrix(&View), Projection, Render_world);
	vxgi.render_7_of_8_deferred_shadowing(engine.pImmediateContext, &PLAYER, engine.pSwapChain, worldMatrix, PLAYER.GetViewMatrix(&View), Projection, &shadow_mapping);

	vxgi.render_8_of_8_cone_tracing(engine.pImmediateContext, &PLAYER, engine.pSwapChain, worldMatrix, PLAYER.GetViewMatrix(&View), Projection, &shadow_mapping, voxeldraw);

	Render_to_screen(frametime);

	//engine.pImmediateContext->IASetInputLayout(surfaceLayout);

	//CSurfaceBuffer buffer;
	//buffer.info.x = 1;
	//buffer.info.y = 1;
	//buffer.info.z = 1;
	//buffer.info.w = 1;

	//engine.pImmediateContext->UpdateSubresource(engine.pCBuffer, 0, NULL, &buffer, 0, 0);

	//UINT stride = sizeof(SimpleVertex);
	//UINT offset = 0;

	//engine.pImmediateContext->VSSetShader(hudvs, NULL, 0);
	//engine.pImmediateContext->VSSetConstantBuffers(0, 1, &engine.pCBuffer);
	//engine.pImmediateContext->VSSetSamplers(0, 1, &engine.pSamplerLinear);

	//engine.pImmediateContext->PSSetShader(hudps, NULL, 0);
	//engine.pImmediateContext->PSSetShaderResources(0, 1, Material("grass"));


	//engine.pImmediateContext->PSSetConstantBuffers(0, 1, &engine.pCBuffer);
	//engine.pImmediateContext->PSSetSamplers(0, 1, &engine.pSamplerLinear);

	//engine.pImmediateContext->IASetVertexBuffers(0, 1, &g_pVertexBuffer, &stride, &offset);
	//engine.pImmediateContext->Draw(6, 0);

	//engine.pSwapChain->Present(0, 0);

	// Ambient thunder

	static double flNextThunder = 0.0;

	if (curTime >= flNextThunder)
	{
		// It's time to play some spooky thunder!
		// Should probably cache the wchar version of the file, but oh well...

		int sndNum;

		string text = "thunder_";

		if (rand())
		{
			text += "distant0";
			sndNum = rand() % 2 + 1;
		}
		else
		{
			text += "close0";
			sndNum = rand() % 3 + 1;
		}

		text += to_string(sndNum);
		text += ".wav";

		start_music(GetWC(&text[0u]), -5000);

		float nextTime = randomf(10.0f, 40.0f);
		flNextThunder += nextTime;
	}

	if (!isStatic) staticScene = 0;
}
