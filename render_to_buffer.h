#include "xmath.h"

//////////////////////////////////////
class BufferClass
	{
	private:
		ID3D11Buffer*				m_buffer;
		ID3D11Buffer*				m_staging;
		ID3D11RenderTargetView*		m_renderTargetView;
		ID3D11ShaderResourceView*	m_shaderResourceView;
		ID3D11UnorderedAccessView*  m_unorderedAccessView;
		bool rtv;
		bool srv;
		bool uav;

	public:
		int w;
		BufferClass()
			{
			uav = FALSE;
			rtv = FALSE;
			srv = FALSE;
			m_buffer = NULL;
			m_staging = NULL;
			m_renderTargetView = NULL;
			m_shaderResourceView = NULL;
			m_unorderedAccessView = NULL;
			w = 0;
			}
		BufferClass(const BufferClass&) {}
		~BufferClass() { ResetBuffersAndViews(); }
		void ResetBuffersAndViews();

		bool Initialize_buffer(ID3D11Device* device, UINT elementSize = sizeof(unsigned int), int count = -1, bool rtv_ = FALSE, bool srv_ = TRUE, bool uav_ = TRUE, bool regenerate = FALSE);
		bool Initialize_staging(ID3D11Device* device);

		ID3D11Buffer* GetBuffer()								{ return m_buffer; }
		ID3D11RenderTargetView* GetRenderTargetView()			{ return m_renderTargetView; }
		ID3D11ShaderResourceView* GetShaderResourceView()		{ return m_shaderResourceView; }
		ID3D11UnorderedAccessView*  GetUnorderedAccessView()	{ return m_unorderedAccessView; }
		ID3D11Buffer* GetStagingBuffer(ID3D11DeviceContext* context = NULL)
			{
			if (!m_buffer) return NULL;
			if (!m_staging)
				{
				ID3D11Device* device = NULL;
				context->GetDevice(&device);
				Initialize_staging(device);
				}
			if (context) context->CopyResource(m_staging, m_buffer);
			return m_staging;
			}
	};