#include "shaderheader.fxh"

RWStructuredBuffer<uint> Octree : register(u1);
RWStructuredBuffer<float3> VFL : register(u2);
RWStructuredBuffer<float4> VFLcolors : register(u3);
RWStructuredBuffer<uint> count : register(u4);
RWTexture3D<float> BrickBuffer : register(u5);
RWStructuredBuffer<uint> NeighborBuffer : register(u6);
RWStructuredBuffer<uint> CSDebugBuffer : register(u7);

cbuffer ConstantBuffer : register(b0)
	{
	float4 CBvalues;
	};

[numthreads(NUM_THREADS, 1, 1)]
void CSclear(uint3 DTid : SV_DispatchThreadID)
	{
	uint numpassesflag = (uint)ceil((float)max(BUFFERSIZE, BUFFERSIZE_NB) / (float)NUM_THREADS);

	for (uint i = 0; i < numpassesflag; i++)
		{
		uint index = DTid.x + NUM_THREADS*i;
		if (index < COUNTSIZE)
			count[index] = 0;
		if (index < BUFFERSIZE)
			{
			Octree[index] = 0;
			VFL[index] = float3(0, 0, 0);
			CSDebugBuffer[index] = 0;
			}
		if (index < BUFFERSIZE_NB) NeighborBuffer[index] = 0;
		}
	}

[numthreads(1, 1, 1)]
void CSstart(uint3 DTid : SV_DispatchThreadID)
	{
	count[2] = 0;
	count[3] = 1;
	count[4] = 10;
	}
//********************************************************************************************************************************************
//
//				flag octree
//
//********************************************************************************************************************************************

[numthreads(NUM_THREADS, 1, 1)]	// means, DTid.x goes from 0 to NUM_THREADS !!!!!!!!!
void CSflag(uint3 DTid : SV_DispatchThreadID)
	{
	int currlevel = (int)CBvalues.x;	//current level of the octree
	if (DTid.x == 0)
		{
		count[1] = count[2];
		count[2] = count[4];

		int currlvlidx =  currlevel;
		count[currlvlidx + 10] = count[2];
		count[currlvlidx + 20] = count[2] / OCT_ELEMENTSIZE;
		if (currlevel == maxlevel) count[currlvlidx + 20] = (count[2] - count[1]) / OCT_ELEMENTSIZE_LL + count[6];
		}

	//how many passes do you need? We have DTid.x going from 0 to NUM_THREADS
	float fnumpassesflag = ceil((float)count[0] / (float)NUM_THREADS);
	int numpassesflag = (int)fnumpassesflag;

	uint octdex = 0;					//actual octree index
	uint currdex = 0;					//buffer of actual octree index
	
	float3 midpt = float3(0, 0, 0);
	float midsize = vxarea / 4.;
	uint idx = 0;						//offset of the sublevel of the octree

	[allow_uav_condition]
	for (int i = 0; i < numpassesflag; i++)
		{
		uint voxel_to_work_on = DTid.x + NUM_THREADS*i;
		if (voxel_to_work_on >= count[0]) break;
		octdex = 0;
		midpt = float3(0, 0, 0);
		midsize = vxarea / 4.;

		uint bin = 0;
		uint bin1 = 0;
		uint bin2 = 0;
		uint bin3 = 0;

		float3 pos = VFL[voxel_to_work_on];

		//TODO: check if the point is outside

		uint binaryindex = 0;

		[allow_uav_condition]
		for (uint xlevel = 0; xlevel <= currlevel; xlevel++)
			{
			bin = 0;
			bin1 = 0;
			bin2 = 0;
			bin3 = 0;

			if (pos.z < midpt.z) //back
				{
				bin |= 1;
				bin1 = 1;
				midpt.z -= midsize;
				}
			else
				{
				midpt.z += midsize;
				}

			if (pos.x >= midpt.x) //right
				{
				bin |= 2;
				bin2 = 2;
				midpt.x += midsize;
				}
			else
				{
				midpt.x -= midsize;
				}

			if (pos.y < midpt.y) //down
				{
				bin |= 4;
				bin3 = 4;
				midpt.y -= midsize;
				}
			else
				{
				midpt.y += midsize;
				}

			idx = bin;
			uint level_offset = currlevel - xlevel - 1;
			if (xlevel < currlevel) //binary index of itself and not of its kid! Because its the flagging stage
				binaryindex |= (bin1 << (level_offset + 10 * 0)) | ((bin2) << (level_offset + 10 * 1 - 1)) | ((bin3) << (level_offset + 10 * 2 - 2));

			currdex = octdex + idx;
			octdex = Octree[currdex];

			midsize = midsize / 2.;
			}

		if (currlevel == maxlevel)
			{
			float4 vflcol = VFLcolors[voxel_to_work_on];
			int r, g, b;
			r = vflcol.r * 255;
			g = vflcol.g * 255;
			b = vflcol.b * 255;
			currdex = currdex - idx;
			InterlockedAdd(Octree[currdex + 2], r);
			InterlockedAdd(Octree[currdex + 3], g);
			InterlockedAdd(Octree[currdex + 4], b);
			InterlockedAdd(Octree[currdex + 5], 1);
			Octree[currdex + OCT_BINARYINDEX_LL] = binaryindex;
			}
		else
			{
			Octree[currdex] = 1;
			Octree[currdex - idx + OCT_BINARYINDEX] = binaryindex;
			}
		}
	}

uint vectobin(float3 pos)
	{
	uint bits = 0;
	float x, y, z; x = y = z = 0;

	// add 128, divide by 256, multiply by 1024
	x = ((pos.x + 128) / 256);
	uint binx = x * 1024;
	binx << 10;

	y = ((pos.y + 128) / 256);
	uint biny = y * 1024;
	biny << 20;

	z = ((pos.z + 128) / 256);
	uint binz = z * 1024;

	bits |= binx;
	bits |= biny;
	bits |= binz;

	return bits;
	}

//********************************************************************************************************************************************
//
//				build octree
//
//********************************************************************************************************************************************
[numthreads(NUM_THREADSX, NUM_THREADSY, 1)]	// means, DTid.x goes from 0 to 511 !!!!!!!!!
void CSbuild(uint3 DTid : SV_DispatchThreadID, uint3 Gid : SV_GroupID)
	{
	int actualnum = DTid.y*OCT_ELEMENTSIZE + DTid.x;

	int num_of_flag_area = count[2] - count[1];
	float fnumpassesflag = ceil((float)num_of_flag_area / (float)(NUM_THREADSX*NUM_THREADSY));
	int numpassesflag = (int)fnumpassesflag;

	[allow_uav_condition]
	for (int i = 0; i < numpassesflag; i++)
		{

		int ocv_to_work_on = actualnum + (NUM_THREADSY*OCT_ELEMENTSIZE)*i;
		if (ocv_to_work_on >= num_of_flag_area)
			break; // don't run if there are no more indices to work on

		ocv_to_work_on += count[1];

		if (DTid.x == 0)
			{
			uint free_brick = 0;
			InterlockedAdd(count[6], 1, free_brick);
			Octree[ocv_to_work_on + 8] = free_brick;
			}

		if (Octree[ocv_to_work_on] == 0)
			continue; // go to next iteration of loop if not flagged

		uint oct_elemsize = OCT_ELEMENTSIZE;
		if ((int)CBvalues.x == (maxlevel - 1))
			oct_elemsize = OCT_ELEMENTSIZE_LL;

		uint free_space = 0;
		InterlockedAdd(count[4], oct_elemsize, free_space);
		InterlockedAdd(count[3], 1);
		Octree[ocv_to_work_on] = free_space;
		}
	}



//********************************************************************************************************************************************
[numthreads(NUM_THREADS, 1, 1)]
void CSSetLastLvl(uint3 DTid : SV_DispatchThreadID, uint3 Gid : SV_GroupID)
	{
	int actualnum = DTid.x*OCT_ELEMENTSIZE_LL;
	int num_of_oct_elements_indices = count[2] - count[1];
	int num_of_oct_elements = num_of_oct_elements_indices / OCT_ELEMENTSIZE_LL;
	float fnumpassesflag = ceil((float)num_of_oct_elements / (float)(1 * NUM_THREADS));
	int numpassesflag = (int)fnumpassesflag;

	[allow_uav_condition]
	for (int i = 0; i < numpassesflag; i++)
		{

		int ocv_to_work_on_index = actualnum + (NUM_THREADS*OCT_ELEMENTSIZE_LL)*i;
		if (ocv_to_work_on_index >= num_of_oct_elements_indices)
			break; // don't run if there are no more indices to work on

		ocv_to_work_on_index += count[1];

		uint free_brick = 0;
		InterlockedAdd(count[6], 1, free_brick);
		Octree[ocv_to_work_on_index + 0] = free_brick;

		int pixelcount = Octree[ocv_to_work_on_index + 5];
		int r = Octree[ocv_to_work_on_index + 2] / pixelcount;
		int g = Octree[ocv_to_work_on_index + 3] / pixelcount;
		int b = Octree[ocv_to_work_on_index + 4] / pixelcount;
		uint brickindex = Octree[ocv_to_work_on_index + 0];
		uint3 redidx = dto3d(brickindex) + uint3(1, 1, 1);
		uint3 greenidx = dto3d(brickindex + brickpoolsector) + uint3(1, 1, 1);
		uint3 blueidx = dto3d(brickindex + 2 * brickpoolsector) + uint3(1, 1, 1);
		BrickBuffer[redidx] = r / 255.;
		BrickBuffer[greenidx] = g / 255.;
		BrickBuffer[blueidx] = b / 255.;
		}
	}

//***********************************************************************************************************
[numthreads(NUM_THREADS, 1, 1)]
void CStemplate(uint3 DTid : SV_DispatchThreadID)
	{

	if (DTid.x == 0)
		{
		}

	}
