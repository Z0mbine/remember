#pragma once
#include "render_to_buffer.h"
//////////////////////////////////////
class TextureClass
	{
	private:
		ID3D11Texture1D*			m_texture1D;
		ID3D11Texture1D*			m_staging1D;
		ID3D11Texture2D*			m_texture2D;
		ID3D11Texture2D*			m_staging2D;
		ID3D11Texture3D*			m_texture3D;
		ID3D11Texture3D*			m_staging3D;
		ID3D11RenderTargetView*		m_renderTargetView;
		ID3D11ShaderResourceView*	m_shaderResourceView;
		ID3D11DepthStencilView*		m_depthStencilView;
		ID3D11UnorderedAccessView*  m_unorderedAccessView;
		bool rtv;
		bool srv;
		bool uav;
		bool dsv;

		UINT GetNumMipLevels();

	public:
		int w, h, d;
		TextureClass()
			{
			uav = FALSE;
			rtv = FALSE;
			srv = FALSE;
			dsv = FALSE;
			m_texture1D = NULL;
			m_texture2D = NULL;
			m_texture3D = NULL;
			m_staging1D = NULL;
			m_staging2D = NULL;
			m_staging3D = NULL;
			m_renderTargetView = NULL;
			m_shaderResourceView = NULL;
			m_depthStencilView = NULL;
			m_unorderedAccessView = NULL;
			w = h = d = 0;
			}
		TextureClass(const TextureClass&) {}
		~TextureClass() { ResetTexturesAndViews(); }
		void ResetTexturesAndViews();

		bool Initialize_1DTex(ID3D11Device* device, HWND hwnd, DXGI_FORMAT format = DXGI_FORMAT_R32G32B32A32_FLOAT, int width = -1, bool rtv_ = FALSE, bool srv_ = TRUE, bool uav_ = TRUE, bool dsv_ = FALSE, bool mipmaps = FALSE, bool regenerate = FALSE);
		bool Initialize_2DTex(ID3D11Device* device, HWND hwnd, DXGI_FORMAT format = DXGI_FORMAT_R32G32B32A32_FLOAT, int width = -1, int height = -1, bool rtv_ = TRUE, bool srv_ = TRUE, bool uav_ = TRUE, bool dsv_ = FALSE, bool mipmaps = TRUE, bool regenerate = FALSE);
		bool Initialize_3DTex(ID3D11Device* device, DXGI_FORMAT format = DXGI_FORMAT_R32G32B32A32_FLOAT, int width = -1, int height = -1, int depth = -1, bool rtv_ = FALSE, bool srv_ = TRUE, bool uav_ = TRUE, bool mipmaps = TRUE, bool regenerate = FALSE);
		bool Initialize_staging(ID3D11Device* device);
		bool Initialize_shadowmap(ID3D11Device* device, DXGI_FORMAT format = DXGI_FORMAT_R32G32B32A32_FLOAT, int width = -1, int height = -1);
		ID3D11Texture1D* GetTexture1D()						{ return m_texture1D; }
		ID3D11Texture2D* GetTexture2D()						{ return m_texture2D; }
		ID3D11Texture3D* GetTexture3D()						{ return m_texture3D; }
		
		ID3D11RenderTargetView* GetRenderTargetView()		{ return m_renderTargetView; }
		ID3D11DepthStencilView* GetDepthStencilView()		{ return m_depthStencilView; }
		ID3D11ShaderResourceView* GetShaderResourceView()	{ return m_shaderResourceView; }
		ID3D11UnorderedAccessView* GetUnorderedAccessView()	{ return m_unorderedAccessView; }
		
		ID3D11Texture1D* GetStagingTexture1D(ID3D11DeviceContext* context = NULL)
			{
			if (!m_texture1D) return NULL;
			if (!m_staging1D)
				{
				ID3D11Device* device = NULL;
				context->GetDevice(&device);
				Initialize_staging(device);
				}
			if (context) context->CopyResource(m_staging1D, m_texture1D);
			return m_staging1D;
			}
		ID3D11Texture2D* GetStagingTexture2D(ID3D11DeviceContext* context = NULL)
			{
			if (!m_texture2D) return NULL;
			if (!m_staging2D)
				{
				ID3D11Device* device = NULL;
				context->GetDevice(&device);
				Initialize_staging(device);
				}
			if (context) context->CopyResource(m_staging2D, m_texture2D);
			return m_staging2D;
			}
		ID3D11Texture3D* GetStagingTexture3D(ID3D11DeviceContext* context = NULL)
			{
			if (!m_texture3D) return NULL;
			if (!m_staging3D)
				{
				ID3D11Device* device = NULL;
				context->GetDevice(&device);
				Initialize_staging(device);
				}
			if (context) context->CopyResource(m_staging3D, m_texture3D);
			return m_staging3D;
			}
	};