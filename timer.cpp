#include "timer.h"

void Timer::Simple(double delay, function_t func)
{
	timer_t timer;
	timer.expireTime = RunTime() + delay;
	timer.func = func;
	timer.repetitions = 1;

	TIMERS.push_back(timer);
}

void Timer::Create(const char *uid, double delay, int repetitions, function_t func)
{
	timer_t timer;
	timer.expireTime = RunTime() + delay;
	timer.repetitions = repetitions;
	timer.uid = uid;
	timer.func = func;

	TIMERS.push_back(timer);
}

void Timer::Remove(const char *uid)
{
	int it;

	for (it = 0; it < TIMERS.size(); it++)
	{
		timer_t timer = TIMERS[it];

		if (timer.uid == uid)
		{
			TIMERS.erase(TIMERS.begin() + it);
			break;
		}
	}
}

void Timer::Process()
{
	int it;
	double curTime = RunTime();

	for (it = 0; it < TIMERS.size(); it++)
	{
		timer_t timer = TIMERS[it];

		if (curTime >= timer.expireTime)
		{
			timer.func();

			timer.repetitions--;

			if (timer.repetitions <= 0)
			{
				TIMERS.erase(TIMERS.begin() + it);
			}
			else
				timer.expireTime = curTime + timer.delay;
		}
	}
}