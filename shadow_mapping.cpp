#include "shadow_mapping.h"

void shadow_mapping_::init(HWND hwnd, ID3D11Device* d3dDevice)
	{
	HRESULT hr = S_OK;
	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;
	Rz = XMMatrixRotationZ(0);
	Rx = XMMatrixRotationX(0);
	
	
	UINT width = SHADOWMAPXYSIZE;
	UINT height = SHADOWMAPXYSIZE;

	RECT rc;
	GetClientRect(hwnd, &rc);
	//width = rc.right - rc.left;
	//height = rc.bottom - rc.top;

	viewport_shadowmap.Width = width;
	viewport_shadowmap.Height = height;
	viewport_shadowmap.MinDepth = 0.0f;
	viewport_shadowmap.MaxDepth = 1.0f;
	viewport_shadowmap.TopLeftX = 0;
	viewport_shadowmap.TopLeftY = 0;


	// Create depth stencil texture
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory(&descDepth, sizeof(descDepth));
	descDepth.Width = width;
	descDepth.Height = height;
	descDepth.MipLevels = 1;
	descDepth.ArraySize = 1;
	descDepth.Format = DXGI_FORMAT_R32_TYPELESS;
	descDepth.SampleDesc.Count = 1;
	descDepth.SampleDesc.Quality = 0;
	descDepth.Usage = D3D11_USAGE_DEFAULT;
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	descDepth.CPUAccessFlags = 0;
	descDepth.MiscFlags = 0;
	hr = d3dDevice->CreateTexture2D(&descDepth, NULL, &pDepthStencil);
	if (FAILED(hr))
		return;
	// Create the depth stencil view
	D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory(&descDSV, sizeof(descDSV));
	descDSV.Format = DXGI_FORMAT_D32_FLOAT;
	descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	descDSV.Texture2D.MipSlice = 0;
	hr = d3dDevice->CreateDepthStencilView(pDepthStencil, &descDSV, &pDepthStencilView);
	if (FAILED(hr))
		return;

	// Create the sample state
	D3D11_SAMPLER_DESC sampDesc;
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = d3dDevice->CreateSamplerState(&sampDesc, &pSamplerLinear);
	if (FAILED(hr))
		return;

	// Create the sample state
	ZeroMemory(&sampDesc, sizeof(sampDesc));
	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = d3dDevice->CreateSamplerState(&sampDesc, &pSamplerPoint);
	if (FAILED(hr))
		return;

	// Create orthogonal view matrices

	XMVECTOR Eye = XMVectorSet(100.0f, 350.0f, 50.0f, 0.0f);//camera position
	XMVECTOR At = XMVectorSet(0.0f, 0, 0.0f, 0.0f);//look at
	XMVECTOR Up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);// normal vector on at vector (always up)

	/*
	Eye = XMVectorSet(0.0f, 100.0f, 0.0f, 0.0f);//camera position
	At = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);//look at
	Up = XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f);// normal vector on at vector (always up)
	*/
	View_light = XMMatrixLookAtLH(Eye, At, Up);

	// Create projection matrix
	Projection_ortho = XMMatrixOrthographicLH(width, (FLOAT)height, 0.01f, 15000.0f);// *XMMatrixScaling(1.7, 1.7, 1.7);

	//Create the constant buffer
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(ConstantBuffer);
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bd.CPUAccessFlags = 0;
	hr = d3dDevice->CreateBuffer(&bd, NULL, &pConstantBuffer);
	if (FAILED(hr))
		return;

	/////////// Shaders ///////////
	ID3DBlob* pVSBlob = NULL;
	ID3DBlob* pPSBlob = NULL;

	//  vertex shader
	hr = CompileShaderFromFile(L"shadow_mapping.fx", "VS", "vs_5_0", &pVSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreateVertexShader(pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &pShadowVS);
	if (FAILED(hr))
		{
		pVSBlob->Release();
		return;
		}
	// pixel shader
	pPSBlob = NULL;
	hr = CompileShaderFromFile(L"shadow_mapping.fx", "PS", "ps_5_0", &pPSBlob);
	if (FAILED(hr))
		{
		MessageBox(NULL,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return;
		}
	hr = d3dDevice->CreatePixelShader(pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &pShadowPS);
	pPSBlob->Release();
	if (FAILED(hr))
		return;

	///////// Writable shader targets ///////// 
	RenderTexture.Initialize_2DTex(d3dDevice,0, DXGI_FORMAT_R32G32B32A32_FLOAT, width, height,TRUE,TRUE,FALSE);
	//RenderTexture.Initialize_shadowmap(d3dDevice, DXGI_FORMAT_R32G32B32A32_FLOAT, width, height);

	// Create rasterizer states
	D3D11_RASTERIZER_DESC rasterizerState;
	rasterizerState.FillMode = D3D11_FILL_SOLID;
	rasterizerState.CullMode = D3D11_CULL_BACK;
	rasterizerState.FrontCounterClockwise = false;
	rasterizerState.DepthBias = 0;
	rasterizerState.DepthBiasClamp = 0.0f;
	rasterizerState.SlopeScaledDepthBias = 0.0f;
	rasterizerState.DepthClipEnable = true;
	rasterizerState.ScissorEnable = false;
	rasterizerState.MultisampleEnable = false;
	rasterizerState.AntialiasedLineEnable = false;
	d3dDevice->CreateRasterizerState(&rasterizerState, &g_pRasterStateCCW);

	}
//*********************************************************************************************************************************
ID3D11SamplerState *shadow_mapping_::get_point_sampler()
	{
	return pSamplerPoint;
	}
//*********************************************************************************************************************************
extern float shadow_factor;
XMMATRIX shadow_mapping_::get_light_VP() 
	{ 
	
	XMMATRIX V = Rx * Rz * View_light;
	return XMMatrixMultiply(V , Projection_ortho) * XMMatrixScaling(5.85*shadow_factor, 5.85*shadow_factor, 5.85*shadow_factor);
	}
//*********************************************************************************************************************************
void shadow_mapping_::render_into_shadow_map(ID3D11RenderTargetView *rt, ID3D11DeviceContext* pImmediateContext, IDXGISwapChain* pSwapChain, XMMATRIX &l_world, float time, void(*render)(ID3D11DeviceContext*, float, ID3D11Buffer*,ConstantBuffer &))
	{
	D3D11_VIEWPORT viewport_normal;
	UINT viewport_num = 1;

	pImmediateContext->RSGetViewports(&viewport_num, &viewport_normal);
	pImmediateContext->RSSetViewports(1, &viewport_shadowmap);
	
	ID3D11RenderTargetView*			RenderTarget;
	RenderTarget = RenderTexture.GetRenderTargetView();
	//RenderTarget = rt;
	float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f }; // red, green, blue, alpha
	pImmediateContext->ClearRenderTargetView(RenderTarget, ClearColor);
	pImmediateContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0, 0);
	pImmediateContext->OMSetRenderTargets(1, &RenderTarget, pDepthStencilView);

	pImmediateContext->VSSetShader(NULL, NULL, 0);
	pImmediateContext->PSSetShader(NULL, NULL, 0);
	pImmediateContext->GSSetShader(NULL, NULL, 0);
	pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//render scene
	ConstantBuffer constantbuffer;
	constantbuffer.World = XMMatrixTranspose(l_world);
	constantbuffer.Projection = XMMatrixTranspose(Projection_ortho*XMMatrixScaling(0.85, 0.85, 0.85));
	constantbuffer.View = XMMatrixTranspose(View_light);
	constantbuffer.LightViewProjection = XMMatrixTranspose(get_light_VP());
	constantbuffer.info = XMFLOAT4(8, 1, 1, 1);
	pImmediateContext->UpdateSubresource(pConstantBuffer, 0, NULL, &constantbuffer, 0, 0);

	pImmediateContext->VSSetShader(pShadowVS, NULL, 0);
	pImmediateContext->VSSetConstantBuffers(0, 1, &pConstantBuffer);
	pImmediateContext->VSSetSamplers(0, 1, &pSamplerLinear);

	pImmediateContext->PSSetShader(pShadowPS, NULL, 0);
	pImmediateContext->PSSetConstantBuffers(0, 1, &pConstantBuffer);
	pImmediateContext->PSSetSamplers(0, 1, &pSamplerLinear);	

	ID3D11RasterizerState *oldstate = NULL;
	pImmediateContext->RSGetState(&oldstate);
	pImmediateContext->RSSetState(g_pRasterStateCCW);
	render(pImmediateContext, time, pConstantBuffer, constantbuffer);
	pImmediateContext->RSSetState(oldstate);

	pSwapChain->Present(0, 0);

	// Clear VS and PS resources
	RenderTarget = NULL;
	pImmediateContext->OMSetRenderTargets(1, &RenderTarget, pDepthStencilView);


	ID3D11ShaderResourceView* srvs[10] = { 0 };
	pImmediateContext->VSSetShaderResources(0, 10, srvs);
	pImmediateContext->PSSetShaderResources(0, 10, srvs);
	pImmediateContext->RSSetViewports(viewport_num, &viewport_normal);
	pImmediateContext->VSSetShader(NULL, NULL, 0);
	pImmediateContext->PSSetShader(NULL, NULL, 0);
	}