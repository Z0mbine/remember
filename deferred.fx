#include "shaderheader.fxh"
Texture2D txDiffuse : register(t0);
Texture2D txShadowMap : register(t1);
SamplerState samLinear : register(s0);
SamplerState samPoint : register(s1);
//**************************************************************************************
cbuffer ConstantBuffer : register(b0)
	{
	matrix World;
	matrix View;
	matrix Projection;
	matrix LightViewProjection;
	float4 info;
	float4 campos;
	float4 sun_position; //rotation of the sun
	float4 daytimer; //day+night cycle
	};
//**************************************************************************************
struct VS_INPUT
	{
	float3 Pos : POSITION;
	float2 Tex : TEXCOORD0;
	float3 Norm : NORMAL;
	float3 Tan : TANGENT0;
	float3 Bi : BINORMAL0;
	};
//**************************************************************************************
struct PS_INPUT
	{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float3 WorldPos : TEXCOORD1;
	float4 LightPos : TEXCOORD2;
	float4 Norm : NORMAL;
	float3 Tan : TANGENT0;
	float3 Bi : BINORMAL0;

	};
//**************************************************************************************
struct PS_CSOutput
	{
	float4 Position		: SV_Target0;
	float4 Normal		: SV_Target1;
	float4 Tangent		: SV_Target2;
	float4 Diffuse		: SV_Target3;
	};

PS_CSOutput PS(PS_INPUT input) : SV_Target
	{
	PS_CSOutput outp;
	outp.Position = float4(input.WorldPos, 1);
	outp.Normal.xyz = (input.Norm.xyz + float3(1, 1, 1))*0.5;
	outp.Tangent.xyz = (input.Tan.xyz + float3(1, 1, 1))*0.5;
	outp.Normal.w = 1;
	outp.Tangent.w = 1;
	outp.Diffuse = txDiffuse.Sample(samLinear, input.Tex);
	//outp.Diffuse = float4(1, 0, 0,1);
	return outp;
	}