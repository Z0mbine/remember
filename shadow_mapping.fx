#include "shaderheader.fxh"
Texture2D txDiffuse : register(t0);
SamplerState samLinear : register(s0);

cbuffer ConstantBuffer : register(b0)
	{
	matrix World;
	matrix View;
	matrix Projection;
	matrix LightViewProjection;
	float4 info;
	float4 campos;
	float4 sun_position; //rotation of the sun
	float4 daytimer; //day+night cycle
	};

struct VS_INPUT
	{
	float3 Pos : POSITION;
	float2 Tex : TEXCOORD0;
	float3 Norm : NORMAL;
	float3 Tan : TANGENT0;
	float3 Bi : BINORMAL0;
	};

struct PS_INPUT
	{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float4 WorldPos : TEXCOORD1;
	float4 Norm : NORMAL;
	float3 Tan : TANGENT0;
	float3 Bi : BINORMAL0;

	};

PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output = (PS_INPUT)0;
	float4 pos = float4(input.Pos, 1);
	pos = mul(pos, World);
	pos = mul(pos, LightViewProjection);
	output.WorldPos = pos;
	output.Pos = pos;
	
	return output;
}

float4 PS(PS_INPUT input) : SV_Target
{
	float depth = input.WorldPos.z / input.WorldPos.w;

	return float4(depth, depth, depth, 1);
	return float4(input.WorldPos.xyz / input.WorldPos.w,1);

	//float4 texture_color = txDiffuse.Sample(samLinear, input.Tex);
	//return texture_color;
}

	/*{
	PS_INPUT output = (PS_INPUT)0;
	float4 pos = float4(input.Pos, 1);
	matrix WVP = World * View * Projection;
	output.WorldPos = pos;
	output.Pos = mul(pos, WVP);
	output.Tex = input.Tex;

	matrix rot = World;
	rot._41 = rot._42 = rot._43 = 0;
	rot._14 = rot._24 = rot._34 = 0;

	output.Norm.xyz = normalize(mul(input.Norm, rot));
	float4 bi = float4(input.Bi, 1);
	float4 tan = float4(input.Tan, 1);
	output.Tan.xyz = normalize(mul(tan, rot)).xyz;
	output.Bi.xyz = normalize(mul(bi, rot)).xyz;

	return output;
	}*/