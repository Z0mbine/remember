#include "shaderheader.fxh"
Texture2D txDiffuse : register(t0);
Texture2D txShadowMap : register(t1);
RWStructuredBuffer<float3> VFL : register(u1);
RWStructuredBuffer<uint> count : register(u2);
RWStructuredBuffer<float4> VFLcolors : register(u3);
SamplerState samLinear : register(s0);
SamplerState samPoint : register(s1);
//**************************************************************************************
cbuffer ConstantBuffer : register(b0)
	{
	matrix World;
	matrix View;
	matrix Projection;
	matrix LightViewProjection;
	float4 info;
	float4 campos;
	float4 sun_position; //rotation of the sun
	float4 daytimer; //day+night cycle
	};
//**************************************************************************************
struct VS_INPUT
	{
	float3 Pos : POSITION;
	float2 Tex : TEXCOORD0;
	float3 Norm : NORMAL;
	float3 Tan : TANGENT0;
	float3 Bi : BINORMAL0;
	};
//**************************************************************************************
struct PS_INPUT
	{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float3 WorldPos : TEXCOORD1;
	float4 LightPos : TEXCOORD2;
	float4 Norm : NORMAL;
	float3 Tan : TANGENT0;
	float3 Bi : BINORMAL0;
	
	};
//**************************************************************************************
struct PS_MRTOutput
	{
	float4 Color    : SV_Target0;
	};
//**************************************************************************************
PS_INPUT VS(VS_INPUT input)
	{
	PS_INPUT output = (PS_INPUT)0;
	float4 pos = float4(input.Pos, 1);
	pos = mul(pos, World);
	output.LightPos = mul(pos, LightViewProjection);
	
	output.WorldPos = pos;
	output.Pos = mul(pos, View);
	output.Pos = mul(output.Pos, Projection);
	output.Tex = input.Tex;

	matrix rot = World;
	rot._41 = rot._42 = rot._43 = 0;
	rot._14 = rot._24 = rot._34 = 0;

	output.Norm.xyz = normalize(mul(input.Norm, rot));
	float4 bi = float4(input.Bi, 1);
	float4 tan = float4(input.Tan, 1);
	output.Tan.xyz = normalize(mul(tan, rot)).xyz;
	output.Bi.xyz = normalize(mul(bi, rot)).xyz;

	return output;
	}
//**************************************************************************************
void PS(PS_INPUT input)
	{

	float4 color = txDiffuse.Sample(samLinear, input.Tex);
	float3 n = input.Norm;
	float3 lp = float3(50, 100, 10); //= sun_position;
	float3 dir = normalize(lp - n);
	float3 light = saturate(dot(dir, n.xyz));

	color.a = 1;
	color.rgb *= light;

	float4 pos = float4(input.WorldPos, 1);

	//shadow mapping
	float4 lpos = input.LightPos;
	float2 projectTexCoord;
	projectTexCoord.x = input.LightPos.x / input.LightPos.w / 2.0f + 0.5f;
	projectTexCoord.y = -input.LightPos.y / input.LightPos.w / 2.0f + 0.5f;

	uint idx;
	InterlockedAdd(count[5], 1, idx);

	if (!((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y)))
		{
		InterlockedAdd(count[0], 1, idx);
		VFL[idx] = pos.xyz;
		VFLcolors[idx] = float4(0, 0, 0,1);
		return;
		}
	float shadowMapDepth = txShadowMap.SampleLevel(samPoint, projectTexCoord, 0).z;
	//return float4(float3(1, 1, 1) * shadowMapDepth, 1);
	float lightDepthValue = input.LightPos.z / input.LightPos.w;
	//return float4(float3(1, 1, 1) * lightDepthValue, 1);
	float bias = 0.001;
	lightDepthValue = lightDepthValue - bias;
	
	

	if (lightDepthValue >shadowMapDepth)
		{
		InterlockedAdd(count[0], 1, idx);
		VFL[idx] = pos.xyz;
		VFLcolors[idx] = float4(0, 0, 0, 1);
		return;
		}

	
	
	if (idx < BUFFERSIZE)
		{
		InterlockedAdd(count[0], 1, idx);
		VFL[idx] = pos.xyz;
		VFLcolors[idx] = color;
		}
	}