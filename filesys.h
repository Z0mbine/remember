#pragma once

#include <vector>

// Was going to use this for data saving but we never got that far...
// Credit to Garry Newman's Bootil utility (Facepunch)
// Modified for personal use

namespace File
{
	int Find(std::vector<std::string> *files, std::vector<std::string> *folders, const std::string &strFind, bool bUpUpFolders);
	int Exists(const std::string &strFileName);
	bool Read(const std::string &strFileName, std::string &strOut);
	bool Write(const std::string & strFileName, const std::string & strOut);
	bool Append(const std::string & strFileName, const std::string & strOut);
	bool IsFolder(const std::string & strFileName);
}