#ifndef _FLAGS_H_
#define _FLAGS_H_

#pragma once

#define IN_ATTACK	0x1
#define IN_JUMP		0x2
#define IN_DUCK		0x4
#define IN_FORWARD	0x8
#define IN_BACK		0x10
#define IN_USE		0x20
#define IN_LEFT		0x40
#define IN_RIGHT	0x80

#endif // _FLAGS_H_