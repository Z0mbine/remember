#include "shaderheader.fxh"
Texture2D txPositions : register(t0);
Texture2D txShadowMap : register(t1);
Texture2D txMask : register(t2);
SamplerState samLinear : register(s0);
SamplerState samPoint : register(s1);
//**************************************************************************************
cbuffer ConstantBuffer : register(b0)
	{
	matrix World;
	matrix View;
	matrix Projection;
	matrix LightViewProjection;
	float4 info;
	float4 campos;
	float4 sun_position; //rotation of the sun
	float4 daytimer; //day+night cycle
	};
//**************************************************************************************
struct VS_INPUT
	{
	float3 Pos : POSITION;
	float2 Tex : TEXCOORD0;
	float3 Norm : NORMAL;
	float3 Tan : TANGENT0;
	float3 Bi : BINORMAL0;
	};
//**************************************************************************************
struct PS_INPUT
	{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float3 WorldPos : TEXCOORD1;
	float4 LightPos : TEXCOORD2;
	float4 Norm : NORMAL;
	float3 Tan : TANGENT0;
	float3 Bi : BINORMAL0;

	};
//**************************************************************************************
struct PS_SCENE_INPUT
	{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	};
SamplerComparisonState cmpSampler
	{
	// sampler state
	Filter = COMPARISON_MIN_MAG_MIP_LINEAR;
	AddressU = MIRROR;
	AddressV = MIRROR;

	// sampler comparison state
	ComparisonFunc = LESS_EQUAL;
	};

float2 texOffset(int u, int v)
	{
	return float2(u * 1.0f / (float)SHADOWMAPXYSIZE, v * 1.0f / (float)SHADOWMAPXYSIZE);
	}

float4 PS(PS_SCENE_INPUT input) : SV_Target
{
	float2 scaledCoords = float2(input.Tex.x, input.Tex.y);
	float4 worldpos = txPositions.Sample(samPoint, input.Tex);
	float4 maskColor = txMask.Sample(samLinear, scaledCoords);
	worldpos.w = 1;
	float4 lightpos = mul(worldpos, LightViewProjection);
	//shadow mapping
	float4 lpos = lightpos;
	float2 projectTexCoord;
	projectTexCoord.x = lightpos.x / lightpos.w / 2.0f + 0.5f;
	projectTexCoord.y = -lightpos.y / lightpos.w / 2.0f + 0.5f;

	if (!((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y)))
		{
		return float4(0, 0, 1, 1);
		}
	float shadowMapDepth = txShadowMap.SampleLevel(samPoint, projectTexCoord, 0).z;
	//sum += txShadowMap.SampleCmpLevelZero(cmpSampler, projectTexCoord + texOffset(x, y), lightDepthValue);
	
	float lightDepthValue = lightpos.z / lightpos.w;
	float bias = 0.001;
	lightDepthValue = lightDepthValue - bias;
	
	float sum = 0;
	float x, y;

	//perform PCF filtering on a 4 x 4 texel neighborhood
	for (y = -1.5; y <= 1.5; y += 1.0)
		{
		for (x = -1.5; x <= 1.5; x += 1.0)
			{
			//sum += txShadowMap.SampleLevel(samPoint, projectTexCoord + texOffset(x, y), 0).z; //shadowMap.SampleCmpLevelZero(cmpSampler, input.lpos.xy + texOffset(x, y), input.lpos.z);
			shadowMapDepth = txShadowMap.SampleLevel(samPoint, projectTexCoord + texOffset(x, y), 0).z;
			if (lightDepthValue <= shadowMapDepth)
				sum += 1;

			}
		}

	float shadowFactor = sum / 16.0;

	//shadowFactor = 0;

	//return float4(shadowMapDepth, shadowMapDepth, shadowMapDepth, 1);
	
	//if (lightDepthValue >shadowMapDepth)
		return float4(shadowFactor, shadowFactor, maskColor.r, 1);


	return float4(1, 1, 1, 1);
}