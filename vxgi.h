#include "shadow_mapping.h"
#include "baseplayer.h"

HRESULT CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);

class vxgi_
	{
	private:
		ID3D11Texture2D*                    pDepthStencil = NULL;
		ID3D11DepthStencilView*             pDepthStencilView = NULL;
		ID3D11SamplerState*                 pSamplerLinear = NULL;

		TextureClass						Normals;
		TextureClass						Tangents;
		TextureClass						DeferredShadow;
		TextureClass						Diffuse;
		TextureClass						Positions;
		TextureClass						ScreenOutput;
		TextureClass						BrickBuffer;

		BufferClass							Octree;
		BufferClass							VFL;
		BufferClass							VFLcolors;
		BufferClass							const_count;
		BufferClass							NeighborBuffer;
		BufferClass							CSDebugBuffer;

		ID3D11Buffer*                       pVertexBuffer = NULL;

		ID3D11VertexShader*                 pVFLVS = NULL;
		ID3D11PixelShader*                  pVFLPS = NULL;
		ID3D11PixelShader*					pDeferredPS = NULL;
		ID3D11VertexShader*                 pSceneVS = NULL;
		ID3D11VertexShader*                 pSceneScreenVS = NULL;
		ID3D11PixelShader*                  pScenePS = NULL;
		ID3D11PixelShader*                  pDeferredShadowPS = NULL;
		ID3D11PixelShader*					g_pScreenPScompare = NULL;
		ID3D11ShaderResourceView* flashlightMask = NULL;

		ID3D11ComputeShader*                pStartCS = NULL;
		ID3D11ComputeShader*                pFlagCS = NULL;
		ID3D11ComputeShader*                pBuildCS = NULL;
		ID3D11ComputeShader*                pSetLastLvlCS = NULL;
		ID3D11ComputeShader*                pNeighborCS = NULL;
		ID3D11ComputeShader*                pColorExchangeCS = NULL;
		ID3D11ComputeShader*                pKidsColorExchangeCS = NULL;
		ID3D11ComputeShader*				pClearCS = NULL;

		ID3D11VertexShader*                 pVoxelVS = NULL;
		ID3D11GeometryShader*				pVoxelGS = NULL;
		ID3D11PixelShader*                  pVoxelPS = NULL;
		voxel_								voxel;

		ID3D11Buffer*                       pCBuffer = NULL;
		ID3D11Buffer*						pCBufferCS = NULL;

		XMMATRIX							Projection_ortho;
		XMMATRIX							View_front;
		XMMATRIX							View_top;
		XMMATRIX							View_left;

		D3D11_VIEWPORT						viewport_octree;

		void Reset_CS(ID3D11DeviceContext* pImmediateContext);
		void render_voxels(ID3D11DeviceContext* pImmediateContext, XMMATRIX &l_view, XMMATRIX &l_Projection);
	public:
		int									curr_maxlevel = maxlevel;
		//*********************************************************************
		void init(HWND hWnd, ID3D11Device* d3dDevice);
		//*********************************************************************
		ID3D11Buffer* get_VFL_staging(ID3D11DeviceContext* context) { return VFL.GetStagingBuffer(context); }
		ID3D11Buffer* get_octree_staging(ID3D11DeviceContext* context) { return Octree.GetStagingBuffer(context); }
		ID3D11Buffer* get_count_staging(ID3D11DeviceContext* context) { return const_count.GetStagingBuffer(context); }
		ID3D11Buffer* get_VFLcolors_staging(ID3D11DeviceContext* context) { return VFLcolors.GetStagingBuffer(context); }
		ID3D11Buffer* get_nbuffer_staging(ID3D11DeviceContext* context) { return NeighborBuffer.GetStagingBuffer(context); }
		ID3D11Buffer* get_debugbuffer_staging(ID3D11DeviceContext* context) { return CSDebugBuffer.GetStagingBuffer(context); }
		
		TextureClass* get_output() { return &ScreenOutput; }
		TextureClass* get_positions() { return &Positions; }
		TextureClass* get_normals() { return &Normals; }
		TextureClass* get_diffuse() { return &Diffuse; }
		//*********************************************************************
		void render_1_of_8_render_into_voxel_fragment_list(ID3D11DeviceContext* pImmediateContext, IDXGISwapChain* pSwapChain, XMMATRIX &l_world, float time, shadow_mapping_ *shadow_mapping, void(*render)(ID3D11DeviceContext*, float, ID3D11Buffer*, ConstantBuffer &));
		void render_2_of_8_build_octree(ID3D11DeviceContext* pImmediateContext);
		void render_3_of_8_collect_kids_color(ID3D11DeviceContext* pImmediateContext);
		void render_4_of_8_build_neighbor_info(ID3D11DeviceContext* pImmediateContext);
		void render_5_of_8_exchange_color(ID3D11DeviceContext* pImmediateContext);		
		void render_6_of_8_render_scene_into_RT(ID3D11DeviceContext* pImmediateContext, CBasePlayer * player, IDXGISwapChain* pSwapChain, XMMATRIX &l_world, XMMATRIX &l_view, XMMATRIX &l_Projection, void(*render)(ID3D11DeviceContext*, float, ID3D11Buffer*, ConstantBuffer &));
		void render_7_of_8_deferred_shadowing(ID3D11DeviceContext* pImmediateContext, CBasePlayer * player, IDXGISwapChain* pSwapChain, XMMATRIX &l_world, XMMATRIX &l_view, XMMATRIX &l_Projection, shadow_mapping_* shadow_mapping);
		void render_8_of_8_cone_tracing(ID3D11DeviceContext* pImmediateContext, CBasePlayer * player,IDXGISwapChain* pSwapChain, XMMATRIX &l_world, XMMATRIX &l_view, XMMATRIX &l_Projection, shadow_mapping_* shadow_mapping, bool draw_voxels = FALSE);
		void render_comparison(ID3D11DeviceContext* pImmediateContext, CBasePlayer * player, IDXGISwapChain* pSwapChain, XMMATRIX &l_world, XMMATRIX &l_view, XMMATRIX &l_Projection, shadow_mapping_* shadow_mapping, bool draw_voxels = FALSE);
		
		void reset_all(ID3D11DeviceContext* pImmediateContext);

		void CleanupDevice()
			{
			if (pDepthStencil) pDepthStencil->Release();
			if (pDepthStencilView) pDepthStencilView->Release();
			if (pSamplerLinear) pSamplerLinear->Release();

			Octree.ResetBuffersAndViews();
			VFL.ResetBuffersAndViews();
			VFLcolors.ResetBuffersAndViews();
			const_count.ResetBuffersAndViews();
			NeighborBuffer.ResetBuffersAndViews();
			CSDebugBuffer.ResetBuffersAndViews();

			ScreenOutput.ResetTexturesAndViews();
			BrickBuffer.ResetTexturesAndViews();

			if (pVFLVS) pVFLVS->Release();
			if (pVFLPS) pVFLPS->Release();
			if (pSceneVS) pSceneVS->Release();
			if (pScenePS) pScenePS->Release();

			if (pStartCS) pStartCS->Release();
			if (pFlagCS) pFlagCS->Release();
			if (pBuildCS) pBuildCS->Release();
			if (pSetLastLvlCS) pSetLastLvlCS->Release();
			if (pNeighborCS) pNeighborCS->Release();
			if (pColorExchangeCS) pColorExchangeCS->Release();
			if (pKidsColorExchangeCS) pKidsColorExchangeCS->Release();
			if (pClearCS) pClearCS->Release();

			if (pVoxelVS) pVoxelVS->Release();
			if (pVoxelGS) pVoxelGS->Release();
			if (pVoxelPS) pVoxelPS->Release();
			voxel.Shutdown();

			if (pCBuffer) pCBuffer->Release();
			if (pCBufferCS) pCBufferCS->Release();
			}
	};


