#pragma once

#include "render_to_texture.h"

class shadow_mapping_
	{
	private:
		ID3D11Texture2D*                    pDepthStencil = NULL;
		ID3D11DepthStencilView*             pDepthStencilView = NULL;
		ID3D11SamplerState*                 pSamplerLinear = NULL;
		ID3D11SamplerState*                 pSamplerPoint = NULL;
		TextureClass						RenderTexture;
		ID3D11RasterizerState*				g_pRasterStateCCW = NULL;
		ID3D11VertexShader*                 pShadowVS = NULL;
		ID3D11PixelShader*                  pShadowPS = NULL;

		ID3D11Buffer*                       pConstantBuffer = NULL;
		D3D11_VIEWPORT						viewport_shadowmap;
		XMMATRIX							Projection_ortho;
		XMMATRIX							Rx, Rz;

	public:
		XMMATRIX							View_light;
		void set_x_rot(float wx) { Rx = XMMatrixRotationX(wx); }
		void set_z_rot(float wz) { Rz = XMMatrixRotationZ(wz); }
		ID3D11SamplerState *get_point_sampler();
		XMMATRIX get_light_VP();
		TextureClass* get_output() { return &RenderTexture; }
		void init(HWND hwnd,ID3D11Device* d3dDevice);
		void render_into_shadow_map(ID3D11RenderTargetView *rt,ID3D11DeviceContext* pImmediateContext, IDXGISwapChain* pSwapChain, XMMATRIX &l_world, float time, void(*render)(ID3D11DeviceContext*, float, ID3D11Buffer*,ConstantBuffer &));
		void set_light(XMFLOAT3 pos, XMFLOAT3 lookat)
			{
			// Create orthogonal view matrices
			XMVECTOR Eye = XMVectorSet(pos.x, pos.y, pos.z, 0.0f);//camera position
			XMVECTOR At = XMVectorSet(lookat.x, lookat.y, lookat.z, 0.0f);//look at
			XMFLOAT3 c = cross(pos, lookat);
			XMVECTOR Up = XMVectorSet(c.x,c.y,c.z, 0.0f);// normal vector on at vector (always up)
			View_light = XMMatrixLookAtLH(Eye, At, Up);
			}
		void CleanupDevice()
			{
			if (pDepthStencil) pDepthStencil->Release();
			if (pDepthStencilView) pDepthStencilView->Release();
			if (pSamplerLinear) pSamplerLinear->Release();
			if (pShadowVS) pShadowPS->Release();
			if (pShadowPS) pSamplerLinear->Release();
			if (pSamplerPoint) pSamplerPoint->Release();
			if (pConstantBuffer) pConstantBuffer->Release();
			RenderTexture.ResetTexturesAndViews();
			pSamplerPoint = NULL;
			pDepthStencil = NULL;
			pDepthStencilView = NULL;
			pSamplerLinear = NULL;
			pShadowVS = NULL;
			pShadowPS = NULL;
			pConstantBuffer = NULL;
			}
	};
