#include "baseplayer.h"
#include <math.h>
#include "timer.h"

// Constructor
CBasePlayer::CBasePlayer()
{
	Initialize();
}

CBasePlayer::~CBasePlayer()
{
}

// TODO: Make sure this isn't needed anymore
Vector CBasePlayer::GetEyePosInternal()
{
	return GetPosInternal() + Vector(0, GetTall(), 0);
}

// TODO: Make sure this isn't needed anymore
Vector CBasePlayer::GetPosInternal()
{
	return Vector(pos.x, pos.y, pos.z);
}

// Called every frame, checks if a footstep should be played
void CBasePlayer::HandleFootstep(double curTime, float frameTime, bool bMoving)
{
	if (m_flStepSoundTime > 0)
	{
		m_flStepSoundTime -= 1000.0f * frameTime;

		if (m_flStepSoundTime < 0)
		{
			m_flStepSoundTime = 0;
		}
	}

	// Still have some time left before playing the next sound
	if (m_flStepSoundTime > 0)
		return;

	// We're not moving, don't bother
	if (!bMoving)
		return;

	// We're not even on the ground, it doesn't work like that
	if (!m_bOnGround)
		return;

	// Roughly a step every half second
	m_flStepSoundTime = 400;

	PlayFootstep();
}

void CBasePlayer::HandleRecording(int id)
{
	// Audio handler crashes if too many sounds are playing at once.
	// Voice recordings scrapped.

	//switch (id)
	//{
	//case 0:
	//{
	//	audio_args_ *info = new audio_args_;
	//	info->file = L"recordings/r1.wav";
	//	info->volume = -1000;

	//	start_music(info);
	//	break;
	//}
	//case 1:
	//{
	//	audio_args_ *info = new audio_args_;
	//	info->file = L"recordings/r2.wav";
	//	info->volume = -1000;

	//	start_music(info);
	//	break;
	//}
	//case 2:
	//{
	//	audio_args_ *info = new audio_args_;
	//	info->file = L"recordings/record3.wav";
	//	info->volume = -1000;

	//	start_music(info);
	//	break;
	//}
	//default:
	//	break;
	//}
}

// Returns a copy of the player's origin
Vector CBasePlayer::GetPos()
{
	return Vector(pos.x, pos.y, pos.z);
}

// Set the player's origin
void CBasePlayer::SetPos(const Vector &vecPos)
{
	VectorCopy(vecPos, pos);
}

// Sets a single component of the player's absolute position
void CBasePlayer::SetPosComponent(int iPart, vec_t value)
{
	pos[iPart] = value;
}

// Returns a copy of the player's view angles
QAngle CBasePlayer::GetEyeAngles()
{
	return QAngle(ang.x, ang.y, ang.z);
}

// Set the player's view angles
void CBasePlayer::SetEyeAngles(const QAngle &eyeAng)
{
	VectorCopy(eyeAng, ang);
}

// Setes a specific member value of the player's eye angles. You shouldn't need to use this outside of
// OnMM
void CBasePlayer::SetEyeComponent(int iPart, vec_t value)
{
	ang[iPart] = value;
}

// Sets how tall the player is
void CBasePlayer::SetTall(float flHeight)
{
	m_flEyeOffset = flHeight;
}

// Returns how tall the player is
vec_t CBasePlayer::GetTall()
{
	return m_flEyeOffset;
}

// Sets the player's walk speed
void CBasePlayer::SetWalkSpeed(float flSpeed)
{
	m_flWalkSpeed = flSpeed;
}

// Returns the player's walk speed
float CBasePlayer::GetWalkSpeed()
{
	return m_flWalkSpeed;
}

// Probably used for jumping???
void CBasePlayer::Jump()
{
	// Can't jump in the air, duh
	if (!m_bOnGround)
		return;

	SetVelocity(Vector(0, 200, 0));
	m_bOnGround = false;

	PlayFootstep();
}

void CBasePlayer::PlayFootstep()
{
	static int foot = 0;

	int stepNum;

	if (foot)
		stepNum = random(1, 2);
	else
		stepNum = random(3, 4);

	foot = !foot;

	string text = "footsteps/tile";
	text += to_string(stepNum);
	text += ".wav";

	start_music(GetWC(&text[0u]), SND_FOOTSTEP);
}

// Additive velocity
void CBasePlayer::SetVelocity(const Vector &velocity)
{
	m_Velocity += velocity;
}

// Absolute velocity. Used for applying immediate force, should be reserved for movement
void CBasePlayer::SetAbsVelocity(const Vector &velocity)
{
	m_Velocity = velocity;
}

// Returns a copy of the player's velocity
Vector CBasePlayer::GetVelocity()
{
	Vector dst;
	VectorCopy(m_Velocity, dst);

	return dst;
}

// Called when the player presses a key on their keyboard
void CBasePlayer::KeyPress(UINT vk, BOOL bPressed, int iRepeat, UINT flags)
{
	switch (vk)
	{
	case KEY_SPACE:
		// Prevent player from holding space and jumping repeatedly
		if (!(keys & IN_JUMP))
			Jump();

		break;

	default:
		break;
	}
}

// Called when the player releases a key on their keyboard
void CBasePlayer::KeyRelease(UINT vk, BOOL bPressed, int iRepeat, UINT flags)
{
	switch (vk)
	{
	case KEY_SPACE:
		break;

	default:
		break;
	}
}

// Returns the position of the player's head
Vector CBasePlayer::GetEyePos()
{
	return GetPos() + Vector(0, GetTall(), 0);
}

// You probably shouldn't edit this
XMMATRIX CBasePlayer::GetViewMatrix(XMMATRIX *engineView)
{
	Vector vecEye = GetEyePosInternal();
	QAngle eyeAngles = GetEyeAngles().GetInRadians();

	XMMATRIX mRotationY = XMMatrixRotationY(eyeAngles.y);
	XMMATRIX mRotationX = XMMatrixRotationX(-eyeAngles.x);
	XMMATRIX mRotationZ = XMMatrixRotationZ(eyeAngles.z);

	XMMATRIX mTranslate = XMMatrixTranslation(-vecEye.x, -vecEye.y, -vecEye.z);

	return mTranslate * (*engineView) * mRotationY * mRotationX;
}

// Deprecated
void CBasePlayer::Command(unsigned int buttons)
{

}

// Called every frame. Never call this manually!
void CBasePlayer::Think(float frameTime)
{
	// Pull apart out bitflag for the numbers we want
	bool forward = (keys & IN_FORWARD) != 0;
	bool backward = (keys & IN_BACK) != 0;
	bool left = (keys & IN_LEFT) != 0;
	bool right = (keys & IN_RIGHT) != 0;
	bool jump = (keys & IN_JUMP) != 0;

	bool bMoving = false;

	double curTime = RunTime();

	QAngle eyeAng = GetEyeAngles();
	QAngle eyeForward = QAngle(0, eyeAng.y, 0);
	Vector moveDir = Vector(0, 0, 0);
	Vector vecForward;
	Vector vecRight;
	Vector vecUp;

	AngleVectorsXM(eyeForward, &vecForward, &vecRight, &vecUp);
	VectorNormalize(vecForward);

	if (forward)
	{
		bMoving = true;
		moveDir = vecForward;
	}
	else if (backward)
	{
		bMoving = true;
		moveDir = -vecForward;
	}

	if (right)
	{
		bMoving = true;
		moveDir += vecRight;
	}
	else if (left)
	{
		bMoving = true;
		moveDir -= vecRight;
	}

	Vector velocity = GetVelocity();
	Vector localVelocity = moveDir * GetWalkSpeed();

	if (m_bOnGround)
		velocity += localVelocity;

	// Collision detection

	Vector predPos = pos + velocity * frameTime;
	
	Vector min;
	Vector max;

	min.x = -1150;
	min.y = 0;
	min.z = -1450;

	max.x = 1150;
	max.y = 0;
	max.z = 850;

	if (predPos.x < -375 && predPos.z > -675 && predPos.z < -600) {

		velocity.x = 0;
	}

	if (predPos.x > -600 && predPos.x < -400 && predPos.z > -700 && predPos.z < -600) {

		velocity.z = 0;

		if (!playerCount[1])
		{
			playerCount[1]++;

			HandleRecording(1);
		}
	}


	if (predPos.z < 120 && predPos.z > -120 && predPos.x >= 175 && predPos.x < 250) {

		velocity.x = 0;
		if (!playerCount[0])
		{
			playerCount[0]++;
			HandleRecording(0);
		}
	}

	if (predPos.z < 120 && predPos.z > -120 && predPos.x <= 350 && predPos.x > 300) {
		velocity.x = 0;

		if (!playerCount[0])
		{
			playerCount[0]++;
			HandleRecording(0);
		}
	}

	if (predPos.x < 300 && predPos.x > 200 && predPos.z < 160 && predPos.z > 100 && predPos.z < 300) {
		
		velocity.z = 0;
		if (!playerCount[0])
		{
			playerCount[0]++;
			HandleRecording(0);
		}
	}


	if (predPos.x < 300 && predPos.x > 200 && predPos.z > -160 && predPos.z < 0) {
		
		//PLAYER.SetPosComponent(2, -160);
		velocity.z = 0;

		if (!playerCount[0])
		{
			playerCount[0]++;
			HandleRecording(0);
		}
	}



	if (!(predPos.WithinAABox(min, max))) {

		if (predPos.x > max.x - 100)
			velocity.x = 0;
			//PLAYER.SetPosComponent(0, max.x);

		if (predPos.x < min.x + 100)
			velocity.x = 0;
			//PLAYER.SetPosComponent(0, min.x);

		if (predPos.z > max.z - 100)
			velocity.z = 0;
			//PLAYER.SetPosComponent(2, max.z);

		if (predPos.z < min.z + 100)
			velocity.z = 0;
			//PLAYER.SetPosComponent(2, min.z);

	}


	

	if (predPos.z < 600 && predPos.z > -350 && predPos.x >= 550) {

		velocity.x = 0;
		//pos.x = 550;
	}


	if (predPos.x < 600 && predPos.x > -260 && predPos.z >= 500) {

		//PLAYER.SetPosComponent(2, 500);
		velocity.z = 0;
	}

	if (predPos.z < 900 && predPos.z > 590 && predPos.x >= -350) {

		//PLAYER.SetPosComponent(0, -350);
		velocity.x = 0;
	}


	if (predPos.x < -315 && predPos.x > -1200 && predPos.z <= -250 && predPos.z > -300) {

		//PLAYER.SetPosComponent(2, -250);
		velocity.z = 0;
	}

	if (predPos.x < 600 && predPos.x > 0 && predPos.z <= -250 && predPos.z > -300) {

		//PLAYER.SetPosComponent(2, -250);
		velocity.z = 0;
	}


	if (predPos.z < -275 && predPos.z > -590 && predPos.x >= -50) {

		//PLAYER.SetPosComponent(0, -50);
		velocity.x = 0;
	}


	if (predPos.z < -300 && predPos.z > -590 && predPos.x <= -250) {

		//PLAYER.SetPosComponent(0, -250);
		velocity.x = 0;
	}



	if (predPos.x < 1100 && predPos.x > -600 && predPos.z <= -550 && predPos.z > -600) {


		if (!(predPos.x < -100 && predPos.x > -200)) {
			//PLAYER.SetPosComponent(2, -550);
			velocity.z = 0;
		}



	}


	if (predPos.z < -595 && predPos.z > -610) {

		velocity.x = 0;

	}


	if (predPos.x < 1200 && predPos.x > -600 && predPos.z > -650 && predPos.z < -615) {


		if (!(predPos.x < -100 && predPos.x > -200)) {
			//PLAYER.SetPosComponent(2, -650);
			velocity.z = 0;
		}

		if (predPos.z < -604 && predPos.z > -615) {

			velocity.z = velocity.x;
			
		}


	}


	if (predPos.x < 280 && predPos.x > -625 && predPos.z <= -1120) {

		//PLAYER.SetPosComponent(2, -1120);
		velocity.z = 0;
	}


	if (predPos.z > -1200 && predPos.z < -500 && predPos.x <= -550) {

		//PLAYER.SetPosComponent(0, -550);
		velocity.x = 0;
	}




	if (predPos.z < -600 && predPos.z > -1150 && predPos.x > 275 && predPos.x < 300) {


		if (!(predPos.z < -700 && predPos.z > -800)) {
			velocity.x = 0;

		}



	}


	if (predPos.z < -600 && predPos.z > -1450 && predPos.x <= 350 && predPos.x > 300) {

		//velocity.x = 0;
		if (!(predPos.z < -700 && predPos.z > -800)) {
			//PLAYER.SetPosComponent(0, 350);
			velocity.x = 0;
		}

	}


	if (predPos.x > 650 && predPos.x < 750 && predPos.z < -1200) {

		
		velocity.z = 0;

		if (!playerCount[2])
		{
			playerCount[2]++;
			HandleRecording(2);
		}
	}


	if (predPos.x > 930 && predPos.x < 1030 && predPos.z < -1230 && predPos.z > -1300) {
		
		velocity.z = 0;

		if (!playerCount[2])
		{
			playerCount[2]++;
			HandleRecording(2);
		}

	}

	if (predPos.x > 930 && predPos.x < 1030 && predPos.z > -1375 && predPos.z < -1250) {
		
		velocity.z = 0;

		if (!playerCount[2])
		{
			playerCount[2]++;
			HandleRecording(2);
		}

	}


	if (predPos.z > -1340 && predPos.z < -1240 && predPos.x < 1070 && predPos.x > 1000) {
		
		velocity.x = 0;

		if (!playerCount[2])
		{
			playerCount[2]++;
			HandleRecording(2);
		}

	}

	if (predPos.z > -1340 && predPos.z < -1240 && predPos.x > 930 && predPos.x < 1000) {
		
		velocity.x = 0;

		if (!playerCount[2])
		{
			playerCount[2]++;
			HandleRecording(2);
		}
	}

	// Apply our built-up velocity and move
	pos += velocity * frameTime;

	if (pos.y <= 0)
	{
		pos.y = 0;

		if (!m_bOnGround)
		{
			m_bOnGround = true;

			PlayFootstep();
		}
	}

	if (!m_bOnGround)
		velocity.y -= 514.435695538f * frameTime;
	else
	{
		velocity.y = 0.0f;
		velocity.x *= 0.7f;
		velocity.z *= 0.7f;
	}

	SetAbsVelocity(velocity);

	// We're moving, check for footstep stuff

	HandleFootstep(curTime, frameTime, bMoving);
}

// Returns which IN_* flags are set on the player
unsigned int CBasePlayer::GetButtons()
{
	return keys;
}

// Sets which IN_* flags are set on the player
void CBasePlayer::SetButtons(unsigned int buttons)
{
	keys = buttons;
}

// Called when the class is constructed
void CBasePlayer::Initialize()
{
	SetPos(Vector(0, 0, 0));
	SetWalkSpeed(70.0f);
	SetTall(64.0f);
	SetEyeAngles(QAngle(0, 0.0f, 0));
	SetAbsVelocity(Vector(0, 0, 0));

	m_bOnGround = true;
}
