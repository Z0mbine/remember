Texture2D txDiffuse : register( t0 );
SamplerState samLinear : register( s0 );

cbuffer ConstantBuffer : register(b0)
{
	float4 info;
};

struct VS_INPUT
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

struct PS_INPUT
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output;

	output.position = input.position;
	output.tex = input.tex;
	output.normal = input.normal;

	return output;
}

float4 PS(PS_INPUT input) : SV_TARGET
{
	float4 textureColor;
	float4 color;
	textureColor = txDiffuse.Sample(samLinear, input.tex);
	//textureColor.rgba *= info;
	return textureColor;
}