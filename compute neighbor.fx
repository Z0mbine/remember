#include "shaderheader.fxh"

RWStructuredBuffer<uint> NeighborBuffer : register(u1);
RWStructuredBuffer<uint> count : register(u2);
RWStructuredBuffer<uint> Octree : register(u3);
RWStructuredBuffer<uint> CSDebugBuffer : register(u4);


cbuffer ConstantBuffer : register(b0)
	{
	float4 CBvalues;
	};

//********************************************************************************************************
int find_in_octree(uint binaryindex, int lvl);
[numthreads(NUM_THREADS, 1, 1)]
void NeighborIndexing(uint3 DTid : SV_DispatchThreadID)
	{
	int level = (int)CBvalues.x;
	int last_of_level = count[10 + level];
	int first_of_level = (level == 0 ? 0 : count[10 + level - 1]);
	int oct_element_size = (level == maxlevel ? OCT_ELEMENTSIZE_LL : OCT_ELEMENTSIZE);
	int oct_brick_idx = (level == maxlevel ? OCT_BRICKINDEX_LL : OCT_BRICKINDEX);
	int oct_binary_idx = (level == maxlevel ? OCT_BINARYINDEX_LL : OCT_BINARYINDEX);
	int actualnum = DTid.x*oct_element_size;
	int num_of_oct_elements_indices = last_of_level - first_of_level;
	int num_of_oct_elements = num_of_oct_elements_indices / oct_element_size;
	float fnumpassesflag = ceil((float)num_of_oct_elements / (float)(1 * NUM_THREADS));
	int numpassesflag = (int)fnumpassesflag;

	[allow_uav_condition]
	for (int i = 0; i < numpassesflag; i++)
		{

		int ocv_to_work_on_index = actualnum + (NUM_THREADS*oct_element_size)*i;
		if (ocv_to_work_on_index >= num_of_oct_elements_indices)
			break; // don't run if there are no more indices to work on

		ocv_to_work_on_index += first_of_level;

		int binaryindex = Octree[ocv_to_work_on_index + oct_binary_idx];
		int brick_n_neighbor_index_self = Octree[ocv_to_work_on_index + oct_brick_idx];
		int brick_n_neighbor_index_neighbor;

		int frontback_index = binaryindex & 1023;
		int leftright_index = binaryindex & 1047552;
		int updown_index = binaryindex & 1072693248;

		bool leftInBounds = ((leftright_index - 1024) >= 0);
		bool upInBounds = ((updown_index - 1048576) >= 0);
		bool frontInBounds = ((frontback_index - 1) >= 0);

		bool rightInBounds = ((leftright_index + 1024) < (power_2(level) << 10));
		bool downInBounds = ((updown_index + 1048576) < (power_2(level) << 20));
		bool backInBounds = ((frontback_index + 1) < power_2(level));

		if (leftInBounds) // 12
			{
			int leftguy = find_in_octree(updown_index | (leftright_index - 1024) | frontback_index, level);
			if (leftguy >= 0)
				{
				brick_n_neighbor_index_neighbor = Octree[leftguy + oct_brick_idx];
				NeighborBuffer[brick_n_neighbor_index_self*NB_COUNT + NB_M_L_M] = brick_n_neighbor_index_neighbor;
				NeighborBuffer[brick_n_neighbor_index_neighbor*NB_COUNT + NB_M_R_M] = brick_n_neighbor_index_self;
				}
			}
		if (frontInBounds) // 4
			{
			int frontguy = find_in_octree(updown_index | leftright_index | (frontback_index - 1), level);
			if (frontguy >= 0)
				{
				brick_n_neighbor_index_neighbor = Octree[frontguy + oct_brick_idx];
				NeighborBuffer[brick_n_neighbor_index_self*NB_COUNT + NB_F_M_M] = brick_n_neighbor_index_neighbor;
				NeighborBuffer[brick_n_neighbor_index_neighbor*NB_COUNT + NB_B_M_M] = brick_n_neighbor_index_self;
				}
			}
		if (upInBounds) // 10
			{
			int upguy = find_in_octree((updown_index - 1048576) | leftright_index | frontback_index, level);
			if (upguy >= 0)
				{
				brick_n_neighbor_index_neighbor = Octree[upguy + oct_brick_idx];
				NeighborBuffer[brick_n_neighbor_index_self*NB_COUNT + NB_M_M_U] = brick_n_neighbor_index_neighbor;
				NeighborBuffer[brick_n_neighbor_index_neighbor*NB_COUNT + NB_M_M_D] = brick_n_neighbor_index_self;
				}
			}

		if (leftInBounds && frontInBounds && upInBounds) // 0guy
			{
			int zeroguy = find_in_octree((updown_index - 1048576) | (leftright_index - 1024) | (frontback_index - 1), level);
			if (zeroguy >= 0)
				{
				brick_n_neighbor_index_neighbor = Octree[zeroguy + oct_brick_idx];
				NeighborBuffer[brick_n_neighbor_index_self*NB_COUNT + NB_F_L_U] = brick_n_neighbor_index_neighbor;
				NeighborBuffer[brick_n_neighbor_index_neighbor*NB_COUNT + NB_B_R_D] = brick_n_neighbor_index_self;
				}
			}
		if (frontInBounds && upInBounds) // 1guy
			{
			int oneguy = find_in_octree((updown_index - 1048576) | leftright_index | (frontback_index - 1), level);
			if (oneguy >= 0)
				{
				brick_n_neighbor_index_neighbor = Octree[oneguy + oct_brick_idx];
				NeighborBuffer[brick_n_neighbor_index_self*NB_COUNT + NB_F_M_U] = brick_n_neighbor_index_neighbor;
				NeighborBuffer[brick_n_neighbor_index_neighbor*NB_COUNT + NB_B_M_D] = brick_n_neighbor_index_self;
				}
			}
		if (rightInBounds && frontInBounds && upInBounds) // 2guy
			{
			int twoguy = find_in_octree((updown_index - 1048576) | (leftright_index + 1024) | (frontback_index - 1), level);
			if (twoguy >= 0)
				{
				brick_n_neighbor_index_neighbor = Octree[twoguy + oct_brick_idx];
				NeighborBuffer[brick_n_neighbor_index_self*NB_COUNT + NB_F_R_U] = brick_n_neighbor_index_neighbor;
				NeighborBuffer[brick_n_neighbor_index_neighbor*NB_COUNT + NB_B_L_D] = brick_n_neighbor_index_self;
				}
			}
		if (leftInBounds && frontInBounds) // 3guy
			{
			int threeguy = find_in_octree(updown_index | (leftright_index - 1024) | (frontback_index - 1), level);
			if (threeguy >= 0)
				{
				brick_n_neighbor_index_neighbor = Octree[threeguy + oct_brick_idx];
				NeighborBuffer[brick_n_neighbor_index_self*NB_COUNT + NB_F_L_M] = brick_n_neighbor_index_neighbor;
				NeighborBuffer[brick_n_neighbor_index_neighbor*NB_COUNT + NB_B_R_M] = brick_n_neighbor_index_self;
				}
			}
		if (rightInBounds && frontInBounds) // 5guy
			{
			int fiveguy = find_in_octree(updown_index | (leftright_index + 1024) | (frontback_index - 1), level);
			if (fiveguy >= 0)
				{
				brick_n_neighbor_index_neighbor = Octree[fiveguy + oct_brick_idx];
				NeighborBuffer[brick_n_neighbor_index_self*NB_COUNT + NB_F_R_M] = brick_n_neighbor_index_neighbor;
				NeighborBuffer[brick_n_neighbor_index_neighbor*NB_COUNT + NB_B_L_M] = brick_n_neighbor_index_self;
				}
			}
		if (leftInBounds && frontInBounds && downInBounds) // 6guy
			{
			int sixguy = find_in_octree((updown_index + 1048576) | (leftright_index - 1024) | (frontback_index - 1), level);
			if (sixguy >= 0)
				{
				brick_n_neighbor_index_neighbor = Octree[sixguy + oct_brick_idx];
				NeighborBuffer[brick_n_neighbor_index_self*NB_COUNT + NB_F_L_D] = brick_n_neighbor_index_neighbor;
				NeighborBuffer[brick_n_neighbor_index_neighbor*NB_COUNT + NB_B_R_U] = brick_n_neighbor_index_self;
				}
			}
		if (frontInBounds && downInBounds) // 7guy
			{
			int sevenguy = find_in_octree((updown_index + 1048576) | leftright_index | (frontback_index - 1), level);
			if (sevenguy >= 0)
				{
				brick_n_neighbor_index_neighbor = Octree[sevenguy + oct_brick_idx];
				NeighborBuffer[brick_n_neighbor_index_self*NB_COUNT + NB_F_M_D] = brick_n_neighbor_index_neighbor;
				NeighborBuffer[brick_n_neighbor_index_neighbor*NB_COUNT + NB_B_M_U] = brick_n_neighbor_index_self;
				}
			}
		if (rightInBounds && frontInBounds && downInBounds) // 8guy
			{
			int eightguy = find_in_octree((updown_index + 1048576) | (leftright_index + 1024) | (frontback_index - 1), level);
			if (eightguy >= 0)
				{
				brick_n_neighbor_index_neighbor = Octree[eightguy + oct_brick_idx];
				NeighborBuffer[brick_n_neighbor_index_self*NB_COUNT + NB_F_R_D] = brick_n_neighbor_index_neighbor;
				NeighborBuffer[brick_n_neighbor_index_neighbor*NB_COUNT + NB_B_L_U] = brick_n_neighbor_index_self;
				}
			}
		if (leftInBounds && upInBounds) // 9guy
			{
			int nineguy = find_in_octree((updown_index - 1048576) | (leftright_index - 1024) | frontback_index, level);
			if (nineguy >= 0)
				{
				brick_n_neighbor_index_neighbor = Octree[nineguy + oct_brick_idx];
				NeighborBuffer[brick_n_neighbor_index_self*NB_COUNT + NB_M_L_U] = brick_n_neighbor_index_neighbor;
				NeighborBuffer[brick_n_neighbor_index_neighbor*NB_COUNT + NB_M_R_D] = brick_n_neighbor_index_self;
				}
			}
		if (rightInBounds && upInBounds) // 11guy
			{
			int elevenguy = find_in_octree((updown_index - 1048576) | (leftright_index + 1024) | frontback_index, level);
			if (elevenguy >= 0)
				{
				brick_n_neighbor_index_neighbor = Octree[elevenguy + oct_brick_idx];
				NeighborBuffer[brick_n_neighbor_index_self*NB_COUNT + NB_M_R_U] = brick_n_neighbor_index_neighbor;
				NeighborBuffer[brick_n_neighbor_index_neighbor*NB_COUNT + NB_M_L_D] = brick_n_neighbor_index_self;
				}
			}
		}
	}

//returns index of the start of a section of the octree
int find_in_octree(uint binaryindex, int lvl)
	{

	uint octdex = 0;
	uint currdex = 0;
	int maxlvl = min(lvl, maxlevel);

	[allow_uav_condition]
	for (uint level = 0; level < maxlvl; level++)
		{
		uint level_offset = maxlvl - level - 1;
		uint frontback_mask = 1 << level_offset;
		uint leftright_mask = 1 << (level_offset + 10);
		uint updown_mask = 1 << (level_offset + 20);
		uint idx = ((binaryindex & frontback_mask) >> level_offset) + ((binaryindex & leftright_mask) >> (level_offset + 10 - 1)) + ((binaryindex & updown_mask) >> (level_offset + 20 - 2));

		currdex = octdex + idx;
		uint octsr = Octree[currdex];

		if ((level + 1) == maxlvl)
			{
			if (octsr > 0)
				return octsr;
			else
				return -1;
			}
		else if (octsr > 0)
			octdex = octsr;
		else
			return -1;
		}

	return -1;
	}
