#pragma once

#ifndef UTIL_H
#define UTIL_H

#include <map>
#include "rendertexture.h"

inline std::wstring s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

inline std::vector<std::string> GetFileNamesInDirectory(const std::string directory)
{
	char DefChar = ' ';
	std::vector<std::string> files;
	WIN32_FIND_DATA fileData;
	HANDLE hFind;

	std::wstring dir(directory.begin(), directory.end());

	if ((hFind = FindFirstFile(dir.c_str(), &fileData)) != INVALID_HANDLE_VALUE)
	{
		while (FindNextFile(hFind, &fileData))
		{
			char fileName[260];
			WideCharToMultiByte(CP_ACP, 0, fileData.cFileName, -1, fileName, 260, &DefChar, NULL);
			std::string name = fileName;
			files.push_back(name);
		}
	}

	FindClose(hFind);
	return files;
}

#endif // UTIL_H