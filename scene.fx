#include "shaderheader.fxh"

Texture2D txDiffuse : register(t0);
StructuredBuffer<unsigned int> Octree_SR : register(t1);
Texture3D<float3> BrickBuffer_SR : register(t2);
Texture2D txShadowMap : register(t3);

Texture2D txPositions : register(t4);
Texture2D txNormals : register(t5);
Texture2D txTangents : register(t6);
Texture2D txDiffuseMap : register(t7);
Texture2D txDeferredShadowMap : register(t8);
Texture2D flashlightMask : register(t9);

SamplerState samLinear : register(s0);
SamplerState samPoint : register(s1);

cbuffer ConstantBuffer : register(b0)
	{
	matrix World;
	matrix View;
	matrix Projection;
	matrix LightViewProjection;
	float4 info;
	float4 campos;
	float4 sun_position; //rotation of the sun
	float4 daytimer; //day+night cycle
	};

cbuffer ConstantBufferClient : register(b1)
{
	float4 v1;
	float4 v2;
	float4 v3;
	float4 v4;
};

struct VS_INPUT
	{
	float3 Pos : POSITION;
	float2 Tex : TEXCOORD0;
	float3 Norm : NORMAL0;
	float3 Tan : TANGENT0;
	float3 Bi : BiNORMAL0;
	};

struct PS_INPUT
	{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float3 WorldPos : TEXCOORD1;
	float4 LightPos : TEXCOORD2;
	float4 Norm : NORMAL0;
	float3 Tan : TANGENT0;
	float3 Bi : BINORMAL0;
	
	};

struct octree_info
	{
	float3 midpoint;
	int brick_current_level;
	int brick_upper_level;
	int octree_level;
	};


struct color_transport
	{
	float3 color;
	int level;
	};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT VS(VS_INPUT input)
	{
	PS_INPUT output = (PS_INPUT)0;
	float4 pos = float4(input.Pos, 1);
	output.Pos = mul(input.Pos, World);
	output.WorldPos = output.Pos;
	output.Pos = mul(output.Pos, View);
	output.Pos = mul(output.Pos, Projection);
	output.Tex = input.Tex;

	matrix rot = World;
	//rot._41 = rot._42 = rot._43 = 0;
	//rot._14 = rot._24 = rot._34 = 0;

	output.Norm.xyz = normalize(mul(input.Norm, rot));
	output.Tan.xyz = normalize(mul(input.Tan, rot));
	output.Bi.xyz = normalize(mul(input.Bi, rot));

	return output;
	}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------

float4 find_in_octree(float3 pos,int lvl) 
	{

	uint octdex = 0;
	uint currdex = 0;
	float3 midpt = float3(0, 0, 0);
	float midsize = vxarea / 4.;
	uint idx = 0;
	int bin_back = 1;
	int bin_right = 2;
	int bin_down = 4;
	int maxlvl = min(lvl, maxlevel);
	for (uint level = 0; level <= maxlvl; level++)
		{

		int bin = 0;
		if (level != maxlvl) {
			if (pos.z < midpt.z)// back
				{
				bin |= bin_back;
				midpt.z -= midsize;
				}
			else
				{
				midpt.z += midsize;
				}
			if (pos.x >= midpt.x)//right
				{
				bin |= bin_right;
				midpt.x += midsize;
				}
			else
				{
				midpt.x -= midsize;
				}
			if (pos.y < midpt.y)//down
				{
				bin |= bin_down;
				midpt.y -= midsize;
				}
			else
				{
				midpt.y += midsize;

				}
			idx = bin;

			midsize = midsize / 2.;
			}
		else
			idx = 0;

		currdex = octdex + idx;
		uint octsr = Octree_SR[currdex];
		uint brick = Octree_SR[currdex+8];
		if (level < maxlvl)
			{
			if (octsr > 0)
				octdex = octsr;
			else
				return float4(midpt, -1);
			}
		else if (level == maxlevel)
			return float4(midpt, octsr);
		else if (level == maxlvl)
			return float4(midpt, brick);
		else
			return float4(midpt, -1);

		}

	return float4(midpt, -1);
	}
//*******************************************************
octree_info find_in_octree_indices(float3 pos, int lvl)
	{
	octree_info outdata;
	outdata.midpoint = float3(0, 0, 0);
	outdata.brick_current_level = -1;
	outdata.brick_upper_level = -1;
	uint octdex = 0;
	uint currdex = 0;
	float3 midpt = float3(0, 0, 0);
	float midsize = vxarea / 4.;
	uint idx = 0;
	int bin_back = 1;
	int bin_right = 2;
	int bin_down = 4;
	int maxlvl = min(lvl, maxlevel);
	int last_lvl_index = 0;
	for (uint level = 0; level <= maxlvl; level++)
		{

		int bin = 0;
		if (level != maxlvl) {
			if (pos.z < midpt.z)// back
				{
				bin |= bin_back;
				midpt.z -= midsize;
				}
			else
				{
				midpt.z += midsize;
				}
			if (pos.x >= midpt.x)//right
				{
				bin |= bin_right;
				midpt.x += midsize;
				}
			else
				{
				midpt.x -= midsize;
				}
			if (pos.y < midpt.y)//down
				{
				bin |= bin_down;
				midpt.y -= midsize;
				}
			else
				{
				midpt.y += midsize;

				}
			idx = bin;

			midsize = midsize / 2.;
			}
		else
			idx = 0;

		
		currdex = octdex + idx;
		outdata.octree_level = level;
		uint octsr = Octree_SR[currdex];
		
		if (level < maxlvl)
			{
			last_lvl_index = octdex;
			if (octsr > 0)
				octdex = octsr;
			else
				return outdata;
			
			}
		else if (level == maxlevel)
			{
		
			outdata.midpoint = midpt;
			outdata.brick_current_level = octsr;
			outdata.brick_upper_level = Octree_SR[last_lvl_index + 8];
			return outdata;
			}
		else if (level == maxlvl) 
			{
			outdata.midpoint = midpt;
			uint brick = Octree_SR[currdex + 8];
			outdata.brick_current_level = brick;
			outdata.brick_upper_level = Octree_SR[last_lvl_index + 8];
			return outdata;
			}
		else
			return outdata;
		}

	return outdata;
	}
float3 get_brick_color(int brick_index,float3 minions_pos)
	{
	int4 redbbi = int4(dto3d(brick_index) + uint3(1, 1, 1), 0);
	int4 greenbbi = int4(dto3d(brick_index + brickpoolsector) + uint3(1, 1, 1), 0);
	int4 bluebbi = int4(dto3d(brick_index + 2 * brickpoolsector) + uint3(1, 1, 1), 0);

	float3 fredbbi = float3((redbbi.x + minions_pos.x + 0.5) / (float)BRICKPOOLSIZE, (redbbi.y + minions_pos.y + 0.5) / (float)BRICKPOOLSIZE, (redbbi.z + minions_pos.z + 0.5) / (float)BRICKPOOLSIZE);
	float3 fgreenbbi = float3((greenbbi.x + minions_pos.x + 0.5) / (float)BRICKPOOLSIZE, (greenbbi.y + minions_pos.y + 0.5) / (float)BRICKPOOLSIZE, (greenbbi.z + minions_pos.z + 0.5) / (float)BRICKPOOLSIZE);
	float3 fbluebbi = float3((bluebbi.x + minions_pos.x + 0.5) / (float)BRICKPOOLSIZE, (bluebbi.y + minions_pos.y + 0.5) / (float)BRICKPOOLSIZE, (bluebbi.z + minions_pos.z + 0.5) / (float)BRICKPOOLSIZE);

	float3 brick_color;
	brick_color.r = BrickBuffer_SR.SampleLevel(samLinear, fredbbi,0);
	brick_color.g = BrickBuffer_SR.SampleLevel(samLinear, fgreenbbi, 0);
	brick_color.b = BrickBuffer_SR.SampleLevel(samLinear, fbluebbi, 0);
	return brick_color;
	
	}

color_transport get_interpolated_octree_color(float3 pos, float lod, int step)
	{
	color_transport ret;
	
	int ilod = (int)lod;
	//lod = 7.4;
	float flod = lod - floor(lod);
	if (flod > 0.01)
		ilod++;
	ilod = max(ilod, 2);


	octree_info inf = find_in_octree_indices(pos, ilod);
	ret.level = inf.octree_level;
	ret.color = float3(0, 0, 0);


	if (inf.brick_current_level < 0) return ret;
	if (inf.brick_upper_level < 0)	 return ret;

	float maxlength = vxarea / pow(2, ilod);
	float3 minmod = float3(maxlength, maxlength, maxlength);
	float3 minions = (inf.midpoint - pos) / minmod;

	float3 colA = get_brick_color(inf.brick_current_level, minions);
	float3 colB = get_brick_color(inf.brick_upper_level, minions);
	
	ret.color = colA;
	//if (flod <= 0.01)
		//return ret;

	if (info.y >= 3)
		ret.color= colB;
	if (info.y == 2)
		ret.color = colA;
	else if (info.y <= 1)
		ret.color = colA * flod + colB * (1 - flod);
	return ret;
	}
/*
float3 cone_tracing(float3 pos, float3 direction, int steps)
	{

	float3 cone_color = float3(0, 0, 0);
	float4 accum = float4(0, 0, 0, 0);
	float minDiameter = 1;
	float minVoxelDiameterInv = 1. / minDiameter;
	float dist = minDiameter;
	float check = 0;
	for (int ii = 0; ii < steps; ii++)
		{
		float sampleDiameter = max(minDiameter, dist);
		float sampleLOD = log2(sampleDiameter * minVoxelDiameterInv);
		sampleLOD = min(sampleLOD, 6);
		sampleLOD = 8 - sampleLOD;
		float3 samplePos = pos;
		samplePos += direction * dist;
		cone_color += get_interpolated_octree_color(samplePos, sampleLOD);
		
		if (ii == 0)
			{
			check = cone_color.r;
			}
		dist += sampleDiameter;
		}
	if (check > 0.1)
		return float3(0, 0, 0);
	return cone_color;
	}*/
/*color_transport cone_tracing_reflect(float3 pos, float3 direction, int steps)
	{
	float voxelsize = 1;
	float3 start = pos + direction;
	float dist_mul = 1.0;
	float angle = 0.785398163;
	color_transport cone_color;
	cone_color.color = float3(0, 0, 0);
	cone_color.level = 0;
	float check = 0;
	float3 t;
	float d;
	float miplevel;
	float sampleLOD;
	float3 dir = direction;
	for (int ii = 0; ii < steps; ii++)
		{
		t = dir*dist_mul;
		d = 2 * length(t) * tan(angle / 2.0);
		miplevel = log2(d / voxelsize) - 0.1;
		//miplevel *= 1.6;
		miplevel /= 5.0;
		miplevel = max(miplevel, 0);
		sampleLOD = maxlevel - miplevel;
		sampleLOD = max(sampleLOD, 2);
		color_transport ct= get_interpolated_octree_color(start + t, sampleLOD, ii);
		cone_color.color += ct.color;
		cone_color.

		if (ii == 0)
			{
			check = cone_color.r;
			}
		dist_mul += d ;
		}
	//if (check > 0.1)
	//return float3(0, 0, 0);

	return cone_color;
	}
	*/

float3 cone_tracing(float3 pos, float3 direction, int steps,float lod_mul)
	{
	float voxelsize = 1;
	float3 start = pos + direction;
	float dist_mul = 1.0;
	float angle = 0.785398163;
	float3 cone_color = float3(0, 0, 0);
	float check = 0;
	float3 t;
	float d;
	float miplevel;
	float sampleLOD;
	float3 dir = direction;
	for (int ii = 0; ii < steps; ii++)
		{
		t = dir*dist_mul;
		d = 2 * length(t) * tan(angle / 2.0);
		miplevel = log2(d/voxelsize) - 0.1;
		miplevel *= lod_mul;
		miplevel = max(miplevel, 0);
		sampleLOD = maxlevel - miplevel;
		sampleLOD = max(sampleLOD, 2);
		color_transport ct = get_interpolated_octree_color(start + t, sampleLOD, ii);
		

		if (ii < 3 && ct.level==maxlevel)
			{
			cone_color = float3(0, 0, 0);
			}
		else
			cone_color += ct.color;

		dist_mul +=d/ 2.7;
		}
	//if (check > 0.1)
		//return float3(0, 0, 0);
		
	return cone_color;
	}

float ambient_occlusion(float3 pos, float3 direction, int steps)
	{
	float voxelsize = 1;
	float3 start = pos + direction;
	float dist_mul = 0.02;
	float angle = 0.785398163;
	float check = 0;
	float3 t;
	float d;
	float miplevel;
	float sampleLOD;
	float3 dir = direction;
	int ii = 0;
	float f = 1;
	for (ii = 0; ii < steps; ii++)
		{
		t = dir*dist_mul;
		
		octree_info inf = find_in_octree_indices(start + t, maxlevel);
		if (inf.octree_level == maxlevel)
			{
			//f = 0.2;
			break;
			}
		dist_mul += (float)ii*0.03;
		}
	f = (float)ii / (float)steps;
	


	return f;
	}
/*
float3 single(float3 pos, float3 direction)
	{
	float voxelsize = 1;
	float3 start = pos + direction;
	float dist_mul = 1.0;
	float angle = 0.785398163;
	float3 cone_color = float3(0, 0, 0);
	float check = 0;
	for (int ii = 0; ii < 1; ii++)
		{
		float3 t = direction * dist_mul;
		float d = 2 * length(t) * tan(angle / 2.0);
		float miplevel = log2(d / voxelsize) - 0.1;
		miplevel *= 1.6;
		miplevel = max(miplevel, 0);
		float sampleLOD = 8 - miplevel;
		sampleLOD = max(sampleLOD, 2);

		cone_color += get_interpolated_octree_color(start + t, sampleLOD);

		if (ii == 0)
			{
			check = cone_color.r;
			}
		dist_mul += d / 2.7;
		}
	//if (check > 0.1)
	//return float3(0, 0, 0);
	return cone_color;
	}
	*/


struct PS_SCENE_INPUT
	{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	};
PS_SCENE_INPUT VS_scene(VS_INPUT input)
	{
	PS_SCENE_INPUT output = (PS_SCENE_INPUT)0;
	float4 pos = float4(input.Pos,1);
	output.Pos = pos;
	output.Tex = input.Tex;
	return output;
	}

SamplerComparisonState cmpSampler
	{
	// sampler state
	Filter = COMPARISON_MIN_MAG_MIP_LINEAR;
	AddressU = MIRROR;
	AddressV = MIRROR;

	// sampler comparison state
	ComparisonFunc = LESS_EQUAL;
	};
//**********************************************************************************************************************************************
/*
txPositions : register(t4);
Texture2D txNormals : register(t5);
Texture2D txDiffuseMap : register(t6)
*/

float4 PScompare(PS_SCENE_INPUT input) : SV_Target
{
	float4 worldpos = txPositions.Sample(samLinear, input.Tex);
	float4 normals = txNormals.Sample(samLinear, input.Tex);
	normals.rgb = (normals.rgb * 2.0) - float3(1, 1, 1);
	float4 diffuses = txDiffuseMap.Sample(samLinear, input.Tex);

	float4 color = diffuses;
	float4 lighting = float4(100, 100, 0, 1);// sun_position;
	float3 light_dir = lighting.xyz - worldpos;
	light_dir = normalize(light_dir);
	float lighti = dot(light_dir, normals.xyz);
	lighti = saturate(lighti);
	color.rgb *= lighti;
	color.a = 1;

	return color;
}


float4 PS(PS_SCENE_INPUT input) : SV_Target
{

	float4 worldpos = txPositions.Sample(samLinear, input.Tex);
	float4 normals = txNormals.Sample(samLinear, input.Tex);
	float4 tangents = txTangents.Sample(samLinear, input.Tex);
	float4 diffuses = txDiffuseMap.Sample(samLinear, input.Tex);
	float4 deferredshadow = txDeferredShadowMap.Sample(samLinear, input.Tex);
	float4 maskColor = flashlightMask.Sample(samLinear, input.Tex);
	
	
//	diffuses = float4(1, 1, 1, 1);
	normals.w = 1;
	tangents.w = 1;
	worldpos.w = 1;
	normals.rgb = (normals.rgb * 2.0) - float3(1, 1, 1);
	tangents.rgb = (tangents.rgb * 2.0) - float3(1, 1, 1);


	int curr_maxlevel = (int)info.x;
	float4 raw_texture_color = diffuses;


	
	float x=0, y=0;

	float tx, ty;
	tx = 1.0f / (float)WINRESX;
	ty = 1.0f / (float)WINRESY;

	//perform PCF filtering on a 4 x 4 texel neighborhood
	float4 sum = float4(0,0,0,0);
	for (y = -1.5; y <= 1.5; y += 1.0)
	{
		for (x = -1.5; x<= 1.5; x += 1.0)
		{
			sum += txDeferredShadowMap.SampleLevel(samLinear, input.Tex + float2(x * tx, y * ty), 0);
			//sum += txShadowMap.SampleCmpLevelZero(cmpSampler, projectTexCoord + texOffset(x, y), lightDepthValue);
		}
	}

	float shadowFactor = sum.r / 16.0;
	shadowFactor *= sum.b / 16.0;
	diffuses.rgb *= shadowFactor;

	float brightness = 3.0f;

	float zDist = distance(worldpos.xyz, campos.xyz);

	float distFrac = saturate(zDist / 900.0f);
	distFrac *= brightness;

	diffuses.rgb *= (brightness - distFrac);


	//diffuses.rgb *= deferredshadow;
	//return float4(diffuses.rgb,1);

	//return texture_color;f
	float3 n = normals;
	//return float4(n, 1);
	float3 bi = cross(normals, tangents);
	float3 tan = tangents;

	float3 lightSource = campos.xyz;

	float3 lp = lightSource;
	float3 dir = normalize(lp - worldpos.xyz);
	float3 light = dot(dir, n.xyz);
	light = pow(light, 0.3f);

	float fact = light.r;
	float infact = (fact + 1.) / 2.;
	float result = (fact + infact) / 2.;
	diffuses.a = 1;
	diffuses.rgb *= light;

	//texture_color.rgb *= 0.5;
	float3 currpos = worldpos;

	//texture_color = float4(0, 0, 0, 1);
	float3 cone_color = float3(0, 0, 0);


	float3 cam = float3(-campos.x, -campos.y, -campos.z);
	float3 cam_to_pix = normalize(worldpos.xyz - cam.xyz);

	//float3 reflectvec = normalize(reflect(cam_to_pix, n));

	float3 reflectvec = cam_to_pix - 2 * n * dot(cam_to_pix, n);


	float3 collect_color_normal = cone_tracing(currpos + n * 2, n,8,1.6);
	float3 collect_color_binormal = cone_tracing(currpos + n * 2, bi + n, 8, 1.6);
	float3 collect_color_n_binormal = cone_tracing(currpos + n * 2, -bi + n, 8, 1.6);
	float3 collect_color_tangent = cone_tracing(currpos + n * 2, tan + n, 8, 1.6);
	float3 collect_color_n_tangent = cone_tracing(currpos + n * 2, -tan + n, 8, 1.6);


	float3 collect_color_reflect = float3(0, 0, 0);
	if (dot(reflectvec, n) > 0)
		{
		collect_color_reflect = cone_tracing(currpos + n*2, reflectvec, 8,1.6);
		}

	float3 collect_color = collect_color_normal + collect_color_tangent*0.7 + collect_color_n_tangent*0.7 + collect_color_binormal*0.7 + collect_color_n_binormal*0.7;
	collect_color *= 1.0;

	//texture_color.rgb += collect_color;
	//texture_color.rgb = texture_color.rgb + collect_color_normal;
	float3 color = diffuses.rgb + raw_texture_color.rgb*collect_color +collect_color_reflect*1.2;


	return float4(color, 1);
}
//**********************************************************************************************************************************************
