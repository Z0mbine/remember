#include "shaderheader.fxh"

RWTexture3D<float> BrickBuffer : register(u1);
RWStructuredBuffer<uint> NeighborBuffer : register(u2);
RWStructuredBuffer<uint> count : register(u3);
RWStructuredBuffer<uint> CSDebugBuffer : register(u4);
RWStructuredBuffer<uint> Octree : register(u5);

cbuffer ConstantBuffer : register(b0)
	{
	float4 CBvalues;
	};

//********************************************************************************************************************************************
//
//				get kids color
//
//********************************************************************************************************************************************
[numthreads(NUM_THREADS, 1, 1)]
void CSkids_color(uint3 DTid : SV_DispatchThreadID, uint3 Gid : SV_GroupID)
	{
	//how many passes do you need? We have DTid.x going from 0 to NUM_THREADS
	int actualnum = DTid.x*OCT_ELEMENTSIZE;
	int level = (int)CBvalues.x;

	int last_of_level = count[10 + level];
	int first_of_level = (level == 0 ? 0 : count[10 + level - 1]);
	int num_of_flag_area = (last_of_level - first_of_level) / OCT_ELEMENTSIZE;//SURE?
	float fnumpassesflag = ceil((float)num_of_flag_area / (float)(NUM_THREADS));
	int numpassesflag = (int)fnumpassesflag;

	[allow_uav_condition]
	for (int i = 0; i < numpassesflag; i++)
		{
		int ocv_to_work_on = actualnum + (NUM_THREADS*OCT_ELEMENTSIZE)*i;
		if (ocv_to_work_on >= (num_of_flag_area*OCT_ELEMENTSIZE))
			break; // don't run if there are no more indices to work on

		ocv_to_work_on += first_of_level;

		float3 color = float3(0, 0, 0);
		int counter = 0;
		for (int uu = 0; uu < 8; uu++)
			{
			int kid = Octree[ocv_to_work_on + uu];

			if (kid == 0) continue;
			int oct_brick_idx = (maxlevel - level > 1 ? OCT_BRICKINDEX : OCT_BRICKINDEX_LL);
			int brickindex = Octree[kid + oct_brick_idx];

			color.r += BrickBuffer[dto3d(brickindex + 0 * brickpoolsector) + uint3(1, 1, 1)];
			color.b += BrickBuffer[dto3d(brickindex + 1 * brickpoolsector) + uint3(1, 1, 1)];
			color.g += BrickBuffer[dto3d(brickindex + 2 * brickpoolsector) + uint3(1, 1, 1)];
			counter++;
			}
		color /= 8;// counter;

		int self_brick = Octree[ocv_to_work_on + OCT_BRICKINDEX];

		uint3 redidx = dto3d(self_brick) + uint3(1, 1, 1);
		uint3 greenidx = dto3d(self_brick + brickpoolsector) + uint3(1, 1, 1);
		uint3 blueidx = dto3d(self_brick + 2 * brickpoolsector) + uint3(1, 1, 1);

		BrickBuffer[redidx] = color.r;
		BrickBuffer[greenidx] = color.b;
		BrickBuffer[blueidx] = color.g;
		}
	}
//**********************
[numthreads(NUM_THREADS, 1, 1)]
void ColorExchange(uint3 DTid : SV_DispatchThreadID)
	{
	int actualnum = DTid.x;
	int num_of_brick_elements = count[20 + maxlevel];

	float fnumpassesflag = ceil((float)num_of_brick_elements / (float)(NUM_THREADS));
	int numpassesflag = (int)fnumpassesflag;

	[allow_uav_condition]
	for (int i = 0; i < numpassesflag; i++)
		{

		int brick_num = actualnum + NUM_THREADS*i;
		if (brick_num >= num_of_brick_elements)
			break; // don't run if there are no more indices to work on

		//brick_num += count[20 + maxlevel - 1];

		int neighborindex = brick_num * NB_COUNT;

		int zeroguy = NeighborBuffer[neighborindex + NB_F_L_U];
		int oneguy = NeighborBuffer[neighborindex + NB_F_M_U];
		int twoguy = NeighborBuffer[neighborindex + NB_F_R_U];
		int threeguy = NeighborBuffer[neighborindex + NB_F_L_M];
		int frontguy = NeighborBuffer[neighborindex + NB_F_M_M];
		int fiveguy = NeighborBuffer[neighborindex + NB_F_R_M];
		int sixguy = NeighborBuffer[neighborindex + NB_F_L_D];
		int sevenguy = NeighborBuffer[neighborindex + NB_F_M_D];
		int eightguy = NeighborBuffer[neighborindex + NB_F_R_D];
		int nineguy = NeighborBuffer[neighborindex + NB_M_L_U];
		int upguy = NeighborBuffer[neighborindex + NB_M_M_U];
		int elevenguy = NeighborBuffer[neighborindex + NB_M_R_U];
		int leftguy = NeighborBuffer[neighborindex + NB_M_L_M];


		uint3 self_r_idx3, self_g_idx3, self_b_idx3;
		self_r_idx3 = dto3d(brick_num);
		self_g_idx3 = dto3d(brick_num + brickpoolsector);
		self_b_idx3 = dto3d(brick_num + 2 * brickpoolsector);

		if (leftguy > 0)
			{
			uint3 left_r_idx, left_g_idx, left_b_idx;
			left_r_idx = dto3d(leftguy);
			left_g_idx = dto3d(leftguy + brickpoolsector);
			left_b_idx = dto3d(leftguy + 2 * brickpoolsector);
			// left exchange
			BrickBuffer[self_r_idx3 + uint3(2, 1, 1)] = BrickBuffer[left_r_idx + uint3(1, 1, 1)];
			BrickBuffer[self_g_idx3 + uint3(2, 1, 1)] = BrickBuffer[left_g_idx + uint3(1, 1, 1)];
			BrickBuffer[self_b_idx3 + uint3(2, 1, 1)] = BrickBuffer[left_b_idx + uint3(1, 1, 1)];
			BrickBuffer[left_r_idx + uint3(0, 1, 1)] = BrickBuffer[self_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[left_g_idx + uint3(0, 1, 1)] = BrickBuffer[self_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[left_b_idx + uint3(0, 1, 1)] = BrickBuffer[self_b_idx3 + uint3(1, 1, 1)];
			}

		if (upguy > 0)
			{
			uint3 up_r_idx, up_g_idx, up_b_idx;
			up_r_idx = dto3d(upguy);
			up_g_idx = dto3d(upguy + brickpoolsector);
			up_b_idx = dto3d(upguy + 2 * brickpoolsector);
			// up exchange
			BrickBuffer[self_r_idx3 + uint3(1, 0, 1)] = BrickBuffer[up_r_idx + uint3(1, 1, 1)];
			BrickBuffer[self_g_idx3 + uint3(1, 0, 1)] = BrickBuffer[up_g_idx + uint3(1, 1, 1)];
			BrickBuffer[self_b_idx3 + uint3(1, 0, 1)] = BrickBuffer[up_b_idx + uint3(1, 1, 1)];
			BrickBuffer[up_r_idx + uint3(1, 2, 1)] = BrickBuffer[self_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[up_g_idx + uint3(1, 2, 1)] = BrickBuffer[self_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[up_b_idx + uint3(1, 2, 1)] = BrickBuffer[self_b_idx3 + uint3(1, 1, 1)];
			}

		if (frontguy > 0)
			{
			uint3 front_r_idx3, front_g_idx3, front_b_idx3;
			front_r_idx3 = dto3d(frontguy);
			front_g_idx3 = dto3d(frontguy + brickpoolsector);
			front_b_idx3 = dto3d(frontguy + 2 * brickpoolsector);
			// front exchange
			BrickBuffer[self_r_idx3 + uint3(1, 1, 0)] = BrickBuffer[front_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_g_idx3 + uint3(1, 1, 0)] = BrickBuffer[front_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_b_idx3 + uint3(1, 1, 0)] = BrickBuffer[front_b_idx3 + uint3(1, 1, 1)];
			BrickBuffer[front_r_idx3 + uint3(1, 1, 2)] = BrickBuffer[self_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[front_g_idx3 + uint3(1, 1, 2)] = BrickBuffer[self_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[front_b_idx3 + uint3(1, 1, 2)] = BrickBuffer[self_b_idx3 + uint3(1, 1, 1)];
			}

		if (zeroguy > 0)
			{
			uint3 zero_r_idx3, zero_g_idx3, zero_b_idx3;
			zero_r_idx3 = dto3d(zeroguy);
			zero_g_idx3 = dto3d(zeroguy + brickpoolsector);
			zero_b_idx3 = dto3d(zeroguy + 2 * brickpoolsector);
			// f/l/u exchange
			BrickBuffer[self_r_idx3 + uint3(2, 0, 0)] = BrickBuffer[zero_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_g_idx3 + uint3(2, 0, 0)] = BrickBuffer[zero_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_b_idx3 + uint3(2, 0, 0)] = BrickBuffer[zero_b_idx3 + uint3(1, 1, 1)];
			BrickBuffer[zero_r_idx3 + uint3(0, 2, 2)] = BrickBuffer[self_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[zero_g_idx3 + uint3(0, 2, 2)] = BrickBuffer[self_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[zero_b_idx3 + uint3(0, 2, 2)] = BrickBuffer[self_b_idx3 + uint3(1, 1, 1)];
			}

		if (oneguy > 0)
			{
			uint3 one_r_idx3, one_g_idx3, one_b_idx3;
			one_r_idx3 = dto3d(oneguy);
			one_g_idx3 = dto3d(oneguy + brickpoolsector);
			one_b_idx3 = dto3d(oneguy + 2 * brickpoolsector);
			// f/u exchange
			BrickBuffer[self_r_idx3 + uint3(1, 0, 0)] = BrickBuffer[one_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_g_idx3 + uint3(1, 0, 0)] = BrickBuffer[one_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_b_idx3 + uint3(1, 0, 0)] = BrickBuffer[one_b_idx3 + uint3(1, 1, 1)];
			BrickBuffer[one_r_idx3 + uint3(1, 2, 2)] = BrickBuffer[self_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[one_g_idx3 + uint3(1, 2, 2)] = BrickBuffer[self_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[one_b_idx3 + uint3(1, 2, 2)] = BrickBuffer[self_b_idx3 + uint3(1, 1, 1)];
			}

		if (twoguy > 0)
			{
			uint3 two_r_idx3, two_g_idx3, two_b_idx3;
			two_r_idx3 = dto3d(twoguy);
			two_g_idx3 = dto3d(twoguy + brickpoolsector);
			two_b_idx3 = dto3d(twoguy + 2 * brickpoolsector);
			// f/r/u exchange
			BrickBuffer[self_r_idx3 + uint3(0, 0, 0)] = BrickBuffer[two_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_g_idx3 + uint3(0, 0, 0)] = BrickBuffer[two_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_b_idx3 + uint3(0, 0, 0)] = BrickBuffer[two_b_idx3 + uint3(1, 1, 1)];
			BrickBuffer[two_r_idx3 + uint3(2, 2, 2)] = BrickBuffer[self_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[two_g_idx3 + uint3(2, 2, 2)] = BrickBuffer[self_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[two_b_idx3 + uint3(2, 2, 2)] = BrickBuffer[self_b_idx3 + uint3(1, 1, 1)];
			}

		if (threeguy > 0)
			{
			uint3 three_r_idx3, three_g_idx3, three_b_idx3;
			three_r_idx3 = dto3d(threeguy);
			three_g_idx3 = dto3d(threeguy + brickpoolsector);
			three_b_idx3 = dto3d(threeguy + 2 * brickpoolsector);
			// f/l exchange
			BrickBuffer[self_r_idx3 + uint3(2, 1, 0)] = BrickBuffer[three_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_g_idx3 + uint3(2, 1, 0)] = BrickBuffer[three_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_b_idx3 + uint3(2, 1, 0)] = BrickBuffer[three_b_idx3 + uint3(1, 1, 1)];
			BrickBuffer[three_r_idx3 + uint3(0, 1, 2)] = BrickBuffer[self_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[three_g_idx3 + uint3(0, 1, 2)] = BrickBuffer[self_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[three_b_idx3 + uint3(0, 1, 2)] = BrickBuffer[self_b_idx3 + uint3(1, 1, 1)];
			}

		if (fiveguy > 0)
			{
			uint3 five_r_idx3, five_g_idx3, five_b_idx3;
			five_r_idx3 = dto3d(fiveguy);
			five_g_idx3 = dto3d(fiveguy + brickpoolsector);
			five_b_idx3 = dto3d(fiveguy + 2 * brickpoolsector);
			// f/r exchange
			BrickBuffer[self_r_idx3 + uint3(0, 1, 0)] = BrickBuffer[five_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_g_idx3 + uint3(0, 1, 0)] = BrickBuffer[five_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_b_idx3 + uint3(0, 1, 0)] = BrickBuffer[five_b_idx3 + uint3(1, 1, 1)];
			BrickBuffer[five_r_idx3 + uint3(2, 1, 2)] = BrickBuffer[self_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[five_g_idx3 + uint3(2, 1, 2)] = BrickBuffer[self_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[five_b_idx3 + uint3(2, 1, 2)] = BrickBuffer[self_b_idx3 + uint3(1, 1, 1)];
			}

		if (sixguy > 0)
			{
			uint3 six_r_idx3, six_g_idx3, six_b_idx3;
			six_r_idx3 = dto3d(sixguy);
			six_g_idx3 = dto3d(sixguy + brickpoolsector);
			six_b_idx3 = dto3d(sixguy + 2 * brickpoolsector);
			// f/l/d exchange
			BrickBuffer[self_r_idx3 + uint3(2, 2, 0)] = BrickBuffer[six_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_g_idx3 + uint3(2, 2, 0)] = BrickBuffer[six_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_b_idx3 + uint3(2, 2, 0)] = BrickBuffer[six_b_idx3 + uint3(1, 1, 1)];
			BrickBuffer[six_r_idx3 + uint3(0, 0, 2)] = BrickBuffer[self_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[six_g_idx3 + uint3(0, 0, 2)] = BrickBuffer[self_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[six_b_idx3 + uint3(0, 0, 2)] = BrickBuffer[self_b_idx3 + uint3(1, 1, 1)];
			}

		if (sevenguy > 0)
			{
			uint3 seven_r_idx3, seven_g_idx3, seven_b_idx3;
			seven_r_idx3 = dto3d(sevenguy);
			seven_g_idx3 = dto3d(sevenguy + brickpoolsector);
			seven_b_idx3 = dto3d(sevenguy + 2 * brickpoolsector);
			// f/d exchange
			BrickBuffer[self_r_idx3 + uint3(1, 2, 0)] = BrickBuffer[seven_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_g_idx3 + uint3(1, 2, 0)] = BrickBuffer[seven_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_b_idx3 + uint3(1, 2, 0)] = BrickBuffer[seven_b_idx3 + uint3(1, 1, 1)];
			BrickBuffer[seven_r_idx3 + uint3(1, 0, 2)] = BrickBuffer[self_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[seven_g_idx3 + uint3(1, 0, 2)] = BrickBuffer[self_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[seven_b_idx3 + uint3(1, 0, 2)] = BrickBuffer[self_b_idx3 + uint3(1, 1, 1)];
			}

		if (eightguy > 0)
			{
			uint3 eight_r_idx3, eight_g_idx3, eight_b_idx3;
			eight_r_idx3 = dto3d(eightguy);
			eight_g_idx3 = dto3d(eightguy + brickpoolsector);
			eight_b_idx3 = dto3d(eightguy + 2 * brickpoolsector);
			// f/r/d exchange
			BrickBuffer[self_r_idx3 + uint3(0, 2, 0)] = BrickBuffer[eight_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_g_idx3 + uint3(0, 2, 0)] = BrickBuffer[eight_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_b_idx3 + uint3(0, 2, 0)] = BrickBuffer[eight_b_idx3 + uint3(1, 1, 1)];
			BrickBuffer[eight_r_idx3 + uint3(2, 0, 2)] = BrickBuffer[self_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[eight_g_idx3 + uint3(2, 0, 2)] = BrickBuffer[self_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[eight_b_idx3 + uint3(2, 0, 2)] = BrickBuffer[self_b_idx3 + uint3(1, 1, 1)];
			}

		if (nineguy > 0)
			{
			uint3 nine_r_idx3, nine_g_idx3, nine_b_idx3;
			nine_r_idx3 = dto3d(nineguy);
			nine_g_idx3 = dto3d(nineguy + brickpoolsector);
			nine_b_idx3 = dto3d(nineguy + 2 * brickpoolsector);
			// l/u exchange
			BrickBuffer[self_r_idx3 + uint3(2, 0, 1)] = BrickBuffer[nine_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_g_idx3 + uint3(2, 0, 1)] = BrickBuffer[nine_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_b_idx3 + uint3(2, 0, 1)] = BrickBuffer[nine_b_idx3 + uint3(1, 1, 1)];
			BrickBuffer[nine_r_idx3 + uint3(0, 2, 1)] = BrickBuffer[self_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[nine_g_idx3 + uint3(0, 2, 1)] = BrickBuffer[self_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[nine_b_idx3 + uint3(0, 2, 1)] = BrickBuffer[self_b_idx3 + uint3(1, 1, 1)];
			}

		if (elevenguy > 0)
			{
			uint3 eleven_r_idx3, eleven_g_idx3, eleven_b_idx3;
			eleven_r_idx3 = dto3d(elevenguy);
			eleven_g_idx3 = dto3d(elevenguy + brickpoolsector);
			eleven_b_idx3 = dto3d(elevenguy + 2 * brickpoolsector);
			// r/u exchange
			BrickBuffer[self_r_idx3 + uint3(0, 0, 1)] = BrickBuffer[eleven_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_g_idx3 + uint3(0, 0, 1)] = BrickBuffer[eleven_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[self_b_idx3 + uint3(0, 0, 1)] = BrickBuffer[eleven_b_idx3 + uint3(1, 1, 1)];
			BrickBuffer[eleven_r_idx3 + uint3(2, 2, 1)] = BrickBuffer[self_r_idx3 + uint3(1, 1, 1)];
			BrickBuffer[eleven_g_idx3 + uint3(2, 2, 1)] = BrickBuffer[self_g_idx3 + uint3(1, 1, 1)];
			BrickBuffer[eleven_b_idx3 + uint3(2, 2, 1)] = BrickBuffer[self_b_idx3 + uint3(1, 1, 1)];
			}
		}
	}
