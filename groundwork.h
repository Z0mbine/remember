#include "basetypes.h"
#include <time.h>
#include <io.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "resource.h"
#include "commonheader.h"
#include "Font.h"

using namespace std;

#define EPS 0.00001

float Vec3Length(const XMFLOAT3 &v);
float Vec3Dot(XMFLOAT3 a, XMFLOAT3 b);
XMFLOAT3 Vec3Cross(XMFLOAT3 a, XMFLOAT3 b);
XMFLOAT3 Vec3Normalize(const  XMFLOAT3 &a);
/*XMFLOAT3 operator+(const XMFLOAT3 lhs, const XMFLOAT3 rhs);
XMFLOAT3 operator*(const XMFLOAT3 lhs, const float rhs);
XMFLOAT3 operator-(const XMFLOAT3 lhs, const XMFLOAT3 rhs);*/
bool Load(char *filename, ID3D11Device* g_pd3dDevice, ID3D11Buffer **ppVertexBuffer, int *vertex_count, float scale = 1.0);
bool LoadEMS(char *filename, ID3D11Device* g_pd3dDevice, ID3D11Buffer **ppVertexBuffer, int *vertex_count, float scale = 1.0);
HRESULT CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);


//********************************************
class billboard
	{
	public:
		XMFLOAT3 position;
		float scale;
		float rotation;
		float transparency;

		billboard()
			{
			position = XMFLOAT3(0, 0, 0);
			scale = 1;
			transparency = 1;
			}

		XMMATRIX get_matrix(XMMATRIX &ViewMatrix)
			{
			XMMATRIX view, R, Ry, T, S;
			Ry = XMMatrixRotationX(-rotation);
			view = Ry * ViewMatrix;
			//eliminate camera translation:
			view._41 = view._42 = view._43 = 0.0;
			XMVECTOR det;
			R = XMMatrixInverse(&det, view); //inverse rotation
			T = XMMatrixTranslation(position.x, position.y, position.z);
			S = XMMatrixScaling(scale, scale, scale);
			return S*R*T*Ry;


			}

		XMMATRIX get_matrix_y(XMMATRIX &ViewMatrix) { }
	};



//********************************************
class voxel_
{
public:
	SimpleVertex *v;
	ID3D11Buffer *vbuffer;

	int anz;
	int size;
	voxel_()
	{
		vbuffer = NULL;
		size = vxarea;
		anz = (int)pow(size, 3);
		v = new SimpleVertex[anz];
		for (int xx = 0; xx < size; xx++)
			for (int yy = 0; yy < size; yy++)
				for (int zz = 0; zz < size; zz++)
				{
					v[xx + yy * size + zz * size * size].Pos.x = (float)xx;
					v[xx + yy * size + zz * size * size].Pos.y = (float)yy;
					v[xx + yy * size + zz * size * size].Pos.z = (float)zz;
				}
	}

	void Shutdown()
	{
		if (vbuffer) vbuffer->Release();
	}

	~voxel_()
	{
		delete[] v;
	}
};