#ifndef TIMER_H
#define TIMER_H
#pragma once

#include "basetypes.h"
#include <functional>

struct timer_t
{
	double expireTime;
	double delay;
	int repetitions;
	std::string uid;
	function_t func;
};

static std::vector<timer_t> TIMERS;

namespace Timer
{
	void Simple(double delay, function_t func);
	void Create(const char *uid, double delay, int repetitions, function_t func);
	void Remove(const char *uid);
	void Process();
}

#endif // TIMER_H