Texture2D txDiffuse : register(t0); //grass
Texture2D txDepth : register(t1); //height
Texture2D txNormal : register(t2); //normal
Texture2D txDiffuse2 : register(t3); //ROCK
Texture2D txDiffuse3 : register(t4); //SNOW
SamplerState samLinear : register(s0);

cbuffer ConstantBuffer : register(b0)
{
	matrix World;
	matrix View;
	matrix Projection;
	float4 info;
};

//--------------------------------------------------------------------------------------
struct VS_INPUT
{
	float4 Pos : POSITION;
	float2 Tex : TEXCOORD0;
	float3 Norm : NORMAL0;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float4 Col : COLOR0;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------

PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output = (PS_INPUT)0;
	float4 pos = input.Pos;
	float3 color = txDepth.SampleLevel(samLinear, input.Tex, 0);

	pos.y = color.r * -13;
	output.Pos = mul(pos, World);
	output.Pos = mul(output.Pos, View);
	output.Pos = mul(output.Pos, Projection);
	output.Tex = input.Tex;

	return output;
}

float remap(float value, float inMin, float inMax, float outMin, float outMax)
{
	return outMin + (((value - inMin) / (inMax - inMin)) * (outMax - outMin));
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	float3 textureNormal = txNormal.Sample(samLinear, input.Tex, 0);
	float3 depthColor = txDepth.Sample(samLinear, input.Tex, 0);

	float height = depthColor.r * -13.0f;
	height = height / -13.0f;

	float4 pos = input.Pos;
	pos = mul(pos, World);
	float2 grassScale = input.Tex;
	grassScale *= 250;
	float2 rockScale = input.Tex;
	rockScale *= 50;
	float2 snowScale = input.Tex;
	snowScale *= 100;

	float4 color = txDiffuse.Sample(samLinear, grassScale);
	float4 color2 = txDiffuse2.Sample(samLinear, rockScale);
	float4 color3 = txDiffuse3.Sample(samLinear, snowScale);

	textureNormal = (textureNormal * 2) - float3(1, 1, 1);

	float3 norm = mul(normalize(textureNormal), World);
	norm = normalize(norm);

	float3 lightpos = float3(1000, 3000, 0);

	float3 lightdirection = normalize(lightpos - pos.xyz);
	float3 light = dot(lightdirection, norm);

	color.rgb *= 0.7 + color.rgb * (light.rgb * 1); // 110% is on purpose
	color2.rgb *= 0.7 + color2.rgb * (light.rgb * 1); // 110% is on purpose

	float4 finalColor;

	if (height < 0.2)
	{
		finalColor = color;

		if (height <= 0.18)
		{
			float frac = remap(height, 0.18f, 0.0f, 0.0f, 1.0f);
			finalColor = lerp(color, float4(0, 0, 0, 1), frac);
		}
	}
	else if (height >= 0.2 && height < 0.4)
	{
		float frac = remap(height, 0.2f, 0.4f, 0.0f, 1.0f);

		finalColor = lerp(color, color2, frac);
	}
	else if (height >= 0.4 && height < 0.6)
		finalColor = color2;
	else if (height >= 0.6 && height < 0.7)
	{
		float frac = remap(height, 0.6f, 0.7f, 0.0f, 1.0f);

		finalColor = lerp(color2, color3, frac);
	}
	else
		finalColor = color3;

	finalColor.a = 1;

	return finalColor;
}