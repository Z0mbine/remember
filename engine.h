#pragma once

#ifndef ENGINE_H
#define ENGINE_H

#include "baseplayer.h"
using namespace std;

inline HRESULT CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut)
{
	HRESULT hr = S_OK;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

	ID3DBlob* pErrorBlob;
	hr = D3DX11CompileFromFile(szFileName, NULL, NULL, szEntryPoint, szShaderModel,
		dwShaderFlags, 0, NULL, ppBlobOut, &pErrorBlob, NULL);
	if (FAILED(hr))
	{
		if (pErrorBlob != NULL)
			OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());
		if (pErrorBlob) pErrorBlob->Release();
		return hr;
	}
	if (pErrorBlob) pErrorBlob->Release();

	return S_OK;
}

class CEngine
{
public:
	D3D_DRIVER_TYPE                     driverType = D3D_DRIVER_TYPE_NULL;
	D3D_FEATURE_LEVEL                   featureLevel = D3D_FEATURE_LEVEL_11_0;
	ID3D11Device*                       pd3dDevice = NULL;
	ID3D11DeviceContext*                pImmediateContext = NULL;
	IDXGISwapChain*                     pSwapChain = NULL;
	ID3D11Texture2D*                    pDepthStencil = NULL;
	ID3D11DepthStencilView*             pDepthStencilView = NULL;

	ID3D11InputLayout*                  pVertexLayout = NULL;

	map<string, VertexBufferData>		VertexBuffers;
	map<string, ID3D11VertexShader*>	VertexShaders;
	map<string, ID3D11PixelShader*>		PixelShaders;

	ID3D11BlendState*					BlendState;
	XMFLOAT4                            vMeshColor = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
	ID3D11VertexShader*	pVertexShader = NULL;

	RenderTextureClass RenderToTexture;
	HINSTANCE                           hInst = NULL;
	HWND                                hWnd = NULL;
	XMMATRIX                            World;
	XMMATRIX                            View;
	XMMATRIX                            Projection;

	float m_flGravity = 0.1f;

	HINSTANCE GetInst();
	HWND GetWindow();
	D3D_DRIVER_TYPE GetDriverType();
	D3D_FEATURE_LEVEL GetFeatureLevel();
	ID3D11Device *GetDevice();
	ID3D11DeviceContext *GetDeviceContext();
	IDXGISwapChain *GetSwapChain();
	ID3D11RenderTargetView *GetRenderTarget();
	ID3D11Texture2D *GetStencilBuffer();
	ID3D11DepthStencilView *GetStencilView();
	ID3D11Buffer* const *GetConstantBuffer();
	ID3D11SamplerState *GetLinearSampler();
	ID3D11SamplerState *GetScreenSampler();
	XMMATRIX GetWorldMatrix();
	XMMATRIX GetViewMatrix();
	XMMATRIX GetProjectionMatrix();
	XMFLOAT4 GetMeshColor();
	HRESULT LoadShaders();

	ID3D11RasterizerState *rs_CW, *rs_CCW, *rs_NO, *rs_Wire;
	ID3D11DepthStencilState	*ds_on, *ds_off;

	ID3D11Buffer*                       pCBuffer = NULL;
	ID3D11RenderTargetView*             pRenderTargetView = NULL;
	ID3D11SamplerState*                 pSamplerLinear = NULL;
	ID3D11SamplerState*                 SamplerScreen = NULL;

	// Utility functions to retrieve a vertex shader and pixel shader respectively.
	ID3D11VertexShader* GetVertexShader(const string name);
	ID3D11PixelShader* GetPixelShader(const string name);


	// Utility function for creating a vertex buffer from an array of vertices.
	HRESULT CreateVertexBuffer(const string name, const SimpleVertex &vertices, int iNumVertices);
	// Utility function for creating a shader file.
	HRESULT CreateShader(const string shaderName, const string fileName);

	VertexBufferData GetVertexBuffer(const string name);

	// Our wonderful windows initializations
	unsigned int Timer = -1;
	LRESULT CALLBACK HandleMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
	HRESULT InitDevice();
	void CleanupDevice();
	static BOOL OnCreate(HWND hwnd, CREATESTRUCT FAR* lpCreateStruct);
	static void OnTimer(HWND hwnd, UINT id);
	static void OnKeyUp(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags);
	static void OnKeyDown(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags);
	static void OnLBU(HWND hwnd, int x, int y, UINT keyFlags);
	static void OnRBU(HWND hwnd, int x, int y, UINT keyFlags);
	static void OnMM(HWND hwnd, int x, int y, UINT keyFlags);
	static void OnLBD(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static void Render();

	CEngine();
	~CEngine();
};

#endif // ENGINE_H