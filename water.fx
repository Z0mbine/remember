//--------------------------------------------------------------------------------------
// File: Tutorial022.fx
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------

Texture2D txDiffuse : register(t0);
SamplerState samLinear : register(s0);

// How big is a single square?
#define FRAMESIZE 128.0f
#define TEXSIZE 4096.0f
#define ROWS 4
#define COLS 1

cbuffer ConstantBuffer : register(b0)
{
	matrix World;
	matrix View;
	matrix Projection;
	float4 info;
};

//--------------------------------------------------------------------------------------
struct VS_INPUT
{
	float4 Pos : POSITION;
	float2 Tex : TEXCOORD0;
	float3 Norm : NORMAL0;
};

struct PS_INPUT
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD0;
	float4 Col : COLOR0;
};
//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output = (PS_INPUT)0;

	float4 pos = input.Pos;

	pos = mul(pos, World);
	output.Pos = mul(pos, View);
	output.Pos = mul(output.Pos, Projection);
	output.Tex = input.Tex;

	return output;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PS_INPUT input) : SV_Target
{
	int frame = (int)info.x;

	float2 tex = input.Tex;
	tex.y /= 32;
	tex *= 10;

	// TODO: I'd like to scale the water texture down, but when I do, it makes the animation glitchy

	float column = ((frame % COLS) * FRAMESIZE) / TEXSIZE;
	float row = (int(frame / COLS) * FRAMESIZE) / TEXSIZE;

	tex.x += column;
	tex.y += row;

	float4 color = txDiffuse.Sample(samLinear, tex);
	color.a = 0.65;

	return color;
}
