


#include "xmath.h"
// XMFLOAT3 a,b; a = a + b;
XMFLOAT3 operator+(const XMFLOAT3 lhs, XMFLOAT3 rhs)
	{
	XMFLOAT3 res;
	res.x = lhs.x + rhs.x;
	res.y = lhs.y + rhs.y;
	res.z = lhs.z + rhs.z;
	return res;
	}
// XMFLOAT3 a,b; a = a - b;
XMFLOAT3 operator-(const XMFLOAT3 lhs, XMFLOAT3 rhs)
	{
	XMFLOAT3 res;
	res.x = lhs.x - rhs.x;
	res.y = lhs.y - rhs.y;
	res.z = lhs.z - rhs.z;
	return res;
	}
XMFLOAT2 operator+(const XMFLOAT2 lhs, XMFLOAT2 rhs)
	{
	XMFLOAT2 res;
	res.x = lhs.x + rhs.x;
	res.y = lhs.y + rhs.y;
	return res;
	}
// XMFLOAT3 a,b; a = a - b;
XMFLOAT2 operator-(const XMFLOAT2 lhs, XMFLOAT2 rhs)
	{
	XMFLOAT2 res;
	res.x = lhs.x - rhs.x;
	res.y = lhs.y - rhs.y;
	return res;
	}
//multiplication with matrix
// XMFLOAT3 a; XMMATRIX M;a = M*a;
XMFLOAT3 operator*(const XMMATRIX &lhs, XMFLOAT3 rhs)
	{
	XMFLOAT3 res;

	XMVECTOR f = XMLoadFloat3(&rhs);
	f = XMVector3TransformCoord(f, lhs);
	XMStoreFloat3(&res, f);
	return res;
	}
// XMFLOAT3 a; XMMATRIX M;a = M*a;
XMFLOAT3 operator*(XMFLOAT3 rhs, const XMMATRIX &lhs)
	{
	XMFLOAT3 res;

	XMVECTOR f = XMLoadFloat3(&rhs);
	f = XMVector3TransformCoord(f, lhs);
	XMStoreFloat3(&res, f);
	return res;
	}
// XMFLOAT3 a; a = a*0.01;
XMFLOAT3 operator*(const XMFLOAT3 lhs, float rhs)
	{
	XMFLOAT3 res;
	res.x = lhs.x * rhs;
	res.y = lhs.y * rhs;
	res.z = lhs.z * rhs;
	return res;
	}
// XMFLOAT3 a; a = 0.01*a;
XMFLOAT3 operator*(float rhs, const XMFLOAT3 lhs)
	{
	XMFLOAT3 res;
	res.x = lhs.x * rhs;
	res.y = lhs.y * rhs;
	res.z = lhs.z * rhs;
	return res;
	}
XMFLOAT3 normalize(XMFLOAT3 in)
	{
	float len = length(in);
	return XMFLOAT3(in.x / len, in.y / len, in.z / len);
	}

float length(XMFLOAT3 v)
	{
	return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
	}
float length(XMFLOAT2 v)
	{
	return sqrt(v.x*v.x + v.y*v.y);
	}
float angle(XMFLOAT3 a, XMFLOAT3 b)
	{
	float dot_product = a.x * b.x + a.y * b.y + a.z * b.z;
	float angle = acos(dot_product / (length(a) * length(b)));
	return angle;
	}
bool operator==(const XMFLOAT3 lhs, XMFLOAT3 rhs)
	{
	return (lhs.x == rhs.x) && (lhs.y == rhs.y) && (lhs.z == rhs.z);
	}

float dot(XMFLOAT3 a, XMFLOAT3 b)
	{
	return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
	}
XMFLOAT3 cross(XMFLOAT3 a, XMFLOAT3 b)
	{
	return XMFLOAT3((a.y * b.z) - (a.z * b.y), (a.z * b.x) - (a.x * b.z), (a.x * b.y) - (a.y * b.x));
	}
float dot(XMFLOAT2 a, XMFLOAT2 b)
	{
	return (a.x * b.x) + (a.y * b.y);
	}

XMFLOAT3 XMFLOAT3CompMultiply(XMFLOAT3 a, XMFLOAT3 b)
	{
	return XMFLOAT3(a.x * b.x, a.y * b.y, a.z * b.z);
	}

XMFLOAT3 XMFLOAT3Lint(XMFLOAT3 a, XMFLOAT3 b, float t)
	{
	float x = (1 - t) * a.x + t * b.x;
	float y = (1 - t) * a.y + t * b.y;
	float z = (1 - t) * a.z + t * b.z;
	return XMFLOAT3(x, y, z);
	}

/******************FOR THE LEVEL***************/
XMFLOAT3 mul(XMFLOAT3 v, XMMATRIX &M)
	{
	XMVECTOR f = XMLoadFloat3(&v);
	f = XMVector3TransformCoord(f, M);
	XMStoreFloat3(&v, f);
	v.x += M._41;
	v.y += M._42;
	v.z += M._43;
	return v;
	}

XMFLOAT2 v32(XMFLOAT3 v)
	{
	return XMFLOAT2(v.x, v.y);
	}
XMFLOAT3 v23(XMFLOAT2 v, float z)
	{
	return XMFLOAT3(v.x, v.y, z);
	}
bool line_line_intersection_segments(XMFLOAT2 p1, XMFLOAT2 p2, XMFLOAT2 p3, XMFLOAT2 p4, XMFLOAT2 *erg)
	{
	// Store the values for fast access and easy
	// equations-to-code conversion
	double x1 = p1.x, x2 = p2.x, x3 = p3.x, x4 = p4.x;
	double y1 = p1.y, y2 = p2.y, y3 = p3.y, y4 = p4.y;

	double d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
	// If d is zero, there is no intersection
	if (d == 0) return FALSE;

	// Get the x and y
	double pre = (x1*y2 - y1*x2), post = (x3*y4 - y3*x4);
	double x = (pre * (x3 - x4) - (x1 - x2) * post) / d;
	double y = (pre * (y3 - y4) - (y1 - y2) * post) / d;

	// Check if the x and y coordinates are within both lines
	if (x < min(x1, x2) || x > max(x1, x2) ||
		x < min(x3, x4) || x > max(x3, x4)) return FALSE;
	if (y < min(y1, y2) || y > max(y1, y2) ||
		y < min(y3, y4) || y > max(y3, y4)) return FALSE;

	if (erg)	*erg = XMFLOAT2(x, y);

	return TRUE;
	}